<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
    <meta charset="UTF-8">
    <title>投稿育成論閲覧リスト</title>
    <style>
        body {
            background-image: url("img/p3.jpg");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>

    <link rel="stylesheet" href="css/icon.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/box.css">
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/input-form3.css">
    <link rel="stylesheet" href="css/search.css">
    <link rel="stylesheet" href="css/textarea.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
<header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <div align="center">
        <div class="box16"></div>
        <div class="Box24">
            <table border="4" width="1000">
                <tr bgcolor="#f5deb3">

                    <th>タイトル</th>
                    <th>ユーザ名</th>
                    <th>ポケモンの名前</th>
                    <th>特性</th>
                    <th>ステータス振り</th>
                    <th>持ち物</th>
                </tr>
                <c:forEach var="taL" items="${taList}" >
                <tr bgcolor="#ffffff">
                    <td align="center" width="250px"><a href="TrainingArticleBrowsing?id=${taL.id}">${taL.title}</a></td>
                    <td align="center">${taL.userName}</td>
                    <td width="140px"><img class="icon32_sp" src="pokemonAllnum/${taL.pokemonId}.gif">${taL.name}</td>
                    <td>${taL.characteristic}</td>
                    <td width="270px">　性格；${taL.correction}<br>
                    　h${taL.hp_EffortValue}/ a${taL.attack_EffortValue}/ b${taL.defense_EffortValue}/
                    c${taL.specialAttack_EffortValue}/ d${taL.specialDefense_EffortValue}/ s${taL.speed_EffortValue}</td>
                    <td align="center">${taL.item}</td>
                </tr>
                </c:forEach>
            </table>
            <br>
            <div align="center">
                <a href="AllUserTrainingArticleList">${page}</a>,
            </div>
        </div>

    </div>
</body>

</html>