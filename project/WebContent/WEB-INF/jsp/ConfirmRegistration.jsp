<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
	<head>
		<meta charset="UTF-8">
		<title>新規ユーザ登録確認画面</title>
        <style>
            body {
              background-image: url("img/p7.jpg");
              background-color: #e0ffff;
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
              background-attachment: fixed;
                }
        </style>
        <link rel="stylesheet" href="css/box.css">
        <!--背景の画像をページサイズに合わせるcss-->

        <link rel="stylesheet" href="css/example.css">
        <!--ヘッダーの色-->
        <link rel="stylesheet" href="css/header.css">
        <!--ヘッダーのメニューに該当する物の並びを横に変更-->
        <link rel="stylesheet" href="css/ul.css">
        <!--メニューに該当する物のアンダーバーのデザイン-->
         <link rel="stylesheet" href="css/btn.css">
        <link rel="stylesheet" href="css/btn-gradient.css">
        <link rel="stylesheet" href="css/style3.css">

    </head>
    <body  link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
        <div class="text-white" id="wrapper">
            <div align="center">
                <div id="row-3st-in">
                    <div class="box6">
                        <!--box+上の文字を中央に持ってくる-->
                        <div class="center">
                            <p>"ログインしたユーザ名"さん</p>
                            <p>登録が完了しました</p>
                            <p>下のリンクからログインページに移行してください</p>
                            <a href="Login">
                            ログインページへ
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>