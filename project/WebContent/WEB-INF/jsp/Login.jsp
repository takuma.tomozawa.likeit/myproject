<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ログイン</title>
    <style>
        body {
            background-image: url("img/p4.jpg");
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
    <!--箱の作成-->
    <link rel="stylesheet" href="css/box.css">
    <!--背景の画像をページサイズに合わせるcss-->
    <link rel="stylesheet" href="css/resizeimage.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/focus-textbox.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
    <div class="text-white" id="wrapper">
        <div align="center">
            <div id="row-2st-in">
                <!--box+上の文字を中央に持ってくる-->
                <div class="center">
                    <div class="box2">
                        <p class="form-area3">
                            <img src="img/pekemontitol.png">
                        </p>
                    </div>
                    <!--boxの中に入れるもの-->
                    <div class="box30">
	                    <form action="/pokemon/Login" method="post">
	                    	<c:if test="${error != null}" >
							    <div class="alert alert-danger" role="alert">
								  ${error}
								</div>
							</c:if>
	                        <div>
	                            <b>ようこそ</b>
	                        </div>

	                        <div class="stuff">
	                            <div class="textBox">
	                                <input class="text" type="textbox" placeholder="ログインID" name="loginId" value="" >
	                                <label class="label">ログインID</label>
	                                <label class="error"></label>
	                            </div>
	                            <div class="textBox">
	                                <input class="text" type="password" placeholder="パスワード" name="password" value="" >
	                                <label class="label">パスワード</label>
	                                <label class="error"></label>
	                            </div>
	                        </div>
	                        <br>
	                        <a href="UserList">
	                            <input type="submit" value="続行" class="btn-gradient-3d-simple">
	                        </a>
                        </form>
                    </div>

                    <div style="margin-right:300px">
                        <a href="Signin">
                            戻る
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>

</html>