<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
    <meta charset="UTF-8">
    <title>マイページ</title>
    <style>
        body {
            background-image: url("img/p3.jpg");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>


    <link rel="stylesheet" href="css/box.css">
    <!--背景の画像をページサイズに合わせるcss-->
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/focus-textbox.css">
    <link rel="stylesheet" href="css/search.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
    <header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>



    <div id="myPageRow-1st-in">
        <div class="box15">
            <table border="0" width="600">
                <tr>
                    <td>
                        <div class="myPageBox1">
                            <a href="PokemonTrainedList?id=${id}">
                                <img src="img/sarunoributton.png">
                            </a>
                        </div>
                    </td>
                    <td>
                        <div class="myPageBox1">
                            <a href="AllUserTrainingArticleList">
                                <img src="img/hibanibutton.png">
                            </a>
                        </div>
                    </td>
                    <td>
                        <div class="myPageBox1">
                            <a href="TrainingArticleList">
                                <img src="img/messonbutton.png">
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <c:if test="${userInfo.loginId == 1}">
                            <div align="center">
                                <a href="PokemonDataEntry">
                                    <input type="submit" value="管理者権限" class="btn-gradient-3d-black-simple">
                                </a>
                            </div>
                        </c:if>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>