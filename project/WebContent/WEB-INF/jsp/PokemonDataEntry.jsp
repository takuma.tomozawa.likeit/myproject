<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>管理者用ポケモンデータ入力</title>
    <style>
        body {
            background-image: url("img/p3.jpg");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.1.0.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/box.css">
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/input-form3.css">
    <link rel="stylesheet" href="css/search.css">
    <link rel="stylesheet" href="css/textarea.css">
    <link rel="stylesheet" href="css/sel.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
<header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <div align="center">
        <div class="box7"></div>
        <div class="box22">
            <h3 align="center">
                <font face="ＭＳ ゴシック">以下にポケモンの情報を入力</font>
            </h3>

            <form action="/pokemon/PokemonDataEntry" method="post">

	            <table border="0" width="600">
		            <tr>
			            <td align="center" colspan="6">
				            <c:if test="${error != null}" >
				                <div class="alert alert-danger" role="alert">
				                ${error}
				                </div>
				            </c:if>
			            </td>
		            </tr>
                    <tr>
                        <td colspan="6">
                            <div class="textBox">
                                <input class="text" type="number" name="all_number" placeholder="全国図鑑ID" onkeyup="this.setAttribute('value', this.value);" value="${all_number}">
                                <label class="label">全国図鑑ID</label>
                                <label class="error"></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div class="textBox">
                                <input class="text" type="number" name="galar_number" placeholder="ガラル図鑑ID" onkeyup="this.setAttribute('value', this.value);" value="${galar_number}">
                                <label class="label">ガラル図鑑ID</label>
                                <label class="error"></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div class="textBox">
                                <input class="text" type="textbox" name="name" placeholder="ポケモンの名前" onkeyup="this.setAttribute('value', this.value);" value="${name}">
                                <label class="label">ポケモンの名前</label>
                                <label class="error"></label>
                            </div>
                        </td>
                    </tr>
                    <tr><td colspan="6"><br></td></tr>
                    <tr>
                        <td colspan="3">
                            <div class="textBox">
                                <input class="text" type="textbox" name="type1" placeholder="タイプ1" value="${type1}" list="type">
                                <datalist id="type">
                                    <option value="" hidden disabled selected></option>
                                    <c:forEach items="${typeList}" var="typeList">
                                        <option value="${typeList.type}">${typeList.type}</option>
                                    </c:forEach>
                                </datalist><br>
                                <label class="label">タイプ1</label>
                                <label class="error"></label>
                            </div>
                        </td>

                        <td colspan="3">
                            <div class="textBox">
                                <input class="text" type="textbox" name="type2" placeholder="タイプ2" value="${type2}" list="type">
                                <label class="label">タイプ2</label>
                                <label class="error"></label>
                            </div>
                        </td>
                    </tr>
                    <tr><td colspan="6"><br></td></tr>
                    <tr>
                        <td colspan="3">
                            <div class="textBox">
                                <input class="text" type="textbox" name="characteristic1" placeholder="特性1" onkeyup="this.setAttribute('value', this.value);" value="${characteristic1}">
                                <label class="label">特性1</label>
                                <label class="error"></label>
                            </div>
                        </td>
                        <td colspan="3">
                            <div class="textBox">
                                <input class="text" type="textbox" name="characteristic2" placeholder="特性2" onkeyup="this.setAttribute('value', this.value);" value="${characteristic2}">
                                <label class="label">特性2</label>
                                <label class="error"></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="textBox">
                                <input class="text" type="textbox" name="characteristic_secret" placeholder="夢特性" onkeyup="this.setAttribute('value', this.value);" value="${characteristicSecret}">
                                <label class="label">夢特性</label>
                                <label class="error"></label>
                            </div>
                        </td>
                    </tr>
                    <td colspan="6"><br></td>
                    <tr>
                        <td colspan="2">
                            <div class="textBox">
                                <input class="text" type="number" step="0.1" name="weight" placeholder="重さ" value="${weight}">
                                <label class="label">重さ</label>
                                <label class="error"></label>
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="textBox">
                                <input class="text" type="textbox" name="egg_group1" placeholder="タマゴグループ1" value="${egg_group1}" list="eggGroup">
                                <datalist id="eggGroup">
                                    <option value="" hidden disabled selected></option>
                                    <c:forEach items="${eggGroupList}" var="eggGroupList">
                                        <option value="${eggGroupList.eggGroup}">${eggGroupList.eggGroup}</option>
                                    </c:forEach>
                                </datalist><br>
                                <label class="label">タマゴグループ1</label>
                                <label class="error"></label>
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="textBox">
                                <input class="text" type="textbox" name="egg_group2" placeholder="タマゴグループ2" value="${egg_group2}" list="eggGroup">
                                <datalist id="eggGroup">

                                </datalist><br>
                                <label class="label">タマゴグループ2</label>
                                <label class="error"></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6"><br></td>
                    </tr>
                    <tr>
                        <td colspan="6">種族値</td>
                    </tr>
                    <tr>
                        <td>
                            <div class="stuff2">
                                <div class="textBox">
                                    <input class="text" type="number" placeholder="H" name="hp_race_value" value="${hp_race_value}" min="0" max="999"/>
                                    <label class="label">H</label>
                                    <label class="error"></label>
                                </div>
                            </div>

                        </td>

                        <td>
                            <div class="stuff2">
                                <div class="textBox">
                                    <input class="text" type="number" placeholder="A" name="attack_race_value" value="${attack_race_value}" min="0" max="999"/>
                                    <label class="label">A</label>
                                    <label class="error"></label>
                                </div>
                            </div>
                        </td>


                        <td>
                            <div class="stuff2">
                                <div class="textBox">
                                    <input class="text" type="number" placeholder="B" name="defense_race_value" value="${defense_race_value}" min="0" max="999"/>
                                    <label class="label">B</label>
                                    <label class="error"></label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="stuff2">
                                <div class="textBox">
                                    <input class="text" type="number" placeholder="C" name="special_attack_race_value" value="${special_attack_race_value}" min="0" max="999"/>
                                    <label class="label">C</label>
                                    <label class="error"></label>
                                </div>
                            </div>
                        </td>

                        <td>
                            <div class="stuff2">
                                <div class="textBox">
                                    <input class="text" type="number" placeholder="D" name="special_defense_race_value" value="${special_defense_race_value}" min="0" max="999"/>
                                    <label class="label">D</label>
                                    <label class="error"></label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="stuff2">
                                <div class="textBox">
                                    <input class="text" type="number" placeholder="S" name="speed_race_value" value="${speed_race_value}" min="0" max="999"/>
                                    <label class="label">S</label>
                                    <label class="error"></label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6"><br></td>
                    </tr>



                    <tr><td colspan="6"><br></td></tr>
                    <tr>
                        <td colspan="6">
                            <div align="center">
                                <a href="PokemonRegistrationList">
                                    <input type="submit" value="データの入力" class="btn-gradient-3d-simple">
                                </a>
                            </div>
                        </td>
                    </tr>
                    <tr><td colspan="6"><br></td></tr>
                    <tr>
                        <td colspan="6">
                            <div align="center">
                                <a href="Skill.html">
                                    <input type="submit" value="技リストの入力へ" class="btn-gradient-3d-yellow-simple">
                                </a>
                            </div>
                        </td>
                    </tr>

            </table>
            </form>
        </div>
    </div>
</body>

</html>