<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>第8世代ポケモン詳細画面</title>
        <style>
            body {
              background-image: url("img/p9.png");
              background-color: #e0ffff;
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
              background-attachment: fixed;
                }
        </style>
        <link rel="stylesheet" href="css/back.css">
        <link rel="stylesheet" href="css/table.css">
        <link rel="stylesheet" href="css/table-layout.css">
        <link rel="stylesheet" href="css/box.css">
        <!--背景の画像をページサイズに合わせるcss-->
		<link rel="stylesheet" href="css/position.css">
        <link rel="stylesheet" href="css/example.css">
        <!--ヘッダーの色-->
        <link rel="stylesheet" href="css/header.css">
        <!--ヘッダーのメニューに該当する物の並びを横に変更-->
        <link rel="stylesheet" href="css/ul.css">
        <!--メニューに該当する物のアンダーバーのデザイン-->
         <link rel="stylesheet" href="css/btn.css">
        <link rel="stylesheet" href="css/btn-gradient.css">
        <link rel="stylesheet" href="css/style3.css">
        <link rel="stylesheet" href="css/focus-textbox.css">
        <link rel="stylesheet" href="css/search.css">
    </head>
    <body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
	<header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>
        <div align="center">
            <div class="box6"></div>

            <div id="back">
            <div class="box18">

            <div class="table layout_left">
                <table border="1" width="200" align="center">
                    <tr align="center">
                        <td colspan="2" bgcolor="#89ffc4">
                        ${pbd.name}
                        </td>
                    </tr>
                    <tr align="center" bgcolor="#dbffed">
                        <td colspan="2">
                        <img src="pokemonDetailAllnum/${pbd.allNumber}.gif">
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            ガラルNo.
                        </td>
                        <td>
                            ${pbd.galarNumber}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            全国No.
                        </td>
                        <td>
                            ${pbd.allNumber}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            重さ
                        </td>
                        <td>
                            ${pbd.whight}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            タイプ
                        </td>
                        <td>
                            ${pbd.type1}<br>${pbd.type2}
                        </td>
                    </tr>
                </table>
            </div>

                <div class="table layout_right">
                <table border="1" width="385">
                    <tr align="center">
                        <td colspan="6" bgcolor="#89ffc4">
                        ${pbd.name}の種族値
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            たいりょく
                        </td>
                        <td colspan="5">
                            ${pbd.hp}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            こうげき
                        </td>
                        <td colspan="5">
                            ${pbd.attack}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            ぼうぎょ
                        </td>
                        <td colspan="5">
                            ${pbd.defense}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            とくしゅ
                        </td>
                        <td colspan="5">
                            ${pbd.specialAttack}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            とくぼう
                        </td>
                        <td colspan="5">
                            ${pbd.specialDefense}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            すばやさ
                        </td>
                        <td colspan="5">
                            ${pbd.speed}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#ffc489">
                        実数値
                        </td>
                        <td bgcolor="#ffd3a8">
                        最高
                        </td>
                        <td bgcolor="#ffd3a8">
                        準
                        </td>
                        <td bgcolor="#ffd3a8">
                        並
                        </td>
                        <td bgcolor="#ffd3a8">
                        下降
                        </td>
                        <td bgcolor="#ffd3a8">
                        最低
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            HP
                        </td>
                        <td>
                        ${pbd.hp_v_max}
                        </td>
                        <td>
                        ${pbd.hp_v_max}
                        </td>
                        <td>
                        ${pbd.hp_v_none}
                        </td>
                        <td>
                        ${pbd.hp_v_none}
                        </td>
                        <td>
                        ${pbd.hp_0_none}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            こうげき
                        </td>
                        <td>
                        ${pbd.attack_up_v_max}
                        </td>
                        <td>
                        ${pbd.attack_v_max}
                        </td>
                        <td>
                        ${pbd.attack_v_none}
                        </td>
                        <td>
                        ${pbd.attack_down_v_none}
                        </td>
                        <td>
                        ${pbd.attack_down_0_none}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            ぼうぎょ
                        </td>
                        <td>
                        ${pbd.defense_up_v_max}
                        </td>
                        <td>
                        ${pbd.defense_v_max}
                        </td>
                        <td>
                        ${pbd.defense_v_none}
                        </td>
                        <td>
                        ${pbd.defense_down_v_none}
                        </td>
                        <td>
                        ${pbd.defense_down_0_none}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            とくこう
                        </td>
                        <td>
                        ${pbd.specialAttack_up_v_max}
                        </td>
                        <td>
                        ${pbd.specialAttack_v_max}
                        </td>
                        <td>
                        ${pbd.specialAttack_v_none}
                        </td>
                        <td>
                        ${pbd.specialAttack_down_v_none}
                        </td>
                        <td>
                        ${pbd.specialAttack_down_0_none}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            とくぼう
                        </td>
                        <td>
                        ${pbd.specialDefense_up_v_max}
                        </td>
                        <td>
                        ${pbd.specialDefense_v_max}
                        </td>
                        <td>
                        ${pbd.specialDefense_v_none}
                        </td>
                        <td>
                        ${pbd.specialDefense_down_v_none}
                        </td>
                        <td>
                        ${pbd.specialDefense_down_0_none}
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            すばやさ
                        </td>
                        <td>
                        ${pbd.speed_up_v_max}
                        </td>
                        <td>
                        ${pbd.speed_v_max}
                        </td>
                        <td>
                        ${pbd.speed_v_none}
                        </td>
                        <td>
                        ${pbd.speed_down_v_none}
                        </td>
                        <td>
                        ${pbd.speed_down_0_none}
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="6" bgcolor="#89ffc4">
                            ${pbd.name}の特性
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            ${pbd.characteristic1}
                        </td>
                        <td colspan="5">
                            xxx
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            ${pbd.characteristic2}
                        </td>
                        <td colspan="5">
                            xxx
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="6" bgcolor="#89ffc4">
                            ${pbd.name}の夢特性
                        </td>
                    </tr>
                    <tr align="center">
                        <td bgcolor="#d1ffff">
                            ${pbd.characteristicSecret}
                        </td>
                        <td colspan="5">
                            xxx
                        </td>
                    </tr>
                </table>
            </div>

            </div>

            <div class="box19">
                <div class="table layout_left">
                <table border="1" width="500">
                    <tr align="center">
                        <td colspan="6" bgcolor="#89ffc4">
                        レベルアップで覚える技
                        </td>
                    </tr>
                    <tr align="center" bgcolor="#ffffff">
                        <td>
                        タイプ
                        </td>
                        <td>
                            分類
                        </td>
                        <td>
                            威力
                        </td>
                        <td>
                            命中
                        </td>
                        <td>
                            PP
                        </td>
                        <td>
                            説明
                        </td>
                    </tr>
                    <tr align="center" bgcolor="#d1ffff">
                        <td>
                        レベル.1
                        </td>
                        <td colspan="5">
                            すいとる
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                            くさ
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="6" bgcolor="#89ffc4">
                        技マシンでで覚える技
                        </td>
                    </tr>
                    <tr align="center" bgcolor="#ffffff">
                        <td>
                        タイプ
                        </td>
                        <td>
                            分類
                        </td>
                        <td>
                            威力
                        </td>
                        <td>
                            命中
                        </td>
                        <td>
                            PP
                        </td>
                        <td>
                            説明
                        </td>
                    </tr>
                    <tr align="center" bgcolor="#d1ffff">
                        <td>
                        レベル.1
                        </td>
                        <td colspan="5">
                            すいとる
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="6" bgcolor="#89ffc4">
                        技レコードでで覚える技
                        </td>
                    </tr>
                    <tr align="center" bgcolor="#ffffff">
                        <td>
                        タイプ
                        </td>
                        <td>
                            分類
                        </td>
                        <td>
                            威力
                        </td>
                        <td>
                            命中
                        </td>
                        <td>
                            PP
                        </td>
                        <td>
                            説明
                        </td>
                    </tr>
                    <tr align="center" bgcolor="#d1ffff">
                        <td>
                        レベル.1
                        </td>
                        <td colspan="5">
                            すいとる
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="6" bgcolor="#89ffc4">
                        タマゴ技
                        </td>
                    </tr>
                    <tr align="center" bgcolor="#ffffff">
                        <td>
                        タイプ
                        </td>
                        <td>
                            分類
                        </td>
                        <td>
                            威力
                        </td>
                        <td>
                            命中
                        </td>
                        <td>
                            PP
                        </td>
                        <td>
                            説明
                        </td>
                    </tr>
                    <tr align="center" bgcolor="#d1ffff">
                        <td>
                        レベル.1
                        </td>
                        <td colspan="5">
                            すいとる
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                        <td>
                            xxx
                        </td>
                    </tr>


                </table>
                </div>
            </div>

		</div>

	</body>
</html>