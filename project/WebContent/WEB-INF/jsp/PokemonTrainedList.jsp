<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
    <meta charset="UTF-8">
    <title>育成ポケモンリスト</title>
    <style>
        body {
            background-image: url("img/p3.jpg");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>

    <link rel="stylesheet" href="css/icon.css">
    <link rel="stylesheet" href="css/box.css">
    <!--背景の画像をページサイズに合わせるcss-->
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/focus-textbox.css">
    <link rel="stylesheet" href="css/search.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
    <header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <c:if test="${us == null}">
            <li><a href="Login" class="btn4">ログイン</a></li>
            </c:if>
            <c:if test="${us != null}">
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            </c:if>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <div align="center">
        <div class="box16"></div>
        <div class="box17">
            <table border="0" width="600">
                <tr>
                    <td>
                        <a href="PokemonTrainedRegistration">
                            <img src="img/trainedButton1.png">
                        </a>
                    </td>
                    <td>
                        <a href="PokemonTrainedUpdate">
                            <img src="img/trainedButton2.png">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="Box10">



	        <table border="1" width="800">
	            <tr bgcolor="#a3d6cc">
	                <th></th>
	                <th>名前</th>
	                <th>ニックネーム</th>
	                <th>特性</th>
	                <th>ステータス</th>
	                <th>持ち物</th>
	            </tr>
 	            <c:if test="${ptList != null}">

		            <c:forEach items="${ptList}" var="ptl">
		                <tr  align="center" bgcolor="#ffffff">
		                    <td>
								<form action="/pokemon/PokemonTrainedList" method="post">
			                    	<img class="icon32_sp-2" src="pokemonAllnum/${ptl.pokemonId}.gif">

						            <input type="submit" name="createId" value="${ptl.createId}" class="btn-gradient-3d-pokemon-simple">
						        </form>
		                    </td>
		                    <td>
		                    	${ptl.name}
		                    </td>
		                    <td>${ptl.nickname}</td>
		                    <td>${ptl.characteristic}</td>
		                    <td>
		                    ${ptl.hp_EffortValue}/
		                    ${ptl.attack_EffortValue}/
		                    ${ptl.defense_EffortValue}/
		                    ${ptl.specialAttack_EffortValue}/
		                    ${ptl.specialDefense_EffortValue}/
		                    ${ptl.speed_EffortValue}
		                    </td>
		                    <td>${ptl.item}</td>
		                </tr>
		            </c:forEach>
	            </c:if>
	        </table>
            <br>
            <div align="center">
                <a href="PokemonTrainedList">${page}</a>,
            </div>
        </div>

        <!--以下の記述はいずれかのgif画像を選択すると表示されるようにする-->
        <c:if test="${ptList2 != null}">
	        <div class="box25">
	            <table border="2" width="800">



					<tr align="center">
	                    <td colspan="4" bgcolor="#d6ffff">
	                	<img class="icon32_sp" src="pokemonAllnum/${ptList2.pokemonId}.gif">

	                	このポケモンのニックネーム : ${ptList2.nickname}
	                	</td>
	                </tr>
	                <tr align="center">
	                    <td colspan="4" bgcolor="#f0f8ff">
	                    ※実数値の値はV以外の数値が未確定な為、個体値が31の状態で計算されています<br><br>
	                    性格:${ptList2.correction}　　実数値　/　HP${ptList2.hp}　攻撃${ptList2.attack}　防御${ptList2.defense}　特攻${ptList2.specialAttack}　特防${ptList2.specialDefense}　
	                    素早さ${ptList2.speed}<br>

	                    個体値　/　HP${ptList2.hp_IndividualValue}　攻撃${ptList2.attack_IndividualValue}　防御${ptList2.defense_IndividualValue}　
	                    特攻${ptList2.specialAttack_IndividualValue}　特防${ptList2.specialDefense_IndividualValue}　素早さ${ptList2.speed_IndividualValue}
	                    </td>
	                </tr>
	                <tr bgcolor="#ffffff"  align="center">

	                    <td colspan="1">
	                        技1 : ${ptList2.skill1}
	                    </td>
	                    <td colspan="1">
	                        技2 : ${ptList2.skill2}
	                    </td>
	                    <td colspan="1">
	                        技3 : ${ptList2.skill3}
	                    </td>
	                    <td colspan="1">
	                        技4 : ${ptList2.skill4}
	                    </td>
	                </tr>
	            </table>
	        </div>
	        <div class="box25">
	            <table border="2" width="780">
	                <tr bgcolor="#ffffff">
	                    <td>
	                    ${ptList2.remarks}
	                    </td>
	                </tr>
	            </table>
	        </div>
        </c:if>


    </div>
</body>

</html>