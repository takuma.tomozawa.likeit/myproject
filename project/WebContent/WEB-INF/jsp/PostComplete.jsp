<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
	<head>
		<meta charset="UTF-8">
		<title>育成論投稿完了画面</title>
        <style>
            body {
              background-image: url("img/p7.jpg");
              background-color: #e0ffff;
              background-repeat: no-repeat;
              background-size: contain;
              background-position: center;
              background-attachment: fixed;
                }
        </style>
        <link rel="stylesheet" href="css/box.css">
        <!--背景の画像をページサイズに合わせるcss-->

        <link rel="stylesheet" href="css/example.css">
        <!--ヘッダーの色-->
        <link rel="stylesheet" href="css/header.css">
        <!--ヘッダーのメニューに該当する物の並びを横に変更-->
        <link rel="stylesheet" href="css/ul.css">
        <!--メニューに該当する物のアンダーバーのデザイン-->
         <link rel="stylesheet" href="css/btn.css">
        <link rel="stylesheet" href="css/btn-gradient.css">
        <link rel="stylesheet" href="css/style3.css">

    </head>
    <body  link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
        <div class="text-white" id="wrapper">
            <div align="center">
                <div id="row-3st-in">
                    <div class="box28">
                        <!--box+上の文字を中央に持ってくる-->
                        <div class="center">
                            <p>おつかれさまでした</p>
                            <p>下のリンクから投稿済みの育成論一覧に移行できます</p>
                            <a href="TrainingArticleList">
                            一覧へ
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>