<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
    <meta charset="UTF-8">
    <title>投稿確認画面</title>
    <style>
        body {
            background-image: url("img/p2.jpg");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>
    <link rel="stylesheet" href="css/icon.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/box.css">
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/input-form3.css">
    <link rel="stylesheet" href="css/search.css">
    <!--バックグラウンドのカラーを設定している-->
    <link rel="stylesheet" href="css/textarea.css">

</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
    <div class="text-white" id="wrapper">

        <div align="center">
            <div class="box23">
                <!--box+上の文字を中央に持ってくる-->
                <div class="center">
                    <h3>以下の内容を育成論として投稿します</h3>
                    <h3>修正箇所がなければ下の "記事の投稿" から先に進んでください</h3>
                    <table border="0" width="850">
                        <tr>
                            <td colspan="5" align="center" bgcolor="#99ffff">
                                タイトル名:　${title}
                            </td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" bgcolor="#adffff">
                                ポケモン名:　<img class="icon32_sp" src="pokemonAllnum/${pokemonId}.gif">　${name}
                            </td>
                        </tr>
                        <tr>
                        <td><br></td>
                        <tr>
                            <td colspan="5" align="center" bgcolor="#c1ffff">
                                特性:　${characteristic}
                            </td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" bgcolor="#adffff">
                                性格:　${correction}
                            </td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" bgcolor="#adffff">
                                持ち物:　${item}
                            </td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" bgcolor="#d6ffff">
                                技:　

                                ${skill1}/　

                                ${skill2}/　

                                ${skill3}/　

                                ${skill4}
                            </td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" bgcolor="#d6ffff">
                                努力値:　

                                h${hp_EffortValue}/　

                                a${attack_EffortValue}/　

                                b${defense_EffortValue}/　

                                c${specialAttack_EffortValue}/　

                                d${specialDefense_EffortValue}/　

                                s${speed_EffortValue}
                            </td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" bgcolor="#d6ffff">
                                個体値:　

                                h${hp_IndividualValue}/　

                                a${attack_IndividualValue}/　

                                b${defense_IndividualValue}/　

                                c${specialAttack_IndividualValue}/　

                                d${specialDefense_IndividualValue}/　

                                s${speed_IndividualValue}
                            </td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                    </table>
                    <table border="1" width="850">
                        <tr>
                            <td colspan="5" align="center"><h4>～投稿記事の内容～</h4>
                            <br>
                            ${consideration}
                            <br>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <div align="center">
                        <a href="TrainingArticle">
                            <input type="button" value="修正する" class="btn-gradient-3d-red-simple">
                        </a>
                    </div>

                    <br>
                    <div align="center">
                    	<form action="PostConfirm" method="post">
	                        <input type="submit" value="記事の投稿" class="btn-gradient-3d-simple">
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>

</html>