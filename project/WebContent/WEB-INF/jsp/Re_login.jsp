<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ログイン画面</title>
        <!--ブーストラップ定期-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <!--箱の作成-->
        <link rel="stylesheet" href="css/box.css">
        <!--背景の画像をページサイズに合わせるcss-->
		<link rel="stylesheet" href="css/resizeimage.css">
        <link rel="stylesheet" href="css/example.css">
        <!--ヘッダーの色-->
        <link rel="stylesheet" href="css/header.css">
        <!--ヘッダーのメニューに該当する物の並びを横に変更-->
        <link rel="stylesheet" href="css/ul.css">
        <!--メニューに該当する物のアンダーバーのデザイン-->
         <link rel="stylesheet" href="css/btn.css">
        <link rel="stylesheet" href="css/btn-gradient.css">
    </head>
    <body>
        <header class="cooler text-white">
            <!--ヘッダーメニュー-->
            <ul>
                <li><a href="" class="btn4">トップ画面</a></li>
                <li><a href="" class="btn4">ポケモン図鑑</a></li>
                <li><a href="" class="btn4">a</a></li>
                <li><a href="" class="btn4">a</a></li>
                <li><a href="">a</a></li>
                <li></li>
                <li></li>
                <li></li>
          </ul>
            <!--左右に自動で幅をとる-->
			<div class="container">
                <!--文字を右側に寄せる-->
				<div align="right" >
                    <!--右から100px移動-->
					<span style="margin-right:100px">
					<a href="">
					このサイトについて
					</a>
					</span>
				</div>
			</div>
		</header>

        <div id="row-1st">
                <!--背景の画像をページサイズに合わせる-->
                <p  class="resizeimage img">
                    <!--画像-->
                    <img src="画像/1519202281717.jpg">
                </p>
            <div align="center">
            <div id="row-1st-in">

            <form action="" method="post">
                <!--box+上の文字を中央に持ってくる-->
                <div align="center">
                    <div>
                    ポケットモンスター（ここにぽけもんのなんかロゴ）
                    </div>
                    <!--boxの中に入れるもの-->
                    <div class="box">
                        <div>
                            <p>ようこそ</p>
                        </div>
                        <br>
                         <span>
                            <b>ログインID</b>
                        </span>
                        <input type="text" name="loginId" style="width:200px;" class="form-control">
                        <br>
                        <b>パスワード</b>
                        <input type="password" name="password" style="width:200px;" class="form-control">
                        <br>
                        <a href="UserList" >
                        <input type="submit" value="続行">
                        </a>
                    </div>

                </div>
                <div style="margin-right:300px">
                    <a href="">
                    戻る
                    </a>
                </div>
            </form>
            </div>
            </div>

        </div>

            <!--入力された文字の色が白になる-->
    		<footer class="cooler text-white">
			<div class="container">
				<a href="">
				利用規約
				</a>
				/
				<a href="">
				ヘルプ
				</a>
				/
				<a href="">
				プライバシー
				</a>
			</div>
		</footer>
    </body>
</html>