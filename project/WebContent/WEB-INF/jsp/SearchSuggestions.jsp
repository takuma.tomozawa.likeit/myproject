<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
    <meta charset="UTF-8">
    <title>マイページ</title>
    <style>
        body {
            background-image: url("img/p3.jpg");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>


    <link rel="stylesheet" href="css/box.css">
    <!--背景の画像をページサイズに合わせるcss-->
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/focus-textbox.css">
    <link rel="stylesheet" href="css/search.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
    <header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <c:if test="${us == null}">
            <li><a href="Login" class="btn4">ログイン</a></li>
            </c:if>
            <c:if test="${us != null}">
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            </c:if>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <div align="center">
        <div class="box7"></div>
        <div class="box31">
            <table border="1" bgcolor="#a3d6cc">
                <tr><th>候補を選択してください</th></tr>
                <c:forEach var="pokemon" items="${pokemon}" >
	                <tr align="center">
		                <td>
		                    <a href="PokemonListDetail?allNumber=${pokemon.allNumber}">${pokemon.name}</a>
		                </td>
	                </tr>
	            </c:forEach>
            </table>
        </div>
    </div>
</body>
</html>