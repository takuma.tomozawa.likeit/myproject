<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>サインイン</title>
    <style>
        body {
            background-image: url("img/p4.jpg");
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
        }
    </style>

    <!--箱の作成-->
    <link rel="stylesheet" href="css/box.css">
    <!--背景の画像をページサイズに合わせるcss-->
    <link rel="stylesheet" href="css/resizeimage.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/focus-textbox.css">
    <link rel="stylesheet" href="css/footer.css">
</head>

<body link="#7fffd4" vlink="#0073a8" alink="#8fbc8f">
    <div class="text-white" id="wrapper">
        <div align="center">
            <div id="row-2st-in">
                <div class="center">
                    <div class="box2">
                        <p class="form-area3">
                            <img src="img/pekemontitol.png">
                        </p>
                    </div>
                    <!--boxの中に入れるもの-->
                    <div class="box4">
                        <div>
                            <b>アカウントをお持ちですか?</b>
                        </div>
                        <br>

                        <div>
                            <a href="Login">
                                <input type="submit" value="はい" class="btn-gradient-3d-simple">
                            </a>
                        </div>
                        <br>
                        <div>
                            <a href="UserCreate">
                                <input type="submit" value="いいえ" class="btn-gradient-3d-red-simple">
                            </a>
                        </div>
                    </div>
                    <div style="margin-right:300px">
                        <a href="Top">
                            Topへ戻る
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>