<html>

<head>
    <meta charset="UTF-8">
    <title>管理者用ポケモン技データ入力</title>
    <style>
        body {
            background-image: url("img/p3.jpg");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.1.0.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/box.css">
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/input-form3.css">
    <link rel="stylesheet" href="css/search.css">
    <link rel="stylesheet" href="css/textarea.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
    <header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top.html" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage.html" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <li><a href="login.html" class="btn4">ログアウト</a></li>
            <li><a href="ThisSiteDetaile.html" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail.html" class="btn4"><img src="gif/547.gif" value="ログインユーザ名"></a></li>
            <li>
                <div class="box9">
                    <form action="/" name="search1" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="text" name="search" value="" placeholder="ポケモンの名前を入力">
                            </dt>
                            <dd>
                                <button>
                                    <span></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <div align="center">
        <div class="box7"></div>
        <div class="box22">
            <h3 align="center">
                <font face="ＭＳ ゴシック">以下に技の情報を入力</font>
            </h3>
            <table border="0" width="800">
                <div class="stuff1">

                    <tr>

                        <td colspan="6">
                            <div class="textBox">
                                <input class="text" type="textbox" name="name" placeholder="技の名前" onkeyup="this.setAttribute('value', this.value);" value="" / min="1" max="2000">
                                <label class="label">技の名前</label>
                                <label class="error"></label>
                            </div>
                        </td>

                    <tr>
                        <td colspan="1"><br></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="stuff3">
                                <div class="textBox">
                                    <input class="text" type="textbox" name="type" placeholder="タイプ" onkeyup="this.setAttribute('value', this.value);" value="" / list="type" value="">
                                    <datalist id="type">
                                        <option value="ノーマル">
                                        <option value="かくとう">
                                        <option value="ひこう">
                                        <option value="はがね">
                                        <option value="じめん">
                                        <option value="いわ">
                                        <option value="くさ">
                                        <option value="ほのお">
                                        <option value="みず">
                                        <option value="でんき">
                                        <option value="むし">
                                        <option value="こおり">
                                        <option value="どく">
                                        <option value="あく">
                                        <option value="エスパー">
                                        <option value="ゴースト">
                                        <option value="フェアリー">
                                        <option value="ドラゴン">
                                    </datalist><br>
                                    <label class="label">タイプ</label>
                                    <label class="error"></label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="stuff2">
                                <div class="textBox">
                                    <input class="text" type="textbox" name="classification" placeholder="分類" onkeyup="this.setAttribute('value', this.value);" value="" / list="classification" value="">
                                    <datalist id="classification">
                                        <option value="物理">
                                        <option value="特殊">
                                        <option value="変化">
                                    </datalist><br>
                                    <label class="label">分類</label>
                                    <label class="error"></label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="stuff2">
                                <div class="textBox">
                                    <input class="text" type="textbox" name="power" placeholder="威力" onkeyup="this.setAttribute('value', this.value);" value=""/ min="0" max="999" list="power" value="">
                                    <datalist id="power">
                                        <option value="ー">
                                        <option value="50">
                                        <option value="75">
                                        <option value="80">
                                        <option value="85">
                                        <option value="90">
                                        <option value="95">
                                        <option value="100">
                                        <option value="110">
                                        <option value="120">
                                        <option value="130">
                                        <option value="150">
                                    </datalist><br>
                                    <label class="label">威力</label>
                                    <label class="error"></label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="stuff2">
                                <div class="textBox">
                                    <input class="text" type="textbox" name="accuracy" placeholder="命中" onkeyup="this.setAttribute('value', this.value);" value=""/ min="0" max="100" list="accuracy" value="">
                                    <datalist id="accuracy">
                                        <option value="ー">
                                        <option value="30">
                                        <option value="50">
                                        <option value="70">
                                        <option value="75">
                                        <option value="80">
                                        <option value="85">
                                        <option value="90">
                                        <option value="95">
                                        <option value="100">
                                    </datalist><br>
                                    <label class="label">命中</label>
                                    <label class="error"></label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="stuff2">
                                <div class="textBox">
                                    <input class="text" type="number" placeholder="PP" onkeyup="this.setAttribute('value', this.value);" value=""/ min="0" max="99" list="PP" value="">
                                    <datalist id="PP">
                                        <option value="5">
                                        <option value="10">
                                        <option value="15">
                                        <option value="20">
                                        <option value="30">
                                        <option value="40">

                                    </datalist><br>
                                    <label class="label">PP</label>
                                    <label class="error"></label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div class="formBox">
                                <span class="item-name">技説明</span>

                                <div class="textarea-wrap">
                                    <textarea name="name" rows="3"></textarea>
                                </div>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div align="center">
                                <a href="PokemonDataEntry.html">
                                    <input type="submit" value="データの入力" class="btn-gradient-3d-yellow-simple">
                                </a>
                            </div>
                        </td>
                    </tr>

                </div>
            </table>
        </div>
    </div>
</body>

</html>