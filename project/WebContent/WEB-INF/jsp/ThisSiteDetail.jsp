<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
    <meta charset="UTF-8">
    <title>このサイトについて</title>
    <style>
        body {
            background-image: url("img/p8.jpg");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>
    <link rel="stylesheet" href="css/box.css">
    <!--背景の画像をページサイズに合わせるcss-->
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/focus-textbox.css">
    <link rel="stylesheet" href="css/search.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
    <header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <c:if test="${us == null}">
            <li><a href="Login" class="btn4">ログイン</a></li>
            </c:if>
            <c:if test="${us != null}">
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            </c:if>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <div class="box16"></div>
    <div align="center">
        <div class="box26">
            <table border="1" width="800">
                <tr>
                    <td align="center" width="400">
                        このサイトの管理者：
                    </td>
                    <td align="center">
                        フタモジ
                    </td>
                </tr>
                <tr>
                    <td align="center" width="400">
                        好きなポケモン：
                    </td>
                    <td align="center">
                        エルフーン
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <p>このサイトを作ろうと思った理由</p>
                        <p>自分が育てたポケモンの情報は実際に対戦で使用するときまで</p>
                        <p>ボックスで放置している状態になっている</p>
                        <p>直近で育成したポケモンの情報なら覚えているからいいものの</p>
                        <p>数が増えてきて少し前のポケモンの情報については</p>
                        <p>細かいステータス、仮想敵はだれでどのような役割を持つのかの情報を忘れてしまう</p>
                        <p>それらを早見表みたいな形で情報を登録していきたいためにこれを作りたいと思った</p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>