<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>第8世代ポケモンリスト</title>
    <style>
        body {
            background-image: url("img/p9.png");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>
    <link rel="stylesheet" href="css/icon.css">
    <link rel="stylesheet" href="css/back.css">

    <link rel="stylesheet" href="css/table-layout.css">
    <link rel="stylesheet" href="css/box.css">
    <!--背景の画像をページサイズに合わせるcss-->
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/focus-textbox.css">
    <link rel="stylesheet" href="css/search.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
    <header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <c:if test="${us == null}">
            <li><a href="Login" class="btn4">ログイン</a></li>
            </c:if>
            <c:if test="${us != null}">
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            </c:if>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <div align="center">
        <div class="box6"></div>

        <div>
            <table>
                <tr>
                    <td>
                        <img src="img/pekemontitol.png">
                    </td>
                </tr>
            </table>
        </div>
        <div id="back">
            <div align="center">
                <div class="box21">
					<table border="0" width="850" align="center">
						<c:forEach var="i" begin="0" end="${pokemonList.size()}" step="5">
							<tr align="center">
								<c:forEach var="j" begin="${i}" end="${i+4}" step="1">
									<c:if test="${j > pokemonList.size()}">
										<td></td>
									</c:if>
									<c:if test="${j < pokemonList.size()}">
										<td align="left"><a href="PokemonListDetail?allNumber=${pokemonList.get(j).allNumber}">
												${pokemonList.get(j).galarNumber}:<img class="icon32_sp"
												src="pokemonAllnum/${pokemonList.get(j).allNumber}.gif">
												${pokemonList.get(j).name}
										</a></td>
									</c:if>
								</c:forEach>
							</tr>
						</c:forEach>
					</table>
					<br>
				</div>
			</div>
		</div>
	</div>
</body>

</html>