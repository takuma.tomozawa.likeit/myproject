<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
    <meta charset="UTF-8">
    <title>投稿育成論削除画面</title>
    <style>
        body {
            background-image: url("img/p9.png");
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
        }
    </style>

	<link rel="stylesheet" href="css/icon.css">
    <link rel="stylesheet" href="css/box.css">
    <!--背景の画像をページサイズに合わせるcss-->
    <link rel="stylesheet" href="css/resizeimage.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/focus-textbox.css">
    <link rel="stylesheet" href="css/search.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
<header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>

    <div id="row-2st-in">
        <div class="box8">
        	<form action="TrainingArticleDelete" method="post">
        	<!-- trainingArticleData型のcreateIdをサーブレットのpostに送る -->
    		<input type="hidden" name="createId" value="${tad.createId}">
	            <div align="center">
	                <div>
	                    <b>育成論削除</b>
	                </div>
	                <p>以下の投稿した育成論を削除します</p>
	                <p>削除した情報の復元はできません</p>
	                <p>よろしいですか？</p>
	                <br>
	                <b>タイトル</b>
	                <div>${tad.title}</div>
	                <br>
	                <b>ポケモン</b>
	                <div><img class="icon32_sp-2" src="pokemonAllnum/${tad.pokemonId}.gif">${tad.name}</div>
	                <br>
	                <br>
	                <div>
	                    <input type="submit" value="はい" class="btn-gradient-3d-simple">
	                </div>
	                <br>
	                <div style="margin-right:300px">
	                    <a href="TrainingArticleList">
	                        戻る
	                    </a>
	                </div>
	            </div>
            </form>
        </div>
    </div>
</body>

</html>