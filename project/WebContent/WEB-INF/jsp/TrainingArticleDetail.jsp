<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
    <meta charset="UTF-8">
    <title>育成論閲覧画面</title>
    <style>
        body {
            background-image: url("img/p2.jpg");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/box.css">
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/input-form3.css">
    <link rel="stylesheet" href="css/search.css">
    <!--バックグラウンドのカラーを設定している-->
    <link rel="stylesheet" href="css/textarea.css">

</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
    <div class="text-white" id="wrapper">

        <div align="center">
            <div class="box23">
                <!--box+上の文字を中央に持ってくる-->
                <div class="center">
                    <table border="0" width="850">
                        <tr>
                            <td colspan="5" align="center" bgcolor="#ccffcc">
                                <h3>タイトル名:${tad.title}</h3>
                            </td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" bgcolor="#ccffcc">
                                <h4>ポケモン名:${tad.name}</h4>
                            </td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" bgcolor="#ccffcc">
                                <h4>特性:${tad.characteristic}</h4>
                            </td>
                        </tr>


                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" bgcolor="#ccffcc">
                                <h4>
                                技:

                                ${tad.skill1}/

                                ${tad.skill2}/

                                ${tad.skill3}/

                                ${tad.skill4}/
                                </h4>
                            </td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                    </table>
                    <table border="1">
                        <tr bgcolor="#f0fff0">
                            <td colspan="5" align="center"><h4>～投稿記事の内容～</h4><br>
                                ${tad.consideration}
                            </td>
                        </tr>

                    </table>
                    <div align="left">投稿者:${tad.userName}</div>
                    <div align="left">投稿日時:${tad.updateData}</div>
                    <br>
                    <div align="center">
                        <a href="TrainingArticleList">
                            戻る
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>

</html>