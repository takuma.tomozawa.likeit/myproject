<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
    <meta charset="UTF-8">
    <title>個人育成論管理画面</title>
    <style>
        body {
            background-image: url("img/p3.jpg");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>

    <link rel="stylesheet" href="css/icon.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/box.css">
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/input-form3.css">
    <link rel="stylesheet" href="css/search.css">
    <link rel="stylesheet" href="css/textarea.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
<header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <div align="center">
        <div class="box16"></div>
        <div class="box17">
        	<a href="TrainingArticle">
            <img src="img/trainedButton3.png">
            </a>

        </div>
        <div class="Box27">
            <div align="center">
                <h3><font face="ＭＳ ゴシック">投稿済み育成論一覧</font></h3>
                <table border="4" width="1100">

                    <tr bgcolor="#8d93c8">
                        <th>タイトル</th>
                        <th>ポケモンの名前</th>
                        <th>特性</th>
                        <th>努力値振り</th>
                        <th>持ち物</th>
                        <th></th>
                    </tr>

                    <c:forEach items="${taList}" var="tal">
	                    <tr bgcolor="#ffffff">
	                        <td  align="center">${tal.title}</td>
	                        <td>　<img class="icon32_sp-2" src="pokemonAllnum/${tal.pokemonId}.gif">${tal.name}</td>

	                        <td align="center">${tal.characteristic}</td>
	                        <td>　
	                        h${tal.hp_EffortValue}/
	                        a${tal.attack_EffortValue}/
	                        b${tal.defense_EffortValue}/
	                        c${tal.specialAttack_EffortValue}/
	                        d${tal.specialDefense_EffortValue}/
	                        s${tal.speed_EffortValue}
	                        </td>
	                        <td  align="center">${tal.item}</td>
	                        <td  align="center">
		                        	<span>
			                        	<a href="TrainingArticleDetail?createId=${tal.createId}">
		                                    <input type="button" value="詳細" class="btn-gradient-3d-simple">
		                                </a>
	                                </span>
		                            <span style="margin-left:10px">
			                            <a href="TrainingArticleUpdate?createId=${tal.createId}">
			                                <input type="button" value="更新" class="btn-gradient-3d-green-simple">
			                            </a>
		                            </span>
		                            <span style="margin-left:10px">
		                            	<a href="TrainingArticleDelete?createId=${tal.createId}">
		                                	<input type="button" value="削除" class="btn-gradient-3d-red-simple">
		                                </a>
		                            </span>

	                        </td>
	                    </tr>
                    </c:forEach>

                </table>
                <br>
                <div align="center">
                    <a href="TrainingArticleList">${page}</a>,
                </div>
            </div>
        </div>
    </div>
</body>

</html>