<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
<meta charset=UTF-8">
<title>育成ポケモン選択</title>

    <style>
        body {
            background-image: url("img/p3.jpg");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>

    <link rel="stylesheet" href="css/icon.css">
    <link rel="stylesheet" href="css/box.css">
    <!--背景の画像をページサイズに合わせるcss-->
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/focus-textbox.css">
    <link rel="stylesheet" href="css/search.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
<header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <div align="center">
        <div class="box16"></div>
        <div class="box17">
            <h1 align="center"><img src="img/mozi1.png"></h1>
        </div>
        <div class="Box10">
	        <table border="1" width="800">
	            <tr bgcolor="#a3d6cc">
	                <th></th>
	                <th>名前</th>
	                <th>ニックネーム</th>
	                <th>特性</th>
	                <th>ステータス</th>
	                <th>持ち物</th>
	            </tr>
 	            <c:if test="${ptList != null}">
		            <c:forEach items="${ptList}" var="ptl">
		                <tr  align="center" bgcolor="#e6e6fa">
		                    <td bgcolor="#f0e68c">
								<form action="TrainingArticlePokemonList" method="post">
			                    	<img class="icon32_sp-2" src="pokemonAllnum/${ptl.pokemonId}.gif">

						            <input type="submit" name="createId" value="${ptl.createId}" class="btn-gradient-3d-pokemon-simple">
						        </form>
		                    </td>
		                    <td bgcolor="#e6e6fa">
		                    	${ptl.name}
		                    </td>
		                    <td bgcolor="#e6e6fa">${ptl.nickname}</td>
		                    <td bgcolor="#e6e6fa">${ptl.characteristic}</td>
		                    <td bgcolor="#e6e6fa">
		                    ${ptl.hp_EffortValue}/
		                    ${ptl.attack_EffortValue}/
		                    ${ptl.defense_EffortValue}/
		                    ${ptl.specialAttack_EffortValue}/
		                    ${ptl.specialDefense_EffortValue}/
		                    ${ptl.speed_EffortValue}
		                    </td>
		                    <td bgcolor="#e6e6fa">${ptl.item}</td>
		                </tr>
		            </c:forEach>
	            </c:if>
	        </table>
            <br>
            <a href="TrainingArticle">戻る</a>
            <div align="center">
                <a href="TrainingArticlePokemonList">${page}</a>,
            </div>
        </div>




    </div>
</body>

</html>