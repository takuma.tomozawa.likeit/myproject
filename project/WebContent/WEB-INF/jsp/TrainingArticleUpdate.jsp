<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
    <meta charset="UTF-8">
    <title>育成論の修正画面</title>
    <style>
        body {
            background-image: url("img/p3.jpg");
            background-color: #e0ffff;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            background-attachment: fixed;
        }
    </style>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/box.css">
    <link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/input-form3.css">
    <link rel="stylesheet" href="css/search.css">
    <!--バックグラウンドのカラーを設定している-->
    <link rel="stylesheet" href="css/textarea.css">
    <link rel="stylesheet" href="css/checkbox.css">

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.1.0.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
<header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>
     <div align="center">
        <div class="box7"></div>
        <div class="box22">
            <h3 align="center">育成論の修正</h3>
			<c:if test="${error1 != null}" >
				<div class="alert alert-danger" role="alert">
				${error1}<br>
				${error2}
				</div>
			</c:if>
			<form action="TrainingArticleUpdate" method="post">
			<input type="hidden" name="createId" value="${tad.createId}">
	            <table border="0" width="800">
	                <div class="stuff1">
	                    <tr>
	                        <td colspan="6">
	                            <div class="textBox">
	                                <input class="text" type="textbox" name="title" placeholder="タイトル名" onkeyup="this.setAttribute('value', this.value);" value="${tad.title}">
	                                <label class="label">タイトル名</label>
	                                <label class="error"></label>
	                            </div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td colspan="3">
	                            <div class="textBox">
	                                <input class="text" type="textbox" name="name" placeholder="ポケモン名" onkeyup="this.setAttribute('value', this.value);" value="${tad.name}">
	                                <label class="label">ポケモン名</label>
	                                <label class="error"></label>
	                            </div>
	                        </td>
	                        <td colspan="3">
	                            <div class="textBox">
	                                <input class="text" type="textbox" name="characteristic" placeholder="特性" onkeyup="this.setAttribute('value', this.value);" value="${tad.characteristic}">
	                                <label class="label">特性</label>
	                                <label class="error"></label>
	                            </div>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td colspan="3">
		                        <div class="textBox">
	                            <input class="text" type="textbox" name="correction" placeholder="性格補正" list="correction" value="${tad.correction}">
		                            <datalist id="correction">
		                                <option value="" hidden disabled selected></option>
		                                <c:forEach items="${correctionList}" var="correctionList">
		                                    <option value="${correctionList.correction}">${correctionList.correction}</option>
		                                </c:forEach>
		                            </datalist><br>
	                            <label class="label">性格補正</label>
	                            <label class="error"></label>
	                            </div>
		                    </td>
		                    <td colspan="3">
		                        <div class="textBox">
		                            <input class="text" type="textbox" name="item" placeholder="持ち物" onkeyup="this.setAttribute('value', this.value);"
		                            value="${tad.item}">
		                            <label class="label">持ち物</label>
		                            <label class="error"></label>
		                        </div>
		                    </td>
		                </tr>
	                    <tr><td colspan="6"><br></td></tr>
	                    <tr>
	                        <td colspan="3">
	                            <div class="textBox">
	                                <input class="text" type="textbox" name="skill1" placeholder="技1" onkeyup="this.setAttribute('value', this.value);" value="${tad.skill1}">
	                                <label class="label">技1</label>
	                                <label class="error"></label>
	                            </div>
	                        </td>
	                        <td colspan="3">
	                            <div class="textBox">
	                                <input class="text" type="textbox" name="skill2" placeholder="技2" onkeyup="this.setAttribute('value', this.value);" value="${tad.skill2}">
	                                <label class="label">技2</label>
	                                <label class="error"></label>
	                            </div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td colspan="3">
	                            <div class="textBox">
	                                <input class="text" type="textbox" name="skill3" placeholder="技3" onkeyup="this.setAttribute('value', this.value);" value="${tad.skill3}">
	                                <label class="label">技3</label>
	                                <label class="error"></label>
	                            </div>
	                        </td>
	                        <td colspan="3">
	                            <div class="textBox">
	                                <input class="text" type="textbox" name="skill4" placeholder="技4" onkeyup="this.setAttribute('value', this.value);" value="${tad.skill4}">
	                                <label class="label">技4</label>
	                                <label class="error"></label>
	                            </div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td colspan="6"><br></td>
	                    </tr>
	                    <tr>
	                        <td colspan="6">努力値</td>
	                    </tr>
	                    <tr>
	                        <td>
	                                <div class="textBox">
	                                    <input class="text" type="number" name="hp_EffortValue" placeholder="H" onkeyup="this.setAttribute('value', this.value);"
	                                    value="${tad.hp_EffortValue}"/ min="0" max="252" list="num" value="">
	                                    <datalist id="num">
	                                        <option value="0">
	                                        <option value="100">
	                                        <option value="252">
	                                    </datalist><br>
	                                    <label class="label">H</label>
	                                    <label class="error"></label>
	                                </div>
	                        </td>
	                        <td>
	                                <div class="textBox">
	                                    <input class="text" type="number" name="attack_EffortValue" placeholder="A" onkeyup="this.setAttribute('value', this.value);"
	                                    value="${tad.attack_EffortValue}"/ min="0" max="252" list="num">
	                                    <datalist id="num">
	                                        <option value="0">
	                                        <option value="100">
	                                        <option value="252">
	                                    </datalist><br>
	                                    <label class="label">A</label>
	                                    <label class="error"></label>
	                                </div>
	                        </td>
	                        <td>
	                            <div class="textBox">
	                                <input class="text" type="number" name="defense_EffortValue" placeholder="B" onkeyup="this.setAttribute('value', this.value);"
	                                value="${tad.defense_EffortValue}"/ min="0" max="252" list="num">
	                                <datalist id="num">
	                                    <option value="0">
	                                    <option value="100">
	                                    <option value="252">
	                                </datalist><br>
	                                <label class="label">B</label>
	                                <label class="error"></label>
	                            </div>
	                        </td>
	                        <td>
	                            <div class="textBox">
	                                <input class="text" type="number" name="specialAttack_EffortValue" placeholder="C" onkeyup="this.setAttribute('value', this.value);"
	                                value="${tad.specialAttack_EffortValue}"/ min="0" max="252" list="num">
	                                <datalist id="num">
	                                    <option value="0">
	                                    <option value="100">
	                                    <option value="252">
	                                </datalist><br>
	                                <label class="label">C</label>
	                                <label class="error"></label>
	                            </div>
	                        </td>
	                        <td>
	                            <div class="textBox">
	                                <input class="text" type="number" name="specialDefense_EffortValue" placeholder="D" onkeyup="this.setAttribute('value', this.value);"
	                                value="${tad.specialDefense_EffortValue}"/ min="0" max="252" list="num">
	                                <datalist id="num">
	                                    <option value="0">
	                                    <option value="100">
	                                    <option value="252">
	                                </datalist><br>
	                                <label class="label">D</label>
	                                <label class="error"></label>
	                            </div>
	                        </td>
	                        <td>
	                            <div class="textBox">
	                                <input class="text" type="number" name="speed_EffortValue" placeholder="S" onkeyup="this.setAttribute('value', this.value);"
	                                value="${tad.speed_EffortValue}"/ min="0" max="252" list="num">
	                                <datalist id="num">
	                                    <option value="0">
	                                    <option value="100">
	                                    <option value="252">
	                                </datalist><br>
	                                <label class="label">S</label>
	                                <label class="error"></label>
	                            </div>

	                        </td>
	                    </tr>
	                    <td colspan="6">個体値（個体値がVのステータスにチェック）</td>
	                    <tr>
	                        <td>
	                            <div class="btn-group btn-group" data-toggle="buttons">
	                                <label class="btn active">
	                                    <input type="checkbox" name="hp_IndividualValue" checked><i class="fa fa-square-o fa-2x"></i><i class="fa fa-check-square-o fa-2x"></i><span>H
	                                </label>
	                            </div>
	                        </td>
	                        <td>
	                            <div class="btn-group btn-group" data-toggle="buttons">
	                                <label class="btn active">
	                                    <input type="checkbox" name="attack_IndividualValue" checked><i class="fa fa-square-o fa-2x"></i><i class="fa fa-check-square-o fa-2x"></i><span>A
	                                </label>
	                            </div>
	                        </td>
	                        <td>
	                            <div class="btn-group btn-group" data-toggle="buttons">
	                                <label class="btn active">
	                                    <input type="checkbox" name="defense_IndividualValue" checked><i class="fa fa-square-o fa-2x"></i><i class="fa fa-check-square-o fa-2x"></i><span>B
	                                </label>
	                            </div>
	                        </td>
	                        <td>
	                            <div class="btn-group btn-group" data-toggle="buttons">
	                                <label class="btn active">
	                                    <input type="checkbox" name="specialAttack_IndividualValue" checked><i class="fa fa-square-o fa-2x"></i><i class="fa fa-check-square-o fa-2x"></i><span>C
	                                </label>
	                            </div>
	                        </td>
	                        <td>
	                            <div class="btn-group btn-group" data-toggle="buttons">
	                                <label class="btn active">
	                                    <input type="checkbox" name="specialDefense_IndividualValue" checked><i class="fa fa-square-o fa-2x"></i><i class="fa fa-check-square-o fa-2x"></i><span>D
	                                </label>
	                            </div>
	                        </td>
	                        <td>
	                            <div class="btn-group btn-group" data-toggle="buttons">
	                                <label class="btn active">
	                                    <input type="checkbox" name="speed_IndividualValue" checked><i class="fa fa-square-o fa-2x"></i><i class="fa fa-check-square-o fa-2x"></i><span>S
	                                </label>
	                            </div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td colspan="6"><br></td>
	                    </tr>
	                    <tr>
	                        <td colspan="6" align="center">下記の考察欄に育成論の詳細、対戦方針、他のポケモンとの比較などをご自由にお書きください。</td>
	                    </tr>
	                    <tr>
	                        <td colspan="6">
	                            <div class="formBox">
	                                <span class="item-name">考察</span>
	                                <div class="textarea-wrap">
	                                    <textarea name="consideration" rows="100" cols="70">${tad.consideration}</textarea>
	                                </div>
	                            </div>
	                        </td>
	                    </tr>
	                    <td colspan="6"><br></td>
	                    <tr>
	                        <td colspan="6">
	                            <div align="center">
	                                <input type="submit" value="更新" class="btn-gradient-3d-green-simple">
	                            </div>
	                        </td>
	                    </tr>
	                </div>
	            </table>
            </form>
        </div>
    </div>
</body>

</html>