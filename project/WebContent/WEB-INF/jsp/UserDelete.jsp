<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザ情報削除</title>
    <style>
        body {
            background-image: url("img/p9.png");
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
        }
    </style>

    <link rel="stylesheet" href="css/box.css">
    <!--背景の画像をページサイズに合わせるcss-->
    <link rel="stylesheet" href="css/resizeimage.css">
    <link rel="stylesheet" href="css/example.css">
    <!--ヘッダーの色-->
    <link rel="stylesheet" href="css/header.css">
    <!--ヘッダーのメニューに該当する物の並びを横に変更-->
    <link rel="stylesheet" href="css/ul.css">
    <!--メニューに該当する物のアンダーバーのデザイン-->
    <link rel="stylesheet" href="css/btn.css">
    <link rel="stylesheet" href="css/btn-gradient.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/focus-textbox.css">
    <link rel="stylesheet" href="css/search.css">
</head>

<body link="#0073a8" vlink="#0073a8" alink="#8fbc8f">
<header class="cooler  fixed">
        <!--ヘッダーメニュー-->
        <ul>
            <li><a href="Top" class="btn4">トップ(ポケモンを探す)</a></li>
            <li><a href="MyPage" class="btn4">マイページ</a></li>
            <!--ログインしていない状態なら個々の表記はサインインに変更する-->
            <li><a href="Logout" class="btn4">ログアウト</a></li>
            <li><a href="ThisSiteDetail" class="btn4">このサイトについて</a></li>
            <li><a href="UserDetail?id=${userInfo.id}" class="btn4"><img src="gif/547.gif"></a></li>
            <li>
                <div class="box9">
                    <form action="Search" name="search" method="post">
                        <dl class="search1">
                            <dt>
                                <input type="search" name="search" value="" placeholder="ポケモンの名前を入力">

                            </dt>
                            <dd>
                                <button>
                                    <span><img src="img/searchIcon025.png"></span>
                                </button>
                            </dd>
                        </dl>
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <form action="UserDelete" method="post">

    <input type="hidden" name="id" value="${userDetail.loginId}">
	    <div id="row-2st-in">
	        <div class="box8">

	            <div align="center">
	                <div>
	                    <b>ユーザ削除</b>
	                </div>
	                <p>以下のユーザ情報を削除します</p>
	                <p>削除した情報の復元はできません</p>
	                <p>よろしいですか？</p>
	                <br>
	                <b>ログインID</b>
	                <div>${userDetail.loginId}</div>
	                <br>
	                <b>ユーザ名</b>
	                <div>${userDetail.name}</div>
	                <br>
	                <b>登録日</b>
	                <div>${userDetail.createData}</div>
	                <br>
	                <br>

	                <div>
	                    <input type="submit" value="はい" class="btn-gradient-3d-simple">
	                </div>

	                <br>
	                <div style="margin-right:300px">
	                    <a href="UserList">
	                        戻る
	                    </a>
	                </div>
	            </div>
	        </div>
	    </div>
    </form>
</body>

</html>