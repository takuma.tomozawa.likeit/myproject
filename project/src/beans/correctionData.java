package beans;

import java.io.Serializable;

public class correctionData implements Serializable {
	private int id;
	private String correction;

	public correctionData(int id, String correction) {
		this.id = id;
		this.correction = correction;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCorrection() {
		return correction;
	}

	public void setCorrection(String correction) {
		this.correction = correction;
	}



}
