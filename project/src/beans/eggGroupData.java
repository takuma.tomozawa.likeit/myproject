package beans;

import java.io.Serializable;

public class eggGroupData implements Serializable{
	private int id;
	private String eggGroup;


	public eggGroupData(int id, String eggGroup) {
		this.id = id;
		this.eggGroup = eggGroup;
	}

	public eggGroupData(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEggGroup() {
		return eggGroup;
	}

	public void setEggGroup(String eggGroup) {
		this.eggGroup = eggGroup;
	}

}

