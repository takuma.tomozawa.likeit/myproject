package beans;

import java.io.Serializable;

public class pokemonBasicData implements Serializable{
	private int id;
	private String allNumber;
	private int galarNumber;
	private String name;
	private String type1;
	private String type2;
	private String characteristic1;
	private String characteristic2;
	private String characteristicSecret;
	private double whight;
	private String eggGroup1;
	private String eggGroup2;
	private int hp;
	private int attack;
	private int defense;
	private int specialAttack;
	private int specialDefense;
	private int speed;


	private int hp_v_max;
	private int hp_v_none;
	private int hp_0_none;

	private int attack_up_v_max;
	private int attack_v_max;
	private int attack_v_none;
	private int attack_down_v_none;
	private int attack_down_0_none;

	private int defense_up_v_max;
	private int defense_v_max;
	private int defense_v_none;
	private int defense_down_v_none;
	private int defense_down_0_none;

	private int specialAttack_up_v_max;
	private int specialAttack_v_max;
	private int specialAttack_v_none;
	private int specialAttack_down_v_none;
	private int specialAttack_down_0_none;

	private int specialDefense_up_v_max;
	private int specialDefense_v_max;
	private int specialDefense_v_none;
	private int specialDefense_down_v_none;
	private int specialDefense_down_0_none;

	private int speed_up_v_max;
	private int speed_v_max;
	private int speed_v_none;
	private int speed_down_v_none;
	private int speed_down_0_none;

	public pokemonBasicData(int id, String allNumber, int galarNumber, String name, String type1, String type2,
			String characteristic1, String characteristic2, String characteristicSecret, double whight, String eggGroup1,
			String eggGroup2, int hp, int attack, int defense, int specialAttack, int specialDefense, int speed) {
		super();
		this.id = id;
		this.allNumber = allNumber;
		this.galarNumber = galarNumber;
		this.name = name;
		this.type1 = type1;
		this.type2 = type2;
		this.characteristic1 = characteristic1;
		this.characteristic2 = characteristic2;
		this.characteristicSecret = characteristicSecret;
		this.whight = whight;
		this.eggGroup1 = eggGroup1;
		this.eggGroup2 = eggGroup2;
		this.hp = hp;
		this.attack = attack;
		this.defense = defense;
		this.specialAttack = specialAttack;
		this.specialDefense = specialDefense;
		this.speed = speed;
	}

	public pokemonBasicData(int hp ,int attack ,int defense ,int specialAttack ,int specialDefense ,int speed) {
		this.hp = hp;
		this.attack = attack;
		this.defense = defense;
		this.specialAttack = specialAttack;
		this.specialDefense = specialDefense;
		this.speed = speed;
	}


	public pokemonBasicData(int id, String allNumber, int galarNumber, String name, String type1, String type2,
			String characteristic1, String characteristic2, String characteristicSecret, double whight,
			String eggGroup1, String eggGroup2, int hp, int attack, int defense, int specialAttack,
			int specialDefense, int speed,

			int hp_v_max, int hp_v_none, int hp_0_none,

			int attack_up_v_max, int attack_v_max, int attack_v_none, int attack_down_v_none, int attack_down_0_none,

			int defense_up_v_max, int defense_v_max, int defense_v_none, int defense_down_v_none, int defense_down_0_none,

			int specialAttack_up_v_max, int specialAttack_v_max, int specialAttack_v_none, int specialAttack_down_v_none,int specialAttack_down_0_none,

			int specialDefense_up_v_max, int specialDefense_v_max, int specialDefense_v_none, int specialDefense_down_v_none, int specialDefense_down_0_none,

			int speed_up_v_max, int speed_v_max, int speed_v_none, int speed_down_v_none, int speed_down_0_none) {

		this.id = id;
		this.allNumber = allNumber;
		this.galarNumber = galarNumber;
		this.name = name;
		this.type1 = type1;
		this.type2 = type2;
		this.characteristic1 = characteristic1;
		this.characteristic2 = characteristic2;
		this.characteristicSecret = characteristicSecret;
		this.whight = whight;
		this.eggGroup1 = eggGroup1;
		this.eggGroup2 = eggGroup2;
		this.hp = hp;
		this.attack = attack;
		this.defense = defense;
		this.specialAttack = specialAttack;
		this.specialDefense = specialDefense;
		this.speed = speed;

		this.hp_v_max = hp_v_max;
		this.hp_v_none = hp_v_none;
		this.hp_0_none = hp_0_none;

		this.attack_up_v_max = attack_up_v_max;
		this.attack_v_max = attack_v_max;
		this.attack_v_none = attack_v_none;
		this.attack_down_v_none = attack_down_v_none;
		this.attack_down_0_none = attack_down_0_none;

		this.defense_up_v_max = defense_up_v_max;
		this.defense_v_max = defense_v_max;
		this.defense_v_none = defense_v_none;
		this.defense_down_v_none = defense_down_v_none;
		this.defense_down_0_none = defense_down_0_none;

		this.specialAttack_up_v_max = specialAttack_up_v_max;
		this.specialAttack_v_max = specialAttack_v_max;
		this.specialAttack_v_none = specialAttack_v_none;
		this.specialAttack_down_v_none = specialAttack_down_v_none;
		this.specialAttack_down_0_none = specialAttack_down_0_none;

		this.specialDefense_up_v_max = specialDefense_up_v_max;
		this.specialDefense_v_max = specialDefense_v_max;
		this.specialDefense_v_none = specialDefense_v_none;
		this.specialDefense_down_v_none = specialDefense_down_v_none;
		this.specialDefense_down_0_none = specialDefense_down_0_none;

		this.speed_up_v_max = speed_up_v_max;
		this.speed_v_max = speed_v_max;
		this.speed_v_none = speed_v_none;
		this.speed_down_v_none = speed_down_v_none;
		this.speed_down_0_none = speed_down_0_none;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAllNumber() {
		return allNumber;
	}
	public void setAllNumber(String allNumber) {
		this.allNumber = allNumber;
	}
	public int getGalarNumber() {
		return galarNumber;
	}
	public void setGalarNumber(int galarNumber) {
		this.galarNumber = galarNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType1() {
		return type1;
	}
	public void setType1(String type1) {
		this.type1 = type1;
	}
	public String getType2() {
		return type2;
	}
	public void setType2(String type2) {
		this.type2 = type2;
	}
	public String getCharacteristic1() {
		return characteristic1;
	}
	public void setCharacteristic1(String characteristic1) {
		this.characteristic1 = characteristic1;
	}
	public String getCharacteristic2() {
		return characteristic2;
	}
	public void setCharacteristic2(String characteristic2) {
		this.characteristic2 = characteristic2;
	}
	public String getCharacteristicSecret() {
		return characteristicSecret;
	}
	public void setCharacteristicSecret(String characteristicSecret) {
		this.characteristicSecret = characteristicSecret;
	}
	public double getWhight() {
		return whight;
	}
	public void setWhight(double whight) {
		this.whight = whight;
	}
	public String getEggGroup1() {
		return eggGroup1;
	}
	public void setEggGroup1(String eggGroup1) {
		this.eggGroup1 = eggGroup1;
	}
	public String getEggGroup2() {
		return eggGroup2;
	}
	public void setEggGroup2(String eggGroup2) {
		this.eggGroup2 = eggGroup2;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getAttack() {
		return attack;
	}
	public void setAttack(int attack) {
		this.attack = attack;
	}
	public int getDefense() {
		return defense;
	}
	public void setDefense(int defense) {
		this.defense = defense;
	}
	public int getSpecialAttack() {
		return specialAttack;
	}
	public void setSpecialAttack(int specialAttack) {
		this.specialAttack = specialAttack;
	}
	public int getSpecialDefense() {
		return specialDefense;
	}
	public void setSpecialDefense(int specialDefense) {
		this.specialDefense = specialDefense;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}


	public int getHp_v_max() {
		return hp_v_max;
	}

	public void setHp_v_max(int hp_v_max) {
		this.hp_v_max = hp_v_max;
	}

	public int getHp_v_none() {
		return hp_v_none;
	}

	public void setHp_v_none(int hp_v_none) {
		this.hp_v_none = hp_v_none;
	}

	public int getHp_0_none() {
		return hp_0_none;
	}

	public void setHp_0_none(int hp_0_none) {
		this.hp_0_none = hp_0_none;
	}

	public int getAttack_up_v_max() {
		return attack_up_v_max;
	}

	public void setAttack_up_v_max(int attack_up_v_max) {
		this.attack_up_v_max = attack_up_v_max;
	}

	public int getAttack_v_max() {
		return attack_v_max;
	}

	public void setAttack_v_max(int attack_v_max) {
		this.attack_v_max = attack_v_max;
	}

	public int getAttack_v_none() {
		return attack_v_none;
	}

	public void setAttack_v_none(int attack_v_none) {
		this.attack_v_none = attack_v_none;
	}

	public int getAttack_down_v_none() {
		return attack_down_v_none;
	}

	public void setAttack_down_v_none(int attack_down_v_none) {
		this.attack_down_v_none = attack_down_v_none;
	}

	public int getAttack_down_0_none() {
		return attack_down_0_none;
	}

	public void setAttack_down_0_none(int attack_down_0_none) {
		this.attack_down_0_none = attack_down_0_none;
	}

	public int getDefense_up_v_max() {
		return defense_up_v_max;
	}

	public void setDefense_up_v_max(int defense_up_v_max) {
		this.defense_up_v_max = defense_up_v_max;
	}

	public int getDefense_v_max() {
		return defense_v_max;
	}

	public void setDefense_v_max(int defense_v_max) {
		this.defense_v_max = defense_v_max;
	}

	public int getDefense_v_none() {
		return defense_v_none;
	}

	public void setDefense_v_none(int defense_v_none) {
		this.defense_v_none = defense_v_none;
	}

	public int getDefense_down_v_none() {
		return defense_down_v_none;
	}

	public void setDefense_down_v_none(int defense_down_v_none) {
		this.defense_down_v_none = defense_down_v_none;
	}

	public int getDefense_down_0_none() {
		return defense_down_0_none;
	}

	public void setDefense_down_0_none(int defense_down_0_none) {
		this.defense_down_0_none = defense_down_0_none;
	}

	public int getSpecialAttack_up_v_max() {
		return specialAttack_up_v_max;
	}

	public void setSpecialAttack_up_v_max(int specialAttack_up_v_max) {
		this.specialAttack_up_v_max = specialAttack_up_v_max;
	}

	public int getSpecialAttack_v_max() {
		return specialAttack_v_max;
	}

	public void setSpecialAttack_v_max(int specialAttack_v_max) {
		this.specialAttack_v_max = specialAttack_v_max;
	}

	public int getSpecialAttack_v_none() {
		return specialAttack_v_none;
	}

	public void setSpecialAttack_v_none(int specialAttack_v_none) {
		this.specialAttack_v_none = specialAttack_v_none;
	}

	public int getSpecialAttack_down_v_none() {
		return specialAttack_down_v_none;
	}

	public void setSpecialAttack_down_v_none(int specialAttack_down_v_none) {
		this.specialAttack_down_v_none = specialAttack_down_v_none;
	}

	public int getSpecialAttack_down_0_none() {
		return specialAttack_down_0_none;
	}

	public void setSpecialAttack_down_0_none(int specialAttack_down_0_none) {
		this.specialAttack_down_0_none = specialAttack_down_0_none;
	}

	public int getSpecialDefense_up_v_max() {
		return specialDefense_up_v_max;
	}

	public void setSpecialDefense_up_v_max(int specialDefense_up_v_max) {
		this.specialDefense_up_v_max = specialDefense_up_v_max;
	}

	public int getSpecialDefense_v_max() {
		return specialDefense_v_max;
	}

	public void setSpecialDefense_v_max(int specialDefense_v_max) {
		this.specialDefense_v_max = specialDefense_v_max;
	}

	public int getSpecialDefense_v_none() {
		return specialDefense_v_none;
	}

	public void setSpecialDefense_v_none(int specialDefense_v_none) {
		this.specialDefense_v_none = specialDefense_v_none;
	}

	public int getSpecialDefense_down_v_none() {
		return specialDefense_down_v_none;
	}

	public void setSpecialDefense_down_v_none(int specialDefense_down_v_none) {
		this.specialDefense_down_v_none = specialDefense_down_v_none;
	}

	public int getSpecialDefense_down_0_none() {
		return specialDefense_down_0_none;
	}

	public void setSpecialDefense_down_0_none(int specialDefense_down_0_none) {
		this.specialDefense_down_0_none = specialDefense_down_0_none;
	}

	public int getSpeed_up_v_max() {
		return speed_up_v_max;
	}

	public void setSpeed_up_v_max(int speed_up_v_max) {
		this.speed_up_v_max = speed_up_v_max;
	}

	public int getSpeed_v_max() {
		return speed_v_max;
	}

	public void setSpeed_v_max(int speed_v_max) {
		this.speed_v_max = speed_v_max;
	}

	public int getSpeed_v_none() {
		return speed_v_none;
	}

	public void setSpeed_v_none(int speed_v_none) {
		this.speed_v_none = speed_v_none;
	}

	public int getSpeed_down_v_none() {
		return speed_down_v_none;
	}

	public void setSpeed_down_v_none(int speed_down_v_none) {
		this.speed_down_v_none = speed_down_v_none;
	}

	public int getSpeed_down_0_none() {
		return speed_down_0_none;
	}

	public void setSpeed_down_0_none(int speed_down_0_none) {
		this.speed_down_0_none = speed_down_0_none;
	}


}
