package beans;

import java.io.Serializable;

public class pokemonTrainingData implements Serializable {
	private int id;
	private int userId;
	private String name;
	private String pokemonId;
	private String nickname;
	private String correction;
	private String item;
	private String characteristic;


	private String skill1;//技名
	private String skill2;
	private String skill3;
	private String skill4;


	private int hp_EffortValue;//努力値
	private int attack_EffortValue;
	private int defense_EffortValue;
	private int specialAttack_EffortValue;
	private int specialDefense_EffortValue;
	private int speed_EffortValue;

	private String hp_IndividualValue;//個体値
	private String attack_IndividualValue;
	private String defense_IndividualValue;
	private String specialAttack_IndividualValue;
	private String specialDefense_IndividualValue;
	private String speed_IndividualValue;

	private String remarks;//備考

	private int createId;

	private int hp;//種族値、個体値、努力値を基に計算した実数値
	private int attack;
	private int defense;
	private int specialAttack;
	private int specialDefense;
	private int speed;



	public pokemonTrainingData(int createId) {
		this.createId = createId;
	}

	public pokemonTrainingData(int id, String name,String pokemonId, int userId, String nickname,  String characteristic, String item,
			int hp_EffortValue, int attack_EffortValue, int defense_EffortValue, int specialAttack_EffortValue, int specialDefense_EffortValue,
			int speed_EffortValue, int createId) {

		this.id = id;
		this.userId =userId;
		this.name = name;
		this.pokemonId = pokemonId;
		this.nickname = nickname;
		this.characteristic = characteristic;
		this.item = item;

		this.hp_EffortValue = hp_EffortValue;
		this.attack_EffortValue = attack_EffortValue;
		this.defense_EffortValue = defense_EffortValue;
		this.specialAttack_EffortValue = specialAttack_EffortValue;
		this.specialDefense_EffortValue = specialDefense_EffortValue;
		this.speed_EffortValue = speed_EffortValue;
		this.createId = createId;
	}

		public pokemonTrainingData(String pokemonId, String nickname, String characteristic,String correction, String item,
				String skill1, String skill2, String skill3, String skill4,
				String hp_IndividualValue, String attack_IndividualValue, String defense_IndividualValue,
				String specialAttack_IndividualValue, String specialDefense_IndividualValue, String speed_IndividualValue,
				String remarks, int createId, int hp, int attack, int defense, int specialAttack, int specialDefense, int speed) {

			this.nickname = nickname;
			this.characteristic = characteristic;
			this.pokemonId = pokemonId;
			this.correction = correction;

			this.skill1 = skill1;
			this.skill2 = skill2;
			this.skill3 = skill3;
			this.skill4 = skill4;

			this.hp_IndividualValue = hp_IndividualValue;
			this.attack_IndividualValue = attack_IndividualValue;
			this.defense_IndividualValue = defense_IndividualValue;
			this.specialAttack_IndividualValue = specialAttack_IndividualValue;
			this.specialDefense_IndividualValue = specialDefense_IndividualValue;
			this.speed_IndividualValue = speed_IndividualValue;

			this.remarks = remarks;
			this.createId = createId;

			this.hp = hp;
			this.attack = attack;
			this.defense = defense;
			this.specialAttack = specialAttack;
			this.specialDefense = specialDefense;
			this.speed = speed;

	}

		public pokemonTrainingData(String pokemonId, String nickname, String characteristic,String correction, String item,
				String skill1, String skill2, String skill3, String skill4,
				int hp_EffortValue, int attack_EffortValue, int defense_EffortValue, int specialAttack_EffortValue,
				int specialDefense_EffortValue,int speed_EffortValue,
				String hp_IndividualValue, String attack_IndividualValue, String defense_IndividualValue,
				String specialAttack_IndividualValue, String specialDefense_IndividualValue, String speed_IndividualValue,
				String remarks, int createId) {

			this.nickname = nickname;
			this.characteristic = characteristic;
			this.pokemonId = pokemonId;
			this.correction = correction;
			this.item = item;

			this.skill1 = skill1;
			this.skill2 = skill2;
			this.skill3 = skill3;
			this.skill4 = skill4;

			this.hp_EffortValue = hp_EffortValue;
			this.attack_EffortValue = attack_EffortValue;
			this.defense_EffortValue = defense_EffortValue;
			this.specialAttack_EffortValue = specialAttack_EffortValue;
			this.specialDefense_EffortValue = specialDefense_EffortValue;
			this.speed_EffortValue = speed_EffortValue;

			this.hp_IndividualValue = hp_IndividualValue;
			this.attack_IndividualValue = attack_IndividualValue;
			this.defense_IndividualValue = defense_IndividualValue;
			this.specialAttack_IndividualValue = specialAttack_IndividualValue;
			this.specialDefense_IndividualValue = specialDefense_IndividualValue;
			this.speed_IndividualValue = speed_IndividualValue;

			this.remarks = remarks;
			this.createId = createId;

	}

		public pokemonTrainingData(String name, String nickname, String pokemonId, int createId) {

			this.pokemonId = pokemonId;
			this.nickname = nickname;
			this.name = name;
			this.createId = createId;
		}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPokemonId() {
		return pokemonId;
	}
	public void setPokemonId(String pokemonId) {
		this.pokemonId = pokemonId;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getCharacteristic() {
		return characteristic;
	}
	public void setCharacteristic(String characteristic) {
		this.characteristic = characteristic;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getSkill1() {
		return skill1;
	}
	public void setSkill1(String skill1) {
		this.skill1 = skill1;
	}
	public String getSkill2() {
		return skill2;
	}
	public void setSkill2(String skill2) {
		this.skill2 = skill2;
	}
	public String getSkill3() {
		return skill3;
	}
	public void setSkill3(String skill3) {
		this.skill3 = skill3;
	}
	public String getSkill4() {
		return skill4;
	}
	public void setSkill4(String skill4) {
		this.skill4 = skill4;
	}
	public int getHp_EffortValue() {
		return hp_EffortValue;
	}
	public void setHp_EffortValue(int hp_EffortValue) {
		this.hp_EffortValue = hp_EffortValue;
	}
	public int getAttack_EffortValue() {
		return attack_EffortValue;
	}
	public void setAttack_EffortValue(int attack_EffortValue) {
		this.attack_EffortValue = attack_EffortValue;
	}
	public int getDefense_EffortValue() {
		return defense_EffortValue;
	}
	public void setDefense_EffortValue(int defense_EffortValue) {
		this.defense_EffortValue = defense_EffortValue;
	}
	public int getSpecialAttack_EffortValue() {
		return specialAttack_EffortValue;
	}
	public void setSpecialAttack_EffortValue(int specialAttack_EffortValue) {
		this.specialAttack_EffortValue = specialAttack_EffortValue;
	}
	public int getSpecialDefense_EffortValue() {
		return specialDefense_EffortValue;
	}
	public void setSpecialDefense_EffortValue(int specialDefense_EffortValue) {
		this.specialDefense_EffortValue = specialDefense_EffortValue;
	}
	public int getSpeed_EffortValue() {
		return speed_EffortValue;
	}
	public void setSpeed_EffortValue(int speed_EffortValue) {
		this.speed_EffortValue = speed_EffortValue;
	}
	public String getHp_IndividualValue() {
		return hp_IndividualValue;
	}
	public void setHp_IndividualValue(String hp_IndividualValue) {
		this.hp_IndividualValue = hp_IndividualValue;
	}
	public String getAttack_IndividualValue() {
		return attack_IndividualValue;
	}
	public void setAttack_IndividualValue(String attack_IndividualValue) {
		this.attack_IndividualValue = attack_IndividualValue;
	}
	public String getDefense_IndividualValue() {
		return defense_IndividualValue;
	}
	public void setDefense_IndividualValue(String defense_IndividualValue) {
		this.defense_IndividualValue = defense_IndividualValue;
	}
	public String getSpecialAttack_IndividualValue() {
		return specialAttack_IndividualValue;
	}
	public void setSpecialAttack_IndividualValue(String specialAttack_IndividualValue) {
		this.specialAttack_IndividualValue = specialAttack_IndividualValue;
	}
	public String getSpecialDefense_IndividualValue() {
		return specialDefense_IndividualValue;
	}
	public void setSpecialDefense_IndividualValue(String specialDefense_IndividualValue) {
		this.specialDefense_IndividualValue = specialDefense_IndividualValue;
	}
	public String getSpeed_IndividualValue() {
		return speed_IndividualValue;
	}
	public void setSpeed_IndividualValue(String speed_IndividualValue) {
		this.speed_IndividualValue = speed_IndividualValue;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCorrection() {
		return correction;
	}
	public void setCorrection(String correction) {
		this.correction = correction;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getSpecialAttack() {
		return specialAttack;
	}

	public void setSpecialAttack(int specialAttack) {
		this.specialAttack = specialAttack;
	}

	public int getSpecialDefense() {
		return specialDefense;
	}

	public void setSpecialDefense(int specialDefense) {
		this.specialDefense = specialDefense;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getCreateId() {
		return createId;
	}

	public void setCreateId(int createId) {
		this.createId = createId;
	}



}
