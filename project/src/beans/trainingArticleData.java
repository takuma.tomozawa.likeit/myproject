package beans;

public class trainingArticleData {
	private int id;//作成順
	private String userId;//ユーザ識別用
	private String pokemonId;
	private String name;
	private String updateData;//作成日時
	private String title;
	private String characteristic;//特性
	private String correction;//性格
	private String item;

	private String skill1;
	private String skill2;
	private String skill3;
	private String skill4;

	private String hp_EffortValue;//努力値
	private String attack_EffortValue;
	private String defense_EffortValue;
	private String specialAttack_EffortValue;
	private String specialDefense_EffortValue;
	private String speed_EffortValue;

	private String hp_IndividualValue;//個体値
	private String attack_IndividualValue;
	private String defense_IndividualValue;
	private String specialAttack_IndividualValue;
	private String specialDefense_IndividualValue;
	private String speed_IndividualValue;

	private String consideration;//考察

	private int hp;//種族値、個体値、努力値を基に計算した実数値
	private int attack;
	private int defense;
	private int specialAttack;
	private int specialDefense;
	private int speed;

	private int createId;
	private String userName;

	public trainingArticleData(String userId, String name, String title,
			String characteristic, String correction, String item, String skill1, String skill2, String skill3, String skill4,
			String hp_EffortValue, String attack_EffortValue,
			String defense_EffortValue, String specialAttack_EffortValue, String specialDefense_EffortValue,
			String speed_EffortValue, String hp_IndividualValue, String attack_IndividualValue,
			String defense_IndividualValue, String specialAttack_IndividualValue, String specialDefense_IndividualValue,
			String speed_IndividualValue, String consideration) {
		this.userId = userId;
		this.name = name;
		this.title = title;
		this.characteristic = characteristic;
		this.correction = correction;
		this.item = item;

		this.skill1 = skill1;
		this.skill2 = skill2;
		this.skill3 = skill3;
		this.skill4 = skill4;

		this.hp_EffortValue = hp_EffortValue;
		this.attack_EffortValue = attack_EffortValue;
		this.defense_EffortValue = defense_EffortValue;
		this.specialAttack_EffortValue = specialAttack_EffortValue;
		this.specialDefense_EffortValue = specialDefense_EffortValue;
		this.speed_EffortValue = speed_EffortValue;

		this.hp_IndividualValue = hp_IndividualValue;
		this.attack_IndividualValue = attack_IndividualValue;
		this.defense_IndividualValue = defense_IndividualValue;
		this.specialAttack_IndividualValue = specialAttack_IndividualValue;
		this.specialDefense_IndividualValue = specialDefense_IndividualValue;
		this.speed_IndividualValue = speed_IndividualValue;

		this.consideration = consideration;
	}

	public trainingArticleData(String userId, String pokemonId, String name, String title,
			String characteristic,String item, String hp_EffortValue, String attack_EffortValue,
			String defense_EffortValue, String specialAttack_EffortValue, String specialDefense_EffortValue,
			String speed_EffortValue, int createId
	) {
		this.userId = userId;
		this.pokemonId = pokemonId;
		this.name = name;
		this.title = title;
		this.characteristic = characteristic;
		this.item = item;

		this.hp_EffortValue = hp_EffortValue;
		this.attack_EffortValue = attack_EffortValue;
		this.defense_EffortValue = defense_EffortValue;
		this.specialAttack_EffortValue = specialAttack_EffortValue;
		this.specialDefense_EffortValue = specialDefense_EffortValue;
		this.speed_EffortValue = speed_EffortValue;

		this.createId = createId;

	}

	public trainingArticleData(String userId, String name, String updateData, String title, String pokemonId,
			String characteristic, String correction, String item, String skill1, String skill2, String skill3, String skill4,
			String hp_EffortValue, String attack_EffortValue,
			String defense_EffortValue, String specialAttack_EffortValue, String specialDefense_EffortValue,
			String speed_EffortValue, String hp_IndividualValue, String attack_IndividualValue,
			String defense_IndividualValue, String specialAttack_IndividualValue, String specialDefense_IndividualValue,
			String speed_IndividualValue, String consideration, int hp, int attack, int defense, int specialAttack,
			int specialDefense, int speed, int getId, String userName) {
		this.userId = userId;
		this.name = name;
		this.updateData = updateData;
		this.title = title;
		this.pokemonId = pokemonId;
		this.characteristic = characteristic;
		this.correction = correction;
		this.item = item;

		this.skill1 = skill1;
		this.skill2 = skill2;
		this.skill3 = skill3;
		this.skill4 = skill4;

		this.hp_EffortValue = hp_EffortValue;
		this.attack_EffortValue = attack_EffortValue;
		this.defense_EffortValue = defense_EffortValue;
		this.specialAttack_EffortValue = specialAttack_EffortValue;
		this.specialDefense_EffortValue = specialDefense_EffortValue;
		this.speed_EffortValue = speed_EffortValue;

		this.hp_IndividualValue = hp_IndividualValue;
		this.attack_IndividualValue = attack_IndividualValue;
		this.defense_IndividualValue = defense_IndividualValue;
		this.specialAttack_IndividualValue = specialAttack_IndividualValue;
		this.specialDefense_IndividualValue = specialDefense_IndividualValue;
		this.speed_IndividualValue = speed_IndividualValue;

		this.consideration = consideration;

		this.hp = hp;
		this.attack = attack;
		this.defense = defense;
		this.specialAttack = specialAttack;
		this.specialDefense = specialDefense;
		this.speed = speed;

		this.createId = getId;
		this.userName = userName;
	}

	//allUserArticleTrainingListで使用
	public trainingArticleData(int id,  String userId, String pokemonId, String name, String update, String title, String characteristic,
			String correction, String item, String skill1, String skill2, String skill3, String skill4,
			String h_ev, String a_ev, String b_ev, String c_ev, String d_ev, String s_ev, String h_iv, String a_iv, String b_iv,
			String c_iv, String d_iv, String s_iv, String consideration, int getId, String userName,
			int hp, int attack, int defense, int specialAttack, int specialDefense, int speed) {

		this.id = id;
		this.userId = userId;
		this.pokemonId = pokemonId;
		this.name = name;
		this.updateData = update;
		this.title = title;
		this.characteristic = characteristic;
		this.correction = correction;
		this.item = item;

		this.skill1 = skill1;
		this.skill2 = skill2;
		this.skill3 = skill3;
		this.skill4 = skill4;

		this.hp_EffortValue = h_ev;
		this.attack_EffortValue = a_ev;
		this.defense_EffortValue = b_ev;
		this.specialAttack_EffortValue = c_ev;
		this.specialDefense_EffortValue = d_ev;
		this.speed_EffortValue = s_ev;

		this.hp_IndividualValue = h_iv;
		this.attack_IndividualValue = a_iv;
		this.defense_IndividualValue = b_iv;
		this.specialAttack_IndividualValue = c_iv;
		this.specialDefense_IndividualValue = d_iv;
		this.speed_IndividualValue = s_iv;

		this.consideration = consideration;
		this.createId = getId;
		this.userName = userName;

		this.hp = hp;
		this.attack = attack;
		this.defense = defense;
		this.specialAttack = specialAttack;
		this.specialDefense = specialDefense;
		this.speed = speed;

	}

	public trainingArticleData(int createId) {
		this.createId = createId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPokemonId() {
		return pokemonId;
	}

	public void setPokemonId(String pokemonId) {
		this.pokemonId = pokemonId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUpdateData() {
		return updateData;
	}

	public void setUpdateData(String updateData) {
		this.updateData = updateData;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCharacteristic() {
		return characteristic;
	}

	public void setCharacteristic(String characteristic) {
		this.characteristic = characteristic;
	}

	public String getCorrection() {
		return correction;
	}

	public void setCorrection(String correction) {
		this.correction = correction;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public void setSkill1(String skill1) {
		this.skill1 = skill1;
	}

	public String getSkill2() {
		return skill2;
	}

	public void setSkill2(String skill2) {
		this.skill2 = skill2;
	}

	public String getSkill3() {
		return skill3;
	}

	public void setSkill3(String skill3) {
		this.skill3 = skill3;
	}

	public String getSkill4() {
		return skill4;
	}

	public void setSkill4(String skill4) {
		this.skill4 = skill4;
	}

	public String getHp_EffortValue() {
		return hp_EffortValue;
	}

	public void setHp_EffortValue(String hp_EffortValue) {
		this.hp_EffortValue = hp_EffortValue;
	}

	public String getAttack_EffortValue() {
		return attack_EffortValue;
	}

	public void setAttack_EffortValue(String attack_EffortValue) {
		this.attack_EffortValue = attack_EffortValue;
	}

	public String getDefense_EffortValue() {
		return defense_EffortValue;
	}

	public void setDefense_EffortValue(String defense_EffortValue) {
		this.defense_EffortValue = defense_EffortValue;
	}

	public String getSpecialAttack_EffortValue() {
		return specialAttack_EffortValue;
	}

	public void setSpecialAttack_EffortValue(String specialAttack_EffortValue) {
		this.specialAttack_EffortValue = specialAttack_EffortValue;
	}

	public String getSpecialDefense_EffortValue() {
		return specialDefense_EffortValue;
	}

	public void setSpecialDefense_EffortValue(String specialDefense_EffortValue) {
		this.specialDefense_EffortValue = specialDefense_EffortValue;
	}

	public String getSpeed_EffortValue() {
		return speed_EffortValue;
	}

	public void setSpeed_EffortValue(String speed_EffortValue) {
		this.speed_EffortValue = speed_EffortValue;
	}

	public String getHp_IndividualValue() {
		return hp_IndividualValue;
	}

	public void setHp_IndividualValue(String hp_IndividualValue) {
		this.hp_IndividualValue = hp_IndividualValue;
	}

	public String getAttack_IndividualValue() {
		return attack_IndividualValue;
	}

	public void setAttack_IndividualValue(String attack_IndividualValue) {
		this.attack_IndividualValue = attack_IndividualValue;
	}

	public String getDefense_IndividualValue() {
		return defense_IndividualValue;
	}

	public void setDefense_IndividualValue(String defense_IndividualValue) {
		this.defense_IndividualValue = defense_IndividualValue;
	}

	public String getSpecialAttack_IndividualValue() {
		return specialAttack_IndividualValue;
	}

	public void setSpecialAttack_IndividualValue(String specialAttack_IndividualValue) {
		this.specialAttack_IndividualValue = specialAttack_IndividualValue;
	}

	public String getSpecialDefense_IndividualValue() {
		return specialDefense_IndividualValue;
	}

	public void setSpecialDefense_IndividualValue(String specialDefense_IndividualValue) {
		this.specialDefense_IndividualValue = specialDefense_IndividualValue;
	}

	public String getSpeed_IndividualValue() {
		return speed_IndividualValue;
	}

	public void setSpeed_IndividualValue(String speed_IndividualValue) {
		this.speed_IndividualValue = speed_IndividualValue;
	}

	public String getConsideration() {
		return consideration;
	}

	public void setConsideration(String consideration) {
		this.consideration = consideration;
	}

	public String getSkill1() {
		return skill1;
	}
	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getSpecialAttack() {
		return specialAttack;
	}

	public void setSpecialAttack(int specialAttack) {
		this.specialAttack = specialAttack;
	}

	public int getSpecialDefense() {
		return specialDefense;
	}

	public void setSpecialDefense(int specialDefense) {
		this.specialDefense = specialDefense;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getCreateId() {
		return createId;
	}

	public void setCreateId(int create_id) {
		this.createId = create_id;
	}

	public String getuserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


}
