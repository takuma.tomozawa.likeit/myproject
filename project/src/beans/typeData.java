package beans;

import java.io.Serializable;

public class typeData implements Serializable{
	private int id;
	private String type;


	public typeData(int id, String type) {
		this.id = id;
		this.type = type;
	}

	public typeData(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
