package beans;

import java.io.Serializable;
import java.util.Date;

public class userData implements Serializable{
	private int id;
	private String loginId;
	private String name;
	private String password;
	private Date createData;

	public userData(int id,String loginId,String name,String password,Date createData) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.password = password;
		this.createData = createData;
	}

	public userData(int id, String loginId, String name, Date createData) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.createData = createData;
	}

	public userData(int id, String loginId, String name) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
	}



	public userData() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreateData() {
		return createData;
	}
	public void setCreateData(Date createData) {
		this.createData = createData;
	}

}
