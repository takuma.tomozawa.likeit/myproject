package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.trainingArticleData;
import beans.userData;
import dao.TrainingArticleDao;


@WebServlet("/AllUserTrainingArticleList")
public class AllUserTrainingArticleList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		TrainingArticleDao trainingArticleDao = new TrainingArticleDao();

		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
		response.sendRedirect("Login");
		return;
	}

		List<trainingArticleData> taList = trainingArticleDao.allUserArticleTrainingList();
		request.setAttribute("taList", taList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/AllUserTrainingArticleList.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
