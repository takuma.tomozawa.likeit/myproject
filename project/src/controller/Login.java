package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.userData;
import dao.UserDao;



/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		request.setCharacterEncoding("UTF-8");
//		HttpSession session = request.getSession();
//		userData user = (userData)session.getAttribute("userInfo");
//		if(user != null) {
//			response.sendRedirect("UserList");
//			return;
//		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");

		dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		UserDao userDao = new UserDao();
		userData userInfo = null;



		try {
			//情報の照合をするためにDaoにログインIDとパスワードを暗号化して送っている
			userInfo = userDao.loginInfo(loginId, userDao.md5(password));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		if(userInfo == null) {
		request.setAttribute("error", "ログイン失敗");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
		dispatcher.forward(request, response);
		return;
	}

		HttpSession session = request.getSession();

		session.setAttribute("userInfo", userInfo);

		response.sendRedirect("UserList");
	}
}
