package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.eggGroupData;
import beans.typeData;
import beans.userData;
import dao.EggGroupDao;
import dao.PokemonBasicDao;
import dao.TypeDao;


/**
 * Servlet implementation class PokemonDataEntry
 */
@WebServlet("/PokemonDataEntry")
public class PokemonDataEntry extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
		response.sendRedirect("Login");
		return;
	}

		TypeDao typeDao = new TypeDao();
		ArrayList<typeData> typeList = typeDao.getTypeData();
		request.setAttribute("typeList", typeList);

		EggGroupDao eggGroupDao = new EggGroupDao();
		ArrayList<eggGroupData> eggGroupList = eggGroupDao.getEggGroupData();
		request.setAttribute("eggGroupList", eggGroupList);


		request.setCharacterEncoding("UTF-8");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PokemonDataEntry.jsp");

		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		PokemonBasicDao pokemonBasicDao = new PokemonBasicDao();
		TypeDao typeDao = new TypeDao();
		EggGroupDao eggGroupDao = new EggGroupDao();

		String allNumber = request.getParameter("all_number");
		String galarNumber = request.getParameter("galar_number");
		String name = request.getParameter("name");
		String type1 = request.getParameter("type1");
		String type2 = request.getParameter("type2");
		String characteristic1 = request.getParameter("characteristic1");
		String characteristic2 = request.getParameter("characteristic2");
		String characteristicSecret = request.getParameter("characteristic_secret");
		String weight = request.getParameter("weight");
		String eggGroup1 = request.getParameter("egg_group1");
		String eggGroup2 = request.getParameter("egg_group2");
		String hp = request.getParameter("hp_race_value");
		String attack = request.getParameter("attack_race_value");
		String defense = request.getParameter("defense_race_value");
		String specialAttack = request.getParameter("special_attack_race_value");
		String specialDefense = request.getParameter("special_defense_race_value");
		String speed = request.getParameter("speed_race_value");

		//未入力項目確認
		if(allNumber.equals("") || galarNumber.equals("") || name.equals("") || type1.equals("") || characteristic1.equals("")
				|| weight.equals("") || eggGroup1.equals("") || hp.equals("") || attack.equals("")
				|| defense.equals("") || specialAttack.equals("") || specialDefense.equals("") || speed.equals("")) {
			request.setAttribute("error", "未入力の項目がある");
			request.setAttribute("all_number",allNumber);
			request.setAttribute("galar_number",galarNumber);
			request.setAttribute("name",name);
			request.setAttribute("type1",type1);
			request.setAttribute("type2",type2);
			request.setAttribute("characteristic1",characteristic1);
			request.setAttribute("characteristic2",characteristic2);
			request.setAttribute("characteristicSecret",characteristicSecret);
			request.setAttribute("weight",weight);
			request.setAttribute("egg_group1",eggGroup1);
			request.setAttribute("egg_group2",eggGroup2);
			request.setAttribute("hp_race_value",hp);
			request.setAttribute("attack_race_value",attack);
			request.setAttribute("defense_race_value",defense);
			request.setAttribute("special_attack_race_value",specialAttack);
			request.setAttribute("special_defense_race_value",specialDefense);
			request.setAttribute("speed_race_value",speed);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PokemonDataEntry.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//allPokemonIdの重複チェック※一緒に入力されたgalarNumberを送り
		String api = pokemonBasicDao.allNumberIdRegistrationError(allNumber);
		if(api!=null) {
			request.setAttribute("error", "入力した全国図鑑IDは既に登録済み");
			request.setAttribute("galar_number",galarNumber);
			request.setAttribute("name",name);
			request.setAttribute("type1",type1);
			request.setAttribute("type2",type2);
			request.setAttribute("characteristic1",characteristic1);
			request.setAttribute("characteristic2",characteristic2);
			request.setAttribute("characteristicSecret",characteristicSecret);
			request.setAttribute("weight",weight);
			request.setAttribute("egg_group1",eggGroup1);
			request.setAttribute("egg_group2",eggGroup2);
			request.setAttribute("hp_race_value",hp);
			request.setAttribute("attack_race_value",attack);
			request.setAttribute("defense_race_value",defense);
			request.setAttribute("special_attack_race_value",specialAttack);
			request.setAttribute("special_defense_race_value",specialDefense);
			request.setAttribute("speed_race_value",speed);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PokemonDataEntry.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//galarPokemonIdの重複チェック
		String gpi = pokemonBasicDao.galarNumberIdRegistrationError(galarNumber);
		if(gpi!=null) {
			request.setAttribute("error", "入力したガラル図鑑IDは既に登録済み");
			request.setAttribute("all_number",allNumber);
			request.setAttribute("name",name);
			request.setAttribute("type1",type1);
			request.setAttribute("type2",type2);
			request.setAttribute("characteristic1",characteristic1);
			request.setAttribute("characteristic2",characteristic2);
			request.setAttribute("characteristicSecret",characteristicSecret);
			request.setAttribute("weight",weight);
			request.setAttribute("egg_group1",eggGroup1);
			request.setAttribute("egg_group2",eggGroup2);
			request.setAttribute("hp_race_value",hp);
			request.setAttribute("attack_race_value",attack);
			request.setAttribute("defense_race_value",defense);
			request.setAttribute("special_attack_race_value",specialAttack);
			request.setAttribute("special_defense_race_value",specialDefense);
			request.setAttribute("speed_race_value",speed);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PokemonDataEntry.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//データベースにデータ追加
		pokemonBasicDao.insert(allNumber, galarNumber, name, typeDao.typeId(type1), typeDao.typeId(type2), characteristic1,
				characteristic2, characteristicSecret, weight, eggGroupDao.eggGroupId(eggGroup1), eggGroupDao.eggGroupId(eggGroup2),
				hp, attack, defense, specialAttack, specialDefense, speed);

		response.sendRedirect("PokemonRegistrationList");
	}

}
