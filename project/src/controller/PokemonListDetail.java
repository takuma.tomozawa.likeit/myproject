package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.pokemonBasicData;
import dao.PokemonBasicDao;

/**
 * Servlet implementation class PokemonList8thGenerationDetail
 */
@WebServlet("/PokemonListDetail")
public class PokemonListDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		PokemonBasicDao pokemonBasicDao = new PokemonBasicDao();

		int id = Integer.parseInt(request.getParameter("allNumber"));

		pokemonBasicData pbd = pokemonBasicDao.pokemonDetail(id);
		request.setAttribute("pbd", pbd);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PokemonListDetail.jsp");
		dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
