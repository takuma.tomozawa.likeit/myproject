package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.pokemonBasicData;
import beans.userData;
import dao.PokemonBasicDao;


@WebServlet("/PokemonRegistrationList")
public class PokemonRegistrationList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
		response.sendRedirect("Login");
		return;
	}

		PokemonBasicDao pokemonDao = new PokemonBasicDao();
		List<pokemonBasicData> pokemonList = pokemonDao.findPokemonList();

		request.setAttribute("pokemonList",pokemonList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PokemonRegistrationList.jsp");

		dispatcher.forward(request, response);



	}
}
