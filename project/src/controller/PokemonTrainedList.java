package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.pokemonTrainingData;
import beans.userData;
import dao.PokemonTrainingDao;


@WebServlet("/PokemonTrainedList")
public class PokemonTrainedList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
		response.sendRedirect("Login");
		return;
	}
		String loginId = us.getLoginId();

		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		//どのユーザのポケモン情報なのかを判別するためにログイン情報を送りそれに応じた情報が返ってくる
		List<pokemonTrainingData> ptList = pokemonTrainingDao.findPokemonTrainedList(loginId);
		request.setAttribute("ptList",ptList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PokemonTrainedList.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();

		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		String loginId = us.getLoginId();

		List<pokemonTrainingData> ptList = pokemonTrainingDao.findPokemonTrainedList(loginId);
		request.setAttribute("ptList",ptList);


		String createId = request.getParameter("createId");

		pokemonTrainingData ptList2 = pokemonTrainingDao.findPokemonTrainedList2(loginId, createId);
		request.setAttribute("ptList2",ptList2);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PokemonTrainedList.jsp");
		dispatcher.forward(request, response);

	}

}
