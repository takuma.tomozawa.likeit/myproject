package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.correctionData;
import beans.pokemonBasicData;
import beans.userData;
import dao.CorrectionDao;
import dao.PokemonBasicDao;
import dao.PokemonTrainingDao;

/**
 * Servlet implementation class PokemonTrainedRegistration
 */
@WebServlet("/PokemonTrainedRegistration")
public class PokemonTrainedRegistration extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
		response.sendRedirect("Login");
		return;
	}

		PokemonBasicDao pokemonDao = new PokemonBasicDao();
		List<pokemonBasicData> pokemonList = pokemonDao.findPokemonList();

		CorrectionDao correctionDao= new CorrectionDao();
		List<correctionData> correctionList = correctionDao.getCorrectionData();

		request.setAttribute("pokemonList",pokemonList);
		request.setAttribute("correctionList",correctionList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PokemonTrainedRegistration.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		PokemonBasicDao pokemonBasicDao = new PokemonBasicDao();
//		CalculationDao calculationDao = new CalculationDao();
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		CorrectionDao correctionDao = new CorrectionDao();


		String name = request.getParameter("name");
		String user = request.getParameter("user");
		String userId = request.getParameter("user_id");
		String nickname = request.getParameter("nickname");
		String characteristic = request.getParameter("characteristic");
		String correction = request.getParameter("correction");
		String item = request.getParameter("item");

		String skill1 = request.getParameter("skill1");
		String skill2 = request.getParameter("skill2");
		String skill3 = request.getParameter("skill3");
		String skill4 = request.getParameter("skill4");

		String h_ev = request.getParameter("h_ev");
		String a_ev = request.getParameter("a_ev");
		String b_ev = request.getParameter("b_ev");
		String c_ev = request.getParameter("c_ev");
		String d_ev = request.getParameter("d_ev");
		String s_ev = request.getParameter("s_ev");

		String h_iv = request.getParameter("h_iv");
		String a_iv = request.getParameter("a_iv");
		String b_iv = request.getParameter("b_iv");
		String c_iv = request.getParameter("c_iv");
		String d_iv = request.getParameter("d_iv");
		String s_iv = request.getParameter("s_iv");

		String remarks = request.getParameter("remarks");

//		int total_ev = calculationDao.total(h_ev, a_ev, b_ev, c_ev, d_ev, s_ev);
//		request.setAttribute("total_ev",total_ev);


		request.setAttribute("name", name);
		request.setAttribute("userId", userId);
		request.setAttribute("nickname", nickname);
		request.setAttribute("characteristic", characteristic);
		request.setAttribute("correction", correction);
		request.setAttribute("item", item);

		request.setAttribute("skill1", skill1);
		request.setAttribute("skill2", skill2);
		request.setAttribute("skill3", skill3);
		request.setAttribute("skill4", skill4);

		request.setAttribute("h_ev", h_ev);
		request.setAttribute("a_ev", a_ev);
		request.setAttribute("b_ev", b_ev);
		request.setAttribute("c_ev", c_ev);
		request.setAttribute("d_ev", d_ev);
		request.setAttribute("s_ev", s_ev);

		request.setAttribute("h_iv", h_iv);
		request.setAttribute("a_iv", a_iv);
		request.setAttribute("b_iv", b_iv);
		request.setAttribute("c_iv", c_iv);
		request.setAttribute("d_iv", d_iv);
		request.setAttribute("s_iv", s_iv);

		request.setAttribute("remarks", remarks);

		if(name.equals("")) {
			request.setAttribute("error", "ポケモンの名前を入力して下さい");

			request.setAttribute("userId", userId);
			request.setAttribute("nickname", nickname);
			request.setAttribute("characteristic", characteristic);
			request.setAttribute("correction", correction);
			request.setAttribute("item", item);

			request.setAttribute("skill1", skill1);
			request.setAttribute("skill2", skill2);
			request.setAttribute("skill3", skill3);
			request.setAttribute("skill4", skill4);

			request.setAttribute("h_ev", h_ev);
			request.setAttribute("a_ev", a_ev);
			request.setAttribute("b_ev", b_ev);
			request.setAttribute("c_ev", c_ev);
			request.setAttribute("d_ev", d_ev);
			request.setAttribute("s_ev", s_ev);

			request.setAttribute("h_iv", h_iv);
			request.setAttribute("a_iv", a_iv);
			request.setAttribute("b_iv", b_iv);
			request.setAttribute("c_iv", c_iv);
			request.setAttribute("d_iv", d_iv);
			request.setAttribute("s_iv", s_iv);

			request.setAttribute("remarks", remarks);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PokemonTrainedRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		}


		pokemonTrainingDao.insert(pokemonBasicDao.nameId(name), user, nickname, characteristic,
				correctionDao.correctionId(correction),
				item, skill1, skill2, skill3, skill4,
				h_ev, a_ev, b_ev, c_ev, d_ev, s_ev,
				h_iv, a_iv, b_iv, c_iv, d_iv, s_iv, remarks);

			response.sendRedirect("PokemonTrainedList");
	}

}

