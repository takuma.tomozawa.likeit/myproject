package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.correctionData;
import beans.pokemonTrainingData;
import beans.userData;
import dao.CorrectionDao;
import dao.PokemonTrainingDao;

/**
 * Servlet implementation class PokemonTrainedUpdateForm
 */
@WebServlet("/PokemonTrainedUpdateForm")
public class PokemonTrainedUpdateForm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();

		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
		response.sendRedirect("Login");
		return;
	}

		String loginId = us.getLoginId();

		pokemonTrainingData cId = (pokemonTrainingData)session.getAttribute("createId");
		if(session.getAttribute("createId")==null) {
			response.sendRedirect("PokemonTrainedUpdate");
			return;
		}

		int createId = cId.getCreateId();


		CorrectionDao correctionDao= new CorrectionDao();
		List<correctionData> correctionList = correctionDao.getCorrectionData();

		request.setAttribute("correctionList",correctionList);

		//loginIdとcreateIdを基に必要な情報を引き出す
		pokemonTrainingData ptList3 = pokemonTrainingDao.findPokemonTrainedList3(loginId, createId);

		String pokemonId =  ptList3.getPokemonId();
		String nickname =  ptList3.getNickname();
		String characteristic = ptList3.getCharacteristic();
		String correction = ptList3.getCorrection();
		String item = ptList3.getItem();

		int hp_EffortValue = ptList3.getHp_EffortValue();
		int attack_EffortValue = ptList3.getAttack_EffortValue();
		int defense_EffortValue = ptList3.getDefense_EffortValue();
		int specialAttack_EffortValue = ptList3.getSpecialAttack_EffortValue();
		int specialDefense_EffortValue = ptList3.getSpecialDefense_EffortValue();
		int speed_EffortValue = ptList3.getSpeed_EffortValue();
		String remarks = ptList3.getRemarks();

		request.setAttribute("pokemonId",pokemonId);
		request.setAttribute("nickname",nickname);
		request.setAttribute("characteristic",characteristic);
		request.setAttribute("correction",correction);
		request.setAttribute("item",item);

		request.setAttribute("hp_EffortValue",hp_EffortValue);
		request.setAttribute("attack_EffortValue",attack_EffortValue);
		request.setAttribute("defense_EffortValue",defense_EffortValue);
		request.setAttribute("specialAttack_EffortValue",specialAttack_EffortValue);
		request.setAttribute("specialDefense_EffortValue",specialDefense_EffortValue);
		request.setAttribute("speed_EffortValue",speed_EffortValue);
		request.setAttribute("remarks",remarks);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PokemonTrainedUpdateForm.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		CorrectionDao correctionDao = new CorrectionDao();

		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		String loginId = us.getLoginId();

		pokemonTrainingData cId =  (pokemonTrainingData)session.getAttribute("createId");
		int createId = cId.getCreateId();

		String nickname = request.getParameter("nickname");
		String characteristic = request.getParameter("characteristic");
		String correction = request.getParameter("correction");
		String item = request.getParameter("item");

		String skill1 = request.getParameter("skill1");
		String skill2 = request.getParameter("skill2");
		String skill3 = request.getParameter("skill3");
		String skill4 = request.getParameter("skill4");

		String h_ev = request.getParameter("h_ev");
		String a_ev = request.getParameter("a_ev");
		String b_ev = request.getParameter("b_ev");
		String c_ev = request.getParameter("c_ev");
		String d_ev = request.getParameter("d_ev");
		String s_ev = request.getParameter("s_ev");

		String h_iv = request.getParameter("h_iv");
		String a_iv = request.getParameter("a_iv");
		String b_iv = request.getParameter("b_iv");
		String c_iv = request.getParameter("c_iv");
		String d_iv = request.getParameter("d_iv");
		String s_iv = request.getParameter("s_iv");

		String remarks = request.getParameter("remarks");

		request.setAttribute("nickname", nickname);
		request.setAttribute("characteristic", characteristic);
		request.setAttribute("correction", correction);
		request.setAttribute("item", item);

		request.setAttribute("skill1", skill1);
		request.setAttribute("skill2", skill2);
		request.setAttribute("skill3", skill3);
		request.setAttribute("skill4", skill4);

		request.setAttribute("h_ev", h_ev);
		request.setAttribute("a_ev", a_ev);
		request.setAttribute("b_ev", b_ev);
		request.setAttribute("c_ev", c_ev);
		request.setAttribute("d_ev", d_ev);
		request.setAttribute("s_ev", s_ev);

		request.setAttribute("h_iv", h_iv);
		request.setAttribute("a_iv", a_iv);
		request.setAttribute("b_iv", b_iv);
		request.setAttribute("c_iv", c_iv);
		request.setAttribute("d_iv", d_iv);
		request.setAttribute("s_iv", s_iv);

		request.setAttribute("remarks", remarks);

		pokemonTrainingDao.update(nickname, characteristic,
				correctionDao.correctionId(correction),
				item, skill1, skill2, skill3, skill4,
				h_ev, a_ev, b_ev, c_ev, d_ev, s_ev,
				h_iv, a_iv, b_iv, c_iv, d_iv, s_iv,
				remarks, loginId, createId);



			response.sendRedirect("PokemonTrainedList");
	}

}
