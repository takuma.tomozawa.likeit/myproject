package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.pokemonTrainingData;
import beans.userData;
import dao.PokemonTrainingDao;

/**
 * Servlet implementation class PokemonTrainingDelete
 */
@WebServlet("/PokemonTrainingDelete")
public class PokemonTrainingDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
		response.sendRedirect("Login");
		return;
	}

		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();


			String loginId = us.getLoginId();

			pokemonTrainingData cId =  (pokemonTrainingData)session.getAttribute("createId");
			int createId = cId.getCreateId();
			pokemonTrainingData ptList4 = pokemonTrainingDao.findPokemonTrainedList4(loginId, createId);
			request.setAttribute("ptList4", ptList4);

			String pokemonId =  ptList4.getPokemonId();
			String nickname =  ptList4.getNickname();
			String name =  ptList4.getName();

			request.setAttribute("pokemonId",pokemonId);
			request.setAttribute("nickname",nickname);
			request.setAttribute("name",name);



		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PokemonTrainingDelete.jsp");
		dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();

		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");

		String loginId = us.getLoginId();
		pokemonTrainingData cId =  (pokemonTrainingData)session.getAttribute("createId");
		int createId = cId.getCreateId();

		pokemonTrainingDao.delete(loginId, createId);

		response.sendRedirect("PokemonTrainedUpdate");
	}

}
