package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.trainingArticleData;
import beans.userData;
import dao.PokemonBasicDao;
import dao.PokemonTrainingDao;
import dao.TrainingArticleDao;

/**
 * Servlet implementation class PostConfirm
 */
@WebServlet("/PostConfirm")
public class PostConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		PokemonBasicDao pokemonBasicDao = new PokemonBasicDao();
		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
		response.sendRedirect("Login");
		return;
	}


		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		trainingArticleData ta = (trainingArticleData)session.getAttribute("trainingArticle");

		if(ta == null) {
		response.sendRedirect("errorPage");
		return;
	}

		String loginId = ta.getUserId();

		String title = ta.getTitle();
		String name = ta.getName();
		String pokemonId = pokemonBasicDao.nameId(ta.getName());
		String characteristic = ta.getCharacteristic();
		String correction = ta.getCorrection();
		String item = ta.getItem();

		String skill1 = ta.getSkill1();
		String skill2 = ta.getSkill2();
		String skill3 = ta.getSkill3();
		String skill4 = ta.getSkill4();

		String hp_EffortValue = ta.getHp_EffortValue();
		String attack_EffortValue = ta.getAttack_EffortValue();
		String defense_EffortValue = ta.getDefense_EffortValue();
		String specialAttack_EffortValue = ta.getSpecialAttack_EffortValue();
		String specialDefense_EffortValue = ta.getSpecialDefense_EffortValue();
		String speed_EffortValue = ta.getSpeed_EffortValue();

		String hp_IndividualValue = pokemonTrainingDao.hRaceValueConversion(ta.getHp_IndividualValue());
		String attack_IndividualValue = pokemonTrainingDao.aRaceValueConversion(ta.getAttack_IndividualValue());
		String defense_IndividualValue = pokemonTrainingDao.bRaceValueConversion(ta.getDefense_IndividualValue());
		String specialAttack_IndividualValue = pokemonTrainingDao.cRaceValueConversion(ta.getSpecialAttack_IndividualValue());
		String specialDefense_IndividualValue = pokemonTrainingDao.dRaceValueConversion(ta.getSpecialDefense_IndividualValue());
		String speed_IndividualValue = pokemonTrainingDao.sRaceValueConversion(ta.getSpeed_IndividualValue());

		String consideration = ta.getConsideration();

		request.setAttribute("loginId",loginId);
		request.setAttribute("title",title);
		request.setAttribute("name",name);
		request.setAttribute("pokemonId",pokemonId);
		request.setAttribute("characteristic",characteristic);
		request.setAttribute("correction",correction);
		request.setAttribute("item",item);

		request.setAttribute("skill1",skill1);
		request.setAttribute("skill2",skill2);
		request.setAttribute("skill3",skill3);
		request.setAttribute("skill4",skill4);

		request.setAttribute("hp_EffortValue",hp_EffortValue);
		request.setAttribute("attack_EffortValue",attack_EffortValue);
		request.setAttribute("defense_EffortValue",defense_EffortValue);
		request.setAttribute("specialAttack_EffortValue",specialAttack_EffortValue);
		request.setAttribute("specialDefense_EffortValue",specialDefense_EffortValue);
		request.setAttribute("speed_EffortValue",speed_EffortValue);

		request.setAttribute("hp_IndividualValue",hp_IndividualValue);
		request.setAttribute("attack_IndividualValue",attack_IndividualValue);
		request.setAttribute("defense_IndividualValue",defense_IndividualValue);
		request.setAttribute("specialAttack_IndividualValue",specialAttack_IndividualValue);
		request.setAttribute("specialDefense_IndividualValue",specialDefense_IndividualValue);
		request.setAttribute("speed_IndividualValue",speed_IndividualValue);

		request.setAttribute("consideration",consideration);



		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PostConfirm.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		TrainingArticleDao trainingArticleDao = new TrainingArticleDao();
		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		trainingArticleData ta = (trainingArticleData)session.getAttribute("trainingArticle");

		String loginId = ta.getUserId();

		String title = ta.getTitle();
		String name = ta.getName();
		String characteristic = ta.getCharacteristic();
		String correction = ta.getCorrection();
		String item = ta.getItem();

		String skill1 = ta.getSkill1();
		String skill2 = ta.getSkill2();
		String skill3 = ta.getSkill3();
		String skill4 = ta.getSkill4();

		String hp_EffortValue = ta.getHp_EffortValue();
		String attack_EffortValue = ta.getAttack_EffortValue();
		String defense_EffortValue = ta.getDefense_EffortValue();
		String specialAttack_EffortValue = ta.getSpecialAttack_EffortValue();
		String specialDefense_EffortValue = ta.getSpecialDefense_EffortValue();
		String speed_EffortValue = ta.getSpeed_EffortValue();

		String hp_IndividualValue = ta.getHp_IndividualValue();
		String attack_IndividualValue = ta.getAttack_IndividualValue();
		String defense_IndividualValue = ta.getDefense_IndividualValue();
		String specialAttack_IndividualValue = ta.getSpecialAttack_IndividualValue();
		String specialDefense_IndividualValue = ta.getSpecialDefense_IndividualValue();
		String speed_IndividualValue = ta.getSpeed_IndividualValue();

		String consideration = ta.getConsideration();

		String userName = us.getName();


		trainingArticleDao.insert(loginId, title, name, characteristic, correction, item, skill1, skill2, skill3, skill4,
				hp_EffortValue, attack_EffortValue, defense_EffortValue, specialAttack_EffortValue,
				specialDefense_EffortValue, speed_EffortValue, hp_IndividualValue, attack_IndividualValue,
				defense_IndividualValue, specialAttack_IndividualValue, specialDefense_IndividualValue,
				speed_IndividualValue, consideration, userName);

		session.removeAttribute("trainingArticle");

		response.sendRedirect("PostComplete");
	}

}
