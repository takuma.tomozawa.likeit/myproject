package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.pokemonBasicData;
import dao.PokemonBasicDao;


@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		PokemonBasicDao pokemonBasicDao = new PokemonBasicDao();
		
		session.removeAttribute("pokemon");
		
		String search = request.getParameter("search");
		
		List<pokemonBasicData> pokemon = pokemonBasicDao.search(search);
		session.setAttribute("pokemon", pokemon);

		response.sendRedirect("SearchSuggestions");

	}

}
