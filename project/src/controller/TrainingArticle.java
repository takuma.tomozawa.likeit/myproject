package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.correctionData;
import beans.pokemonTrainingData;
import beans.trainingArticleData;
import beans.userData;
import dao.CorrectionDao;
import dao.PokemonBasicDao;
import dao.PokemonTrainingDao;

/**
 * Servlet implementation class TrainingArticle
 */
@WebServlet("/TrainingArticle")
public class TrainingArticle extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		PokemonBasicDao pokemonBasicDao = new PokemonBasicDao();
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		CorrectionDao correctionDao= new CorrectionDao();
		List<correctionData> correctionList = correctionDao.getCorrectionData();

		request.setAttribute("correctionList",correctionList);

		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
		response.sendRedirect("Login");
		return;
	}

		String loginId = us.getLoginId();

		pokemonTrainingData cId = (pokemonTrainingData)session.getAttribute("createId");


		//リストからポケモンを選択するから先に進んだ場合は以下のif文が実行される
		if(session.getAttribute("createId") != null) {
			int createId = cId.getCreateId();
			pokemonTrainingData ptList3 = pokemonTrainingDao.findPokemonTrainedList3(loginId, createId);

			String name =  pokemonBasicDao.pokemonName(ptList3.getPokemonId());//ポケモンのIDを基に（種族名）名前を引き出す
			//String nickname =  ptList3.getNickname();
			String characteristic = ptList3.getCharacteristic();
			String correction = ptList3.getCorrection();
			String item = ptList3.getItem();

			String skill1 = ptList3.getSkill1();
			String skill2 = ptList3.getSkill2();
			String skill3 = ptList3.getSkill3();
			String skill4 = ptList3.getSkill4();

			int hp_EffortValue = ptList3.getHp_EffortValue();
			int attack_EffortValue = ptList3.getAttack_EffortValue();
			int defense_EffortValue = ptList3.getDefense_EffortValue();
			int specialAttack_EffortValue = ptList3.getSpecialAttack_EffortValue();
			int specialDefense_EffortValue = ptList3.getSpecialDefense_EffortValue();
			int speed_EffortValue = ptList3.getSpeed_EffortValue();

			request.setAttribute("name",name);
			//request.setAttribute("nickname",nickname);
			request.setAttribute("characteristic",characteristic);
			request.setAttribute("correction",correction);
			request.setAttribute("item",item);

			request.setAttribute("skill1",skill1);
			request.setAttribute("skill2",skill2);
			request.setAttribute("skill3",skill3);
			request.setAttribute("skill4",skill4);


			//jspの方で値を入れる場合0が敢えて入らないようにする処理、0が初期値で入ると252が表示されなかったため
			if(hp_EffortValue != 0) {
				request.setAttribute("hp_EffortValue",hp_EffortValue);
			}

			if(attack_EffortValue != 0) {
				request.setAttribute("attack_EffortValue",attack_EffortValue);
			}

			if(defense_EffortValue != 0) {
				request.setAttribute("defense_EffortValue",defense_EffortValue);
			}

			if(specialAttack_EffortValue != 0) {
				request.setAttribute("specialAttack_EffortValue",specialAttack_EffortValue);
			}

			if(specialDefense_EffortValue != 0) {
				request.setAttribute("specialDefense_EffortValue",specialDefense_EffortValue);
			}

			if(speed_EffortValue != 0) {
				request.setAttribute("speed_EffortValue",speed_EffortValue);
			}

			//sessionに格納した値を削除する
			session.removeAttribute("createId");
		}

		//PostConFirmにて修正するで戻ってきた場合に実行されるメソッド
		trainingArticleData ta = (trainingArticleData)session.getAttribute("trainingArticle");
		if(session.getAttribute("trainingArticle") != null) {

			String title = ta.getTitle();
			String name = ta.getName();
			String pokemonId = pokemonBasicDao.nameId(ta.getName());
			String characteristic = ta.getCharacteristic();
			String correction = ta.getCorrection();
			String item = ta.getItem();

			String skill1 = ta.getSkill1();
			String skill2 = ta.getSkill2();
			String skill3 = ta.getSkill3();
			String skill4 = ta.getSkill4();

			int hp_EffortValue = Integer.parseInt(ta.getHp_EffortValue());
			int attack_EffortValue = Integer.parseInt(ta.getAttack_EffortValue());
			int defense_EffortValue = Integer.parseInt(ta.getDefense_EffortValue());
			int specialAttack_EffortValue = Integer.parseInt(ta.getSpecialAttack_EffortValue());
			int specialDefense_EffortValue = Integer.parseInt(ta.getSpecialDefense_EffortValue());
			int speed_EffortValue = Integer.parseInt(ta.getSpeed_EffortValue());

			String hp_IndividualValue = ta.getHp_IndividualValue();
			String attack_IndividualValue = ta.getAttack_IndividualValue();
			String defense_IndividualValue = ta.getDefense_IndividualValue();
			String specialAttack_IndividualValue = ta.getSpecialAttack_IndividualValue();
			String specialDefense_IndividualValue = ta.getSpecialDefense_IndividualValue();
			String speed_IndividualValue = ta.getSpeed_IndividualValue();

			String consideration = ta.getConsideration();

			request.setAttribute("msg","チェックがあった場合でも個体値のチェックはリセットされるので再度入力して下さい");
			request.setAttribute("loginId",loginId);
			request.setAttribute("title",title);
			request.setAttribute("name",name);
			request.setAttribute("pokemonId",pokemonId);
			request.setAttribute("characteristic",characteristic);
			request.setAttribute("correction",correction);
			request.setAttribute("item",item);

			request.setAttribute("skill1",skill1);
			request.setAttribute("skill2",skill2);
			request.setAttribute("skill3",skill3);
			request.setAttribute("skill4",skill4);

			if(hp_EffortValue != 0) {
				request.setAttribute("hp_EffortValue",hp_EffortValue);
			}

			if(attack_EffortValue != 0) {
				request.setAttribute("attack_EffortValue",attack_EffortValue);
			}

			if(defense_EffortValue != 0) {
				request.setAttribute("defense_EffortValue",defense_EffortValue);
			}

			if(specialAttack_EffortValue != 0) {
				request.setAttribute("specialAttack_EffortValue",specialAttack_EffortValue);
			}

			if(specialDefense_EffortValue != 0) {
				request.setAttribute("specialDefense_EffortValue",specialDefense_EffortValue);
			}

			if(speed_EffortValue != 0) {
				request.setAttribute("speed_EffortValue",speed_EffortValue);
			}

			request.setAttribute("hp_IndividualValue",hp_IndividualValue);
			request.setAttribute("attack_IndividualValue",attack_IndividualValue);
			request.setAttribute("defense_IndividualValue",defense_IndividualValue);
			request.setAttribute("specialAttack_IndividualValue",specialAttack_IndividualValue);
			request.setAttribute("specialDefense_IndividualValue",specialDefense_IndividualValue);
			request.setAttribute("speed_IndividualValue",speed_IndividualValue);

			request.setAttribute("consideration",consideration);

			session.removeAttribute("trainingArticle");

		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/TrainingArticle.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();

		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");

		String loginId = us.getLoginId();

		String title = request.getParameter("title");
		String name = request.getParameter("name");
		String characteristic = request.getParameter("characteristic");
		String correction = request.getParameter("correction");
		String item = request.getParameter("item");

		String skill1 = request.getParameter("skill1");
		String skill2 = request.getParameter("skill2");
		String skill3 = request.getParameter("skill3");
		String skill4 = request.getParameter("skill4");

		String hp_EffortValue = pokemonTrainingDao.hpEffortValue(request.getParameter("hp_EffortValue"));
		String attack_EffortValue = pokemonTrainingDao.attackEffortValue(request.getParameter("attack_EffortValue"));
		String defense_EffortValue = pokemonTrainingDao.defenseEffortValue(request.getParameter("defense_EffortValue"));
		String specialAttack_EffortValue = pokemonTrainingDao.specialAttackEffortValue(request.getParameter("specialAttack_EffortValue"));
		String specialDefense_EffortValue = pokemonTrainingDao.specialDefenseEffortValue(request.getParameter("specialDefense_EffortValue"));
		String speed_EffortValue = pokemonTrainingDao.speedEffortValue(request.getParameter("speed_EffortValue"));

		String hp_IndividualValue = request.getParameter("hp_IndividualValue");
		String attack_IndividualValue = request.getParameter("attack_IndividualValue");
		String defense_IndividualValue = request.getParameter("defense_IndividualValue");
		String specialAttack_IndividualValue = request.getParameter("specialAttack_IndividualValue");
		String specialDefense_IndividualValue = request.getParameter("specialDefense_IndividualValue");
		String speed_IndividualValue = request.getParameter("speed_IndividualValue");

		String consideration = request.getParameter("consideration");

		if(title.equals("") || name.equals("") || characteristic.equals("") || correction.equals("")
				|| skill1.equals("") || skill2.equals("") || skill3.equals("") || skill4.equals("")
				|| consideration.equals("")) {
			request.setAttribute("error1", "未入力の項目があります。持ち物と努力値以外の項目は必須項目となります。");
			request.setAttribute("error2","個体値のチェックはリセットされます");
			request.setAttribute("title",title);
			request.setAttribute("name",name);
			request.setAttribute("characteristic",characteristic);
			request.setAttribute("correction",correction);
			request.setAttribute("item",item);

			request.setAttribute("skill1",skill1);
			request.setAttribute("skill2",skill2);
			request.setAttribute("skill3",skill3);
			request.setAttribute("skill4",skill4);

			request.setAttribute("hp_EffortValue",hp_EffortValue);
			request.setAttribute("attack_EffortValue",attack_EffortValue);
			request.setAttribute("defense_EffortValue",defense_EffortValue);
			request.setAttribute("specialAttack_EffortValue",specialAttack_EffortValue);
			request.setAttribute("specialAttack_EffortValue",specialDefense_EffortValue);
			request.setAttribute("speed_EffortValue",speed_EffortValue);

			request.setAttribute("consideration",consideration);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/TrainingArticle.jsp");
			dispatcher.forward(request, response);
			return;
		}

		trainingArticleData trainingArticle = new trainingArticleData(loginId, name, title, characteristic, correction, item,
				skill1, skill2, skill3, skill4,
				hp_EffortValue, attack_EffortValue, defense_EffortValue, specialAttack_EffortValue,
				specialDefense_EffortValue, speed_EffortValue, hp_IndividualValue, attack_IndividualValue,
				defense_IndividualValue, specialAttack_IndividualValue, specialDefense_IndividualValue,
				speed_IndividualValue, consideration);



		session.setAttribute("trainingArticle",trainingArticle);

		response.sendRedirect("PostConfirm");

	}

}
