package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.correctionData;
import beans.trainingArticleData;
import beans.userData;
import dao.CorrectionDao;
import dao.TrainingArticleDao;

/**
 * Servlet implementation class TrainingArticleDelete
 */
@WebServlet("/TrainingArticleDelete")
public class TrainingArticleDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		TrainingArticleDao trainingArticleDao = new TrainingArticleDao();
		CorrectionDao correctionDao= new CorrectionDao();
		List<correctionData> correctionList = correctionDao.getCorrectionData();
		request.setAttribute("correctionList",correctionList);
		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
		response.sendRedirect("Login");
		return;
	}

		String loginId = us.getLoginId();

		//TrainingArticlelistのjspのボタンの箇所、"?creatId=${}"の値をとってきている
		int createId = Integer.parseInt(request.getParameter("createId"));

		trainingArticleData tad = trainingArticleDao.findTrainingArticleList2(loginId, createId);
		session.setAttribute("tad", tad);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/TrainingArticleDelete.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		TrainingArticleDao trainingArticleDao = new TrainingArticleDao();

		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		String loginId = us.getLoginId();

		int createId = Integer.parseInt(request.getParameter("createId"));

		trainingArticleDao.delete(loginId, createId);

		response.sendRedirect("TrainingArticleList");

	}

}
