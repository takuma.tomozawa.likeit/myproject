package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.trainingArticleData;
import beans.userData;
import dao.TrainingArticleDao;

/**
 * Servlet implementation class TrainingArticleList
 */
@WebServlet("/TrainingArticleList")
public class TrainingArticleList extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		TrainingArticleDao trainingArticleDao = new TrainingArticleDao();

		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
		response.sendRedirect("Login");
		return;
	}

		String loginId = us.getLoginId();

		List<trainingArticleData> taList = trainingArticleDao.findTrainingArticleList(loginId);
		request.setAttribute("taList", taList);

		session.removeAttribute("trainingArticle");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/TrainingArticleList.jsp");
		dispatcher.forward(request, response);
	}

}
