package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.correctionData;
import beans.trainingArticleData;
import beans.userData;
import dao.CorrectionDao;
import dao.PokemonTrainingDao;
import dao.TrainingArticleDao;

/**
 * Servlet implementation class TrainingArticleUpdate
 */
@WebServlet("/TrainingArticleUpdate")
public class TrainingArticleUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		TrainingArticleDao trainingArticleDao = new TrainingArticleDao();
		CorrectionDao correctionDao= new CorrectionDao();
		List<correctionData> correctionList = correctionDao.getCorrectionData();
		request.setAttribute("correctionList",correctionList);
		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
		response.sendRedirect("Login");
		return;
	}

		String loginId = us.getLoginId();

		int createId = Integer.parseInt(request.getParameter("createId"));

		trainingArticleData tad = trainingArticleDao.findTrainingArticleList2(loginId, createId);
		session.setAttribute("tad", tad);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/TrainingArticleUpdate.jsp");
		dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		TrainingArticleDao trainingArticleDao = new TrainingArticleDao();


		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");

		String loginId = us.getLoginId();

		int createId = Integer.parseInt(request.getParameter("createId"));

		String title = request.getParameter("title");
		String name = request.getParameter("name");
		String characteristic = request.getParameter("characteristic");
		String correction = request.getParameter("correction");
		String item = request.getParameter("item");

		String skill1 = request.getParameter("skill1");
		String skill2 = request.getParameter("skill2");
		String skill3 = request.getParameter("skill3");
		String skill4 = request.getParameter("skill4");

		String hp_EffortValue = pokemonTrainingDao.hpEffortValue(request.getParameter("hp_EffortValue"));
		String attack_EffortValue = pokemonTrainingDao.attackEffortValue(request.getParameter("attack_EffortValue"));
		String defense_EffortValue = pokemonTrainingDao.defenseEffortValue(request.getParameter("defense_EffortValue"));
		String specialAttack_EffortValue = pokemonTrainingDao.specialAttackEffortValue(request.getParameter("specialAttack_EffortValue"));
		String specialDefense_EffortValue = pokemonTrainingDao.specialDefenseEffortValue(request.getParameter("specialDefense_EffortValue"));
		String speed_EffortValue = pokemonTrainingDao.speedEffortValue(request.getParameter("speed_EffortValue"));

		String hp_IndividualValue = request.getParameter("hp_IndividualValue");
		String attack_IndividualValue = request.getParameter("attack_IndividualValue");
		String defense_IndividualValue = request.getParameter("defense_IndividualValue");
		String specialAttack_IndividualValue = request.getParameter("specialAttack_IndividualValue");
		String specialDefense_IndividualValue = request.getParameter("specialDefense_IndividualValue");
		String speed_IndividualValue = request.getParameter("speed_IndividualValue");

		String consideration = request.getParameter("consideration");

		if(title.equals("") || name.equals("") || characteristic.equals("") || correction.equals("")
				|| skill1.equals("") || skill2.equals("") || skill3.equals("") || skill4.equals("")
				|| consideration.equals("")) {
			request.setAttribute("error1", "未入力の項目があります。持ち物と努力値以外の項目は必須項目となります。");
			request.setAttribute("error2","個体値のチェックはリセットされます");
			request.setAttribute("title",title);
			request.setAttribute("name",name);
			request.setAttribute("characteristic",characteristic);
			request.setAttribute("correction",correction);
			request.setAttribute("item",item);

			request.setAttribute("skill1",skill1);
			request.setAttribute("skill2",skill2);
			request.setAttribute("skill3",skill3);
			request.setAttribute("skill4",skill4);

			request.setAttribute("hp_EffortValue",hp_EffortValue);
			request.setAttribute("attack_EffortValue",attack_EffortValue);
			request.setAttribute("defense_EffortValue",defense_EffortValue);
			request.setAttribute("specialAttack_EffortValue",specialAttack_EffortValue);
			request.setAttribute("specialAttack_EffortValue",specialAttack_EffortValue);
			request.setAttribute("speed_EffortValue",speed_EffortValue);

			request.setAttribute("consideration",consideration);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/TrainingArticleUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		trainingArticleDao.update(title, name, characteristic, correction, item, skill1, skill2, skill3, skill4,
				hp_EffortValue, attack_EffortValue, defense_EffortValue, specialAttack_EffortValue,
				specialDefense_EffortValue, speed_EffortValue, hp_IndividualValue, attack_IndividualValue,
				defense_IndividualValue, specialAttack_IndividualValue, specialDefense_IndividualValue,
				speed_IndividualValue, consideration, loginId, createId);


		response.sendRedirect("TrainingArticleList");

	}

}
