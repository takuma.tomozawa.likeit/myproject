package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

@WebServlet("/UserCreate")
public class UserCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserCreate.jsp");

		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		UserDao f = new UserDao();
		UserDao userDao = new UserDao();

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String rePassword = request.getParameter("rePassword");

		//入力データの重複をチェック
		String function = f.loginIdRegistrationError(loginId);
		if(loginId.equals(function)) {
			request.setAttribute("error", "入力されたログインIDは既に登録されています");
			request.setAttribute("name",name);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserCreate.jsp");
			dispatcher.forward(request, response);
			return ;
		}

		//データの未入力項目のチェック
		if(loginId.equals("") || name.equals("") || password.equals("")) {
		request.setAttribute("error", "未入力の項目があります");
		request.setAttribute("loginId",loginId);
		request.setAttribute("name",name);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserCreate.jsp");
		dispatcher.forward(request, response);
		return ;
		}

		//パスワードの一致チェック
		if(!password.equals(rePassword)) {
		request.setAttribute("error", "パスワードが一致しません");
		request.setAttribute("loginId",loginId);
		request.setAttribute("name",name);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserCreate.jsp");
		dispatcher.forward(request, response);
		return ;
		}

		//入力項目を登録、パスワードはmd5で暗号化して登録される

			try {
				userDao.userInsert(loginId, name, f.md5(password));
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

		response.sendRedirect("ConfirmRegistration");

	}

}
