package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.userData;
import dao.UserDao;


@WebServlet("/UserList")
public class UserList extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		UserDao userDao = new UserDao();

		//ログイン情報がない場合このページにアクセスしたときログイン画面に移行する
		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
			response.sendRedirect("Login");
			return;
		}



		List<userData> userList = userDao.allUser();
		request.setAttribute("userList", userList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserList.jsp");

		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		UserDao dao = new UserDao();

		String searchLoginId = request.getParameter("loginId");
		String searchName = request.getParameter("name");

		List<userData> userSearch = dao.userSearch(searchLoginId, searchName);

		//ユーザの検索結果
		request.setAttribute("userSearch", userSearch);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserList.jsp");
		dispatcher.forward(request, response);
	}

}
