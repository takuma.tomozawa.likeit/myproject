package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.userData;
import dao.UserDao;


@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		userData us = (userData)session.getAttribute("userInfo");
		if(us == null) {
			response.sendRedirect("Login");
			return;
		}

		UserDao userDao = new UserDao();

		String id = request.getParameter("id");
		userData userDetail = userDao.userDetail(id);

		session.setAttribute("userDetail", userDetail);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		UserDao userDao = new UserDao();

		String loginId =  request.getParameter("id");
		String name =  request.getParameter("name");
		String pass =  request.getParameter("pass");
		String rePass =  request.getParameter("rePass");

		if(!pass.equals(rePass)) {
			request.setAttribute("err", "パスワードの入力が一致しません");
			request.setAttribute("name", name);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

			try {
				userDao.userUpdate(loginId, name, userDao.md5(pass));
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}

		response.sendRedirect("UserList");
	}

}
