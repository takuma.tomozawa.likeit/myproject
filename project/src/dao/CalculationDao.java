package dao;

public class CalculationDao {
	//ポケモンの努力値をもとに実数値を計算+努力値の合計値も計算
	public int total(String h, String a, String b, String c, String d, String s) {


		if(h.equals("")) {
			h = "0";
		}
		if(a.equals("")) {
			a = "0";
		}
		if(b.equals("")) {
			b = "0";
		}
		if(c.equals("")) {
			c = "0";
		}
		if(d.equals("")) {
			d = "0";
		}
		if(s.equals("")) {
			s = "0";
		}

		int hp = Integer.parseInt(h);
		int attack = Integer.parseInt(a);
		int defense = Integer.parseInt(b);
		int special_attack = Integer.parseInt(c);
		int special_defense = Integer.parseInt(d);
		int speed = Integer.parseInt(s);

		int total = hp + attack + defense + special_attack + special_defense + speed;

		return total;

	}

}
