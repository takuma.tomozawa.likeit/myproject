package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.correctionData;




public class CorrectionDao {

	//correctionテーブルの情報を取得する
	public ArrayList<correctionData> getCorrectionData(){
		ArrayList<correctionData> correctionDataList = new ArrayList<correctionData>();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM correction");

			ResultSet rs = st.executeQuery();


			while (rs.next()) {
				int id = rs.getInt("id");
				String correction = rs.getString("correction_type");
				correctionData c = new correctionData(id, correction);
				correctionDataList.add(c);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return correctionDataList;
	}

	//送られてきたcorrectionをもとにidを返す
	public String correctionId(String correction) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM correction WHERE correction_type = ?");
			st.setString(1, correction);

			ResultSet rs = st.executeQuery();
			if (!rs.next()) {
				return null;
			}

			String id = rs.getString("id");
			return id;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//送られてきたidをもとにcorrectionを返す
	public String correctionName(int correctionId) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM correction WHERE id = ?");
			st.setInt(1, correctionId);

			ResultSet rs = st.executeQuery();
			if (!rs.next()) {
				return null;
			}

			String correction = rs.getString("correction_type");
			return correction;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
