package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.eggGroupData;




public class EggGroupDao {

	public ArrayList<eggGroupData> getEggGroupData() {
		ArrayList<eggGroupData> eggGroupDataList = new ArrayList<eggGroupData>();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM egg_group");

			ResultSet rs = st.executeQuery();


			while (rs.next()) {
				int id = rs.getInt("id");
				String eggGroup = rs.getString("egg_Group");
				eggGroupData eg = new eggGroupData(id, eggGroup);
				eggGroupDataList.add(eg);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return eggGroupDataList;
	}

	public String eggGroupId(String eggGroup) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM egg_group WHERE egg_group = ?");
			st.setString(1, eggGroup);

			ResultSet rs = st.executeQuery();
			if (!rs.next()) {
				return null;
			}

			String id = rs.getString("id");
			return id;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public String eggGroup(int eggGroupId) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM egg_group WHERE id = ?");
			st.setInt(1, eggGroupId);

			ResultSet rs = st.executeQuery();
			if (!rs.next()) {
				return null;
			}

			String eggGroup = rs.getString("egg_group");
			return eggGroup;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
