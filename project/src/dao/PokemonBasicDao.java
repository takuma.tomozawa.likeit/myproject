package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.pokemonBasicData;

public class PokemonBasicDao {

	//ポケモン基礎情報を登録するDAO
	public void insert(String allNumber, String galarNumber, String name, String type1, String type2, String characteristic1,
			String characteristic2, String characteristicSecret, String weight, String eggGroup1, String eggGroup2,
			String hp, String attack, String defense, String specialAttack, String specialDefense, String speed) {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO pokemon_basic(all_pokemon_id, galar_pokemon_id, name, type_id1, type_id2,  \r\n" +
					"characteristic1, characteristic2, characteristic_secret, weight, egg_group1_id, egg_group2_id, hp_race_value,  \r\n" +
					"attack_race_value, defense_race_value, special_attack_race_value, special_defense_race_value, speed_race_value) \r\n" +
					"VALUE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

			st.setString(1, allNumber);
			st.setString(2, galarNumber);
			st.setString(3, name);
			st.setString(4, type1);
			st.setString(5, type2);
			st.setString(6, characteristic1);
			st.setString(7, characteristic2);
			st.setString(8, characteristicSecret);
			st.setString(9, weight);
			st.setString(10, eggGroup1);
			st.setString(11, eggGroup2);
			st.setString(12, hp);
			st.setString(13, attack);
			st.setString(14, defense);
			st.setString(15, specialAttack);
			st.setString(16, specialDefense);
			st.setString(17, speed);
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//ポケモンの登録した情報を取得する
	public List<pokemonBasicData> findPokemonList() {
		Connection con = null;
		List<pokemonBasicData> pokemonList = new ArrayList<pokemonBasicData>();
		TypeDao typeDao = new TypeDao();
		EggGroupDao eggGroupDao = new EggGroupDao();

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM pokemon_basic";

			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String allNumber = rs.getString("all_pokemon_id");
				int galarNumber = rs.getInt("galar_pokemon_id");
				String name = rs.getString("name");
				String type1 = typeDao.typeName(rs.getInt("type_id1"));
				String type2 = typeDao.typeName(rs.getInt("type_id2"));
				String characteristic1 = rs.getString("characteristic1");
				String characteristic2 = rs.getString("characteristic2");
				String characteristicSecret = rs.getString("characteristic_Secret");
				double whight = rs.getInt("weight");
				String eggGroup1 = eggGroupDao.eggGroup(rs.getInt("egg_group1_id"));
				String eggGroup2 = eggGroupDao.eggGroup(rs.getInt("egg_group2_id"));

				int hp = rs.getInt("hp_race_value");
				int attack = rs.getInt("attack_race_value");
				int defense = rs.getInt("defense_race_value");
				int specialAttack = rs.getInt("special_attack_race_value");
				int specialDefense = rs.getInt("special_defense_race_value");
				int speed = rs.getInt("speed_race_value");

				pokemonBasicData pokemon = new pokemonBasicData(id, allNumber, galarNumber,
						name, type1, type2, characteristic1, characteristic2,
						characteristicSecret, whight, eggGroup1, eggGroup2, hp,
						attack, defense, specialAttack, specialDefense, speed);

				pokemonList.add(pokemon);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return pokemonList;
	}


	public String allNumberIdRegistrationError(String allNumber) {
		Connection con = null;

		try {
			con = DBManager.getConnection();

			String sql = "SELECT all_pokemon_id FROM pokemon_basic WHERE all_pokemon_id = ?";

			PreparedStatement pSt = con.prepareStatement(sql);
			pSt.setString(1, allNumber);
			ResultSet rs = pSt.executeQuery();

			while (!rs.next()) {
				return null;
			}


			//結果表のlogin_idを呼び出し、行数分それを実行
			String allNumberData = rs.getString("all_pokemon_id");
			return allNumberData;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
	}

	public String galarNumberIdRegistrationError(String galarNumber) {
		Connection con = null;

		try {
			con = DBManager.getConnection();

			String sql = "SELECT galar_pokemon_id FROM pokemon_basic WHERE galar_pokemon_id = ?";

			PreparedStatement pSt = con.prepareStatement(sql);
			pSt.setString(1, galarNumber);
			ResultSet rs = pSt.executeQuery();

			while (!rs.next()) {
				return null;
			}


			String galarNumberData = rs.getString("galar_pokemon_id");
			return galarNumberData;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
	}

	public String nameId(String name) {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM pokemon_basic WHERE name = ?");
			st.setString(1, name);

			ResultSet rs = st.executeQuery();
			if (!rs.next()) {
				return null;
			}

			String pokemonId = rs.getString("all_pokemon_id");
			return pokemonId;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public String pokemonName(String id) {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM pokemon_basic WHERE all_pokemon_id = ?");
			st.setString(1, id);

			ResultSet rs = st.executeQuery();
			if (!rs.next()) {
				return null;
			}

			String name = rs.getString("name");
			return name;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ポケモンを検索するメソッド、部分一致検索に対応
	public List<pokemonBasicData> search(String search) {
		List<pokemonBasicData> pokemonList = new ArrayList<pokemonBasicData>();
		TypeDao typeDao = new TypeDao();
		EggGroupDao eggGroupDao = new EggGroupDao();
		Connection con = null;

		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM "+

					"pokemon_basic "+

					"WHERE name LIKE '%" + search + "%'";

			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String allNumber = rs.getString("all_pokemon_id");
				int galarNumber = rs.getInt("galar_pokemon_id");
				String name = rs.getString("name");
				String type1 = typeDao.typeName(rs.getInt("type_id1"));
				String type2 = typeDao.typeName(rs.getInt("type_id2"));
				String characteristic1 = rs.getString("characteristic1");
				String characteristic2 = rs.getString("characteristic2");
				String characteristicSecret = rs.getString("characteristic_Secret");
				double whight = rs.getInt("weight");
				String eggGroup1 = eggGroupDao.eggGroup(rs.getInt("egg_group1_id"));
				String eggGroup2 = eggGroupDao.eggGroup(rs.getInt("egg_group2_id"));

				int hp = rs.getInt("hp_race_value");
				int attack = rs.getInt("attack_race_value");
				int defense = rs.getInt("defense_race_value");
				int specialAttack = rs.getInt("special_attack_race_value");
				int specialDefense = rs.getInt("special_defense_race_value");
				int speed = rs.getInt("speed_race_value");

				pokemonBasicData pokemon = new pokemonBasicData(id, allNumber, galarNumber,
						name, type1, type2, characteristic1, characteristic2,
						characteristicSecret, whight, eggGroup1, eggGroup2, hp,
						attack, defense, specialAttack, specialDefense, speed);

				pokemonList.add(pokemon);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return pokemonList;
	}

	//ポケモンの全国図鑑番号を基にデータを取得する
	public pokemonBasicData pokemonDetail(int  number) {
		PokemonBasicDao pokemonBasicDao = new PokemonBasicDao();
		TypeDao typeDao = new TypeDao();
		EggGroupDao eggGroupDao = new EggGroupDao();
		Connection con = null;

		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM pokemon_basic WHERE all_pokemon_id = ?";

			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, number);
			ResultSet rs = st.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id = rs.getInt("id");
			String allNumber = rs.getString("all_pokemon_id");
			int galarNumber = rs.getInt("galar_pokemon_id");
			String name = rs.getString("name");
			String type1 = typeDao.typeName(rs.getInt("type_id1"));
			String type2 = typeDao.typeName(rs.getInt("type_id2"));
			String characteristic1 = rs.getString("characteristic1");
			String characteristic2 = rs.getString("characteristic2");
			String characteristicSecret = rs.getString("characteristic_Secret");
			double whight = rs.getInt("weight");
			String eggGroup1 = eggGroupDao.eggGroup(rs.getInt("egg_group1_id"));
			String eggGroup2 = eggGroupDao.eggGroup(rs.getInt("egg_group2_id"));

			int hp = rs.getInt("hp_race_value");
			int attack = rs.getInt("attack_race_value");
			int defense = rs.getInt("defense_race_value");
			int specialAttack = rs.getInt("special_attack_race_value");
			int specialDefense = rs.getInt("special_defense_race_value");
			int speed = rs.getInt("speed_race_value");


			int hpList[] = pokemonBasicDao.culculationHp(hp);
			int hp_v_max = hpList[0];
			int hp_v_none = hpList[1];
			int hp_0_none = hpList[2];

			int attackList[] = pokemonBasicDao.culculationStatus(attack);
			int attack_up_v_max =  attackList[0];
			int attack_v_max =  attackList[1];
			int attack_v_none =  attackList[2];
			int attack_down_v_none =  attackList[3];
			int attack_down_0_none =  attackList[4];

			int defenseList[] = pokemonBasicDao.culculationStatus(defense);
			int defense_up_v_max =  defenseList[0];
			int defense_v_max =  defenseList[1];
			int defense_v_none =  defenseList[2];
			int defense_down_v_none =  defenseList[3];
			int defense_down_0_none =  defenseList[4];

			int specialAttackList[] = pokemonBasicDao.culculationStatus(specialAttack);
			int specialAttack_up_v_max =  specialAttackList[0];
			int specialAttack_v_max =  specialAttackList[1];
			int specialAttack_v_none =  specialAttackList[2];
			int specialAttack_down_v_none =  specialAttackList[3];
			int specialAttack_down_0_none =  specialAttackList[4];

			int specialDefenseList[] = pokemonBasicDao.culculationStatus(specialDefense);
			int specialDefense_up_v_max =  specialDefenseList[0];
			int specialDefense_v_max =  specialDefenseList[1];
			int specialDefense_v_none =  specialDefenseList[2];
			int specialDefense_down_v_none =  specialDefenseList[3];
			int specialDefense_down_0_none =  specialDefenseList[4];

			int speedList[] = pokemonBasicDao.culculationStatus(speed);
			int speed_up_v_max =  speedList[0];
			int speed_v_max =  speedList[1];
			int speed_v_none =  speedList[2];
			int speed_down_v_none =  speedList[3];
			int speed_down_0_none =  speedList[4];


			return new pokemonBasicData(id, allNumber, galarNumber,
					name, type1, type2, characteristic1, characteristic2,
					characteristicSecret, whight, eggGroup1, eggGroup2, hp,
					attack, defense, specialAttack, specialDefense, speed,
					hp_v_max, hp_v_none, hp_0_none,
					attack_up_v_max, attack_v_max, attack_v_none, attack_down_v_none, attack_down_0_none,
					defense_up_v_max, defense_v_max, defense_v_none, defense_down_v_none, defense_down_0_none,
					specialAttack_up_v_max, specialAttack_v_max, specialAttack_v_none, specialAttack_down_v_none, specialAttack_down_0_none,
					specialDefense_up_v_max, specialDefense_v_max, specialDefense_v_none, specialDefense_down_v_none, specialDefense_down_0_none,
					speed_up_v_max, speed_v_max, speed_v_none, speed_down_v_none, speed_down_0_none);



		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ポケモンの実数値を計算するメソッド
	public int[] culculationHp(int h) {
		int[] culList = new int[3];

    	culList[0] = (h * 2 + 31 + 252 / 4) * 50 / 100 + 50 + 10;//h；個体値V、努力値極振り
    	culList[1] = (h * 2 + 31 + 0 / 4) * 50 / 100 + 50 + 10;//h；個体値V、努力値無振り
    	culList[2] = (h * 2 + 0 + 0 / 4) * 50 / 100 + 50 + 10;//h；個体値0、努力値無振り

    	return culList;
	}

	//hp以外を計算するためのメソッド
    public int[] culculationStatus(int status) {
    	int[] culList = new int[5];
    	culList[0] = (int) (((status * 2 + 31 + 252 / 4) * 50 / 100 + 5) * 1.1);//プラス補正、個体値V、努力値極振り
    	culList[1] = (status * 2 + 31 + 252 / 4) * 50 / 100 + 5;//個体値V、努力値極振り
    	culList[2] = (status * 2 + 31 + 0 / 4) * 50 / 100 + 5;//個体値V、努力値無振り
    	culList[3] = (int) (((status * 2 + 31 + 0 / 4) * 50 / 100 + 5) * 0.9);//マイナス補正、個体値31、努力値無振り
    	culList[4] = (int) (((status * 2 + 0 + 0 / 4) * 50 / 100 + 5) * 0.9);//マイナス補正、個体値0、努力値無振り

    	return culList;
	}
}
