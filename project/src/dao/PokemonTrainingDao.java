package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.pokemonTrainingData;

public class PokemonTrainingDao {

	//ポケモンの育成情報登録
	public void insert(String pokemonId, String userId, String nickname, String characteristic, String correction,
			String item, String skill1, String skill2, String skill3, String skill4,
			String hp_EffortValue, String attack_EffortValue, String defense_EffortValue,
			String specialAttack_EffortValue, String specialDefense_EffortValue,String speed_EffortValue,
			String hp_IndividualValue, String attack_IndividualValue, String defense_IndividualValue,
			String specialAttack_IndividualValue, String specialDefense_IndividualValue, String speed_IndividualValue,
			String remarks) {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO training_pokemon(" +
					"	pokemon_id, " +
					"	user_id, " +
					"	nickname, " +
					"	correction, " +
					"	item, " +
					"	characteristic, " +
					"	skill1, " +
					"	skill2, " +
					"	skill3, " +
					"	skill4, " +

					"	hp_effort_value, " +
					"	attack_effort_value, " +
					"	defense_effort_value," +
					"	special_attack_effort_value, " +
					"	special_defense_effort_value, " +
					"	speed_effort_value, " +

					"	hp_individual_value," +
					"	attack_individual_value, " +
					"	defense_individual_value, " +
					"	special_attack_individual_value, " +
					"	special_defense_individual_value," +
					"	speed_individual_value, " +

					"	remarks, " +
					"	create_id " +
					"	) " +

					"	VALUES(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?)");


			PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();

			st.setString(1, pokemonId);//ポケモン名前をidに変換
			st.setString(2, userId);//loginId
			st.setString(3, nickname);//ニックネーム
			st.setString(4, pokemonTrainingDao.correction(correction));//性格補正
			st.setString(5, item);//所持アイテム
			st.setString(6, characteristic);//特性

			st.setString(7, skill1);//技4つ
			st.setString(8, skill2);
			st.setString(9, skill3);
			st.setString(10, skill4);

			//このままだと何も入力されてない欄に数字以外のものが入るため以下の処理が必要
			st.setString(11, pokemonTrainingDao.hpEffortValue(hp_EffortValue));
			st.setString(12, pokemonTrainingDao.attackEffortValue(attack_EffortValue));
			st.setString(13, pokemonTrainingDao.defenseEffortValue(defense_EffortValue));
			st.setString(14, pokemonTrainingDao.specialAttackEffortValue(specialAttack_EffortValue));
			st.setString(15, pokemonTrainingDao.specialDefenseEffortValue(specialDefense_EffortValue));
			st.setString(16, pokemonTrainingDao.speedEffortValue(speed_EffortValue));

			st.setString(17, hp_IndividualValue);//個体値String型
			st.setString(18, attack_IndividualValue);
			st.setString(19, defense_IndividualValue);
			st.setString(20, specialAttack_IndividualValue);
			st.setString(21, specialDefense_IndividualValue);
			st.setString(22, speed_IndividualValue);

			st.setString(23, remarks);//備考欄
			////ユーザIDを送ることでcreate_idの最後のレコードに1を足したものが返ってくる
			st.setInt(24, pokemonTrainingDao.getCreateId(userId));
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//ユーザごとの育成リストの作成順をカウントする
	public int getCreateId(String userId) {

		Connection con = null;
		try{

			con = DBManager.getConnection();


			//user_idで検索をかけて一番後に作成されたレコードを1件だけ呼び出す
			String sql = "SELECT * FROM training_pokemon " +
					"where user_id = ? " +
					"order by create_id desc limit 1";

			PreparedStatement st = con.prepareStatement(sql);

			st.setString(1, userId);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {

				int userCreateId = Integer.parseInt(rs.getString("create_id"));

				//呼び出したレコードに1を足していく
				userCreateId += 1;

				return userCreateId;



			}
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
		//データベースにデータがない、新しく登録するユーザの場合は1が返る仕組み
		return 1;

	}

	//ポケモンの育成情報取得
	public List<pokemonTrainingData> findPokemonTrainedList(String loginId){
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		PokemonBasicDao pokemonbasicDao = new PokemonBasicDao();
		Connection con = null;
		List<pokemonTrainingData> ptList = new ArrayList<pokemonTrainingData>();
		try{

			con = DBManager.getConnection();


			String sql = "SELECT * FROM training_pokemon WHERE user_id = ?";

			PreparedStatement st = con.prepareStatement(sql);

			st.setString(1, loginId);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String pokemonName = pokemonbasicDao.pokemonName(rs.getString("pokemon_id"));
				String pokemonId = rs.getString("pokemon_id");
				int userId = rs.getInt("user_id");
				String nickname = pokemonTrainingDao.nickname1(rs.getString("nickname"), pokemonName);
				String item = rs.getString("item");
				String characteristic = rs.getString("characteristic");


				int h_ev = rs.getInt("hp_effort_value");
				int a_ev = rs.getInt("attack_effort_value");
				int b_ev = rs.getInt("defense_effort_value");
				int c_ev = rs.getInt("special_attack_effort_value");
				int d_ev = rs.getInt("special_defense_effort_value");
				int s_ev = rs.getInt("speed_effort_value");

				int createId = rs.getInt("create_id");



				pokemonTrainingData pokemon = new pokemonTrainingData(id, pokemonName, pokemonId,
						userId, nickname, characteristic, item, h_ev, a_ev, b_ev, c_ev, d_ev, s_ev, createId);

				ptList.add(pokemon);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return ptList;
	}

	//postの方で使うリスト
	public pokemonTrainingData findPokemonTrainedList2(String loginId, String createId){
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		CorrectionDao correctionDao = new CorrectionDao();
		Connection con = null;
		try{

			con = DBManager.getConnection();


			String sql = "SELECT" +
					"	training_pokemon.pokemon_id," +
					"	training_pokemon.nickname," +
					"	training_pokemon.characteristic," +
					"	training_pokemon.item," +
					"	training_pokemon.correction," +
					"	training_pokemon.skill1," +
					"	training_pokemon.skill2," +
					"	training_pokemon.skill3," +
					"	training_pokemon.skill4," +

					"	training_pokemon.hp_effort_value," +
					"	training_pokemon.attack_effort_value," +
					"	training_pokemon.defense_effort_value," +
					"	training_pokemon.special_attack_effort_value," +
					"	training_pokemon.special_defense_effort_value," +
					"	training_pokemon.speed_effort_value," +

					"	training_pokemon.hp_individual_value," +
					"	training_pokemon.attack_individual_value," +
					"	training_pokemon.defense_individual_value," +
					"	training_pokemon.special_attack_individual_value," +
					"	training_pokemon.special_defense_individual_value," +
					"	training_pokemon.speed_individual_value," +

					"	training_pokemon.remarks," +
					"	training_pokemon.create_id," +

					"	pokemon_basic.all_pokemon_id," +
					"	pokemon_basic.hp_race_value," +
					"	pokemon_basic.attack_race_value," +
					"	pokemon_basic.defense_race_value," +
					"	pokemon_basic.special_defense_race_value," +
					"	pokemon_basic.special_attack_race_value," +
					"	pokemon_basic.speed_race_value " +

					"FROM " +
					"	training_pokemon INNER JOIN pokemon_basic " +
					"ON " +
					"	training_pokemon.pokemon_id = pokemon_basic.all_pokemon_id " +
					"WHERE " +
					"	user_id = ? " +
					"AND " +
					" training_pokemon.create_id = ?";

			PreparedStatement st = con.prepareStatement(sql);

			st.setString(1, loginId);//user_id
			st.setString(2, createId);
			ResultSet rs = st.executeQuery();

			//PokemonBasicDaoの方から種族値をとろうとしていたものの遠回りであると感じ以下は没にした
//			List<String> prvList = pokemonBasicDao.pokemonRealValue(name);
//
//			String[] array = new String[prvList.size()];
//			prvList.toArray(array);

//			int h_rv = Integer.parseInt(array[0]);
//			int a_rv = Integer.parseInt(array[1]);
//			int b_rv = Integer.parseInt(array[2]);
//			int c_rv = Integer.parseInt(array[3]);
//			int d_rv = Integer.parseInt(array[4]);
//			int s_rv = Integer.parseInt(array[5]);
//			int[] array = new int[prvList.size()];
//
//			for(int i = 0; i < array.length; i++) {
//				array[i] = Integer.parseInt(prvList.get(i));
//			}

//			int[] raceValue = new int[0]);
//			int h_rv = raceValue[0];
//			int a_rv = raceValue[1];
//			int b_rv = raceValue[2];
//			int c_rv = raceValue[3];
//			int d_rv = raceValue[4];
//			int s_rv = raceValue[5];

			if (!rs.next()) {
				return null;
			}
				String pokemonId = rs.getString("pokemon_id");
				String nickname = pokemonTrainingDao.nickname2(rs.getString("nickname"));
				String item = rs.getString("item");
				String characteristic = rs.getString("characteristic");
				int correction = rs.getInt("correction");

				String skill1 = pokemonTrainingDao.skill1(rs.getString("skill1"));
				String skill2 = pokemonTrainingDao.skill2(rs.getString("skill2"));
				String skill3 = pokemonTrainingDao.skill3(rs.getString("skill3"));
				String skill4 = pokemonTrainingDao.skill4(rs.getString("skill4"));

				int h_ev = rs.getInt("hp_effort_value");
				int a_ev = rs.getInt("attack_effort_value");
				int b_ev = rs.getInt("defense_effort_value");
				int c_ev = rs.getInt("special_attack_effort_value");
				int d_ev = rs.getInt("special_defense_effort_value");
				int s_ev = rs.getInt("speed_effort_value");

				String h_iv = pokemonTrainingDao.hRaceValueConversion(rs.getString("hp_individual_value"));
				String a_iv = pokemonTrainingDao.aRaceValueConversion(rs.getString("attack_individual_value"));
				String b_iv = pokemonTrainingDao.bRaceValueConversion(rs.getString("defense_individual_value"));
				String c_iv = pokemonTrainingDao.cRaceValueConversion(rs.getString("special_attack_individual_value"));
				String d_iv = pokemonTrainingDao.dRaceValueConversion(rs.getString("special_defense_individual_value"));
				String s_iv = pokemonTrainingDao.sRaceValueConversion(rs.getString("speed_individual_value"));

				int h_rv = rs.getInt("hp_race_value");
				int a_rv = rs.getInt("attack_race_value");
				int b_rv = rs.getInt("defense_race_value");
				int c_rv = rs.getInt("special_attack_race_value");
				int d_rv = rs.getInt("special_defense_race_value");
				int s_rv = rs.getInt("speed_race_value");

				String remarks = rs.getString("remarks");
				int getId = rs.getInt("create_id");


				//ステータスの実数値を計算するため以下のメソッドに送る
				int culList[] = pokemonTrainingDao.culculation(correction, h_rv, a_rv, b_rv, c_rv, d_rv, s_rv,
						h_ev, a_ev, b_ev, c_ev, d_ev, s_ev);

				int hp = culList[0];
				int attack = culList[1];
				int defense = culList[2];
				int specialAttack = culList[3];
				int specialDefense = culList[4];
				int speed = culList[5];

				return new pokemonTrainingData(pokemonId, nickname, characteristic, correctionDao.correctionName(correction), item,
						skill1, skill2, skill3, skill4, h_iv, a_iv, b_iv, c_iv,d_iv, s_iv, remarks, getId,
						hp, attack, defense, specialAttack, specialDefense, speed);

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

	public pokemonTrainingData findPokemonTrainedList3(String loginId, int createId){
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		CorrectionDao correctionDao = new CorrectionDao();
		Connection con = null;

		try{

			con = DBManager.getConnection();


			String sql = "SELECT" +
					"	training_pokemon.pokemon_id," +
					"	training_pokemon.nickname," +
					"	training_pokemon.characteristic," +
					"	training_pokemon.item," +
					"	training_pokemon.correction," +
					"	training_pokemon.skill1," +
					"	training_pokemon.skill2," +
					"	training_pokemon.skill3," +
					"	training_pokemon.skill4," +

					"	training_pokemon.hp_effort_value," +
					"	training_pokemon.attack_effort_value," +
					"	training_pokemon.defense_effort_value," +
					"	training_pokemon.special_attack_effort_value," +
					"	training_pokemon.special_defense_effort_value," +
					"	training_pokemon.speed_effort_value," +

					"	training_pokemon.hp_individual_value," +
					"	training_pokemon.attack_individual_value," +
					"	training_pokemon.defense_individual_value," +
					"	training_pokemon.special_attack_individual_value," +
					"	training_pokemon.special_defense_individual_value," +
					"	training_pokemon.speed_individual_value," +

					"	training_pokemon.remarks," +
					"	training_pokemon.create_id," +

					"	pokemon_basic.all_pokemon_id," +
					"	pokemon_basic.hp_race_value," +
					"	pokemon_basic.attack_race_value," +
					"	pokemon_basic.defense_race_value," +
					"	pokemon_basic.special_defense_race_value," +
					"	pokemon_basic.special_attack_race_value," +
					"	pokemon_basic.speed_race_value " +

					"FROM " +
					"	training_pokemon INNER JOIN pokemon_basic " +
					"ON " +
					"	training_pokemon.pokemon_id = pokemon_basic.all_pokemon_id " +
					"WHERE " +
					"	user_id = ? " +
					"AND " +
					" training_pokemon.create_id = ?";

			PreparedStatement st = con.prepareStatement(sql);

			st.setString(1, loginId);//user_id
			st.setInt(2, createId);
			ResultSet rs = st.executeQuery();


			if (!rs.next()) {
				return null;
			}

				String pokemonId = rs.getString("pokemon_id");
				String nickname = pokemonTrainingDao.nickname2(rs.getString("nickname"));
				String item = rs.getString("item");
				String characteristic = rs.getString("characteristic");
				int correction = rs.getInt("correction");

				String skill1 = rs.getString("skill1");
				String skill2 = rs.getString("skill2");
				String skill3 = rs.getString("skill3");
				String skill4 = rs.getString("skill4");

				int h_ev = rs.getInt("hp_effort_value");
				int a_ev = rs.getInt("attack_effort_value");
				int b_ev = rs.getInt("defense_effort_value");
				int c_ev = rs.getInt("special_attack_effort_value");
				int d_ev = rs.getInt("special_defense_effort_value");
				int s_ev = rs.getInt("speed_effort_value");

				String h_iv = pokemonTrainingDao.hRaceValueConversion(rs.getString("hp_individual_value"));
				String a_iv = pokemonTrainingDao.aRaceValueConversion(rs.getString("attack_individual_value"));
				String b_iv = pokemonTrainingDao.bRaceValueConversion(rs.getString("defense_individual_value"));
				String c_iv = pokemonTrainingDao.cRaceValueConversion(rs.getString("special_attack_individual_value"));
				String d_iv = pokemonTrainingDao.dRaceValueConversion(rs.getString("special_defense_individual_value"));
				String s_iv = pokemonTrainingDao.sRaceValueConversion(rs.getString("speed_individual_value"));

				String remarks = rs.getString("remarks");
				int getCreateId = rs.getInt("create_Id");


				return new pokemonTrainingData(pokemonId, nickname, characteristic, correctionDao.correctionName(correction),
						item, skill1, skill2, skill3, skill4,
						 h_ev, a_ev, b_ev, c_ev, d_ev, s_ev,
						h_iv, a_iv, b_iv, c_iv,d_iv, s_iv, remarks, getCreateId);


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public pokemonTrainingData findPokemonTrainedList4(String loginId, int createId){
		PokemonBasicDao pokemonBasicDao = new PokemonBasicDao();
		Connection con = null;

		try{

			con = DBManager.getConnection();


			String sql = "SELECT" +
					"	training_pokemon.pokemon_id, " +
					"	training_pokemon.nickname, " +

					"	pokemon_basic.all_pokemon_id" +

					"	FROM " +
					"	training_pokemon " +
					"	INNER JOIN " +
					"	pokemon_basic " +
					"	ON \r\n" +
					"	training_pokemon.pokemon_id = pokemon_basic.all_pokemon_id " +
					"	WHERE " +
					"	user_id = ? " +
					"	AND " +
					"	training_pokemon.create_id = ?";

			PreparedStatement st = con.prepareStatement(sql);

			st.setString(1, loginId);//user_id
			st.setInt(2, createId);
			ResultSet rs = st.executeQuery();


			if (!rs.next()) {
				return null;
			}

				String pokemonId = rs.getString("pokemon_id");
				String pokemonName = pokemonBasicDao.pokemonName(rs.getString("pokemon_id"));
				String nickname = rs.getString("nickname");



				//ステータスの字数値を計算するため以下のメソッドに送る

				return new pokemonTrainingData(pokemonName, nickname, pokemonId, createId);


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//育成ポケモン更新機能
	public void update(String nickname, String characteristic, String correction,
	String item, String skill1, String skill2, String skill3, String skill4,
	String h_ev, String a_ev, String b_ev, String c_ev, String d_ev, String s_ev,
	String h_iv, String a_iv, String b_iv, String c_iv, String d_iv, String s_iv, String remarks,
	String loginId, int createId) {

		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con= DBManager.getConnection();

			st = con.prepareStatement("UPDATE training_pokemon SET"

				+ " nickname = ?, characteristic = ?, correction = ?, item = ?, "

				+ " skill1 = ?, skill2 = ?, skill3 = ?, skill4 = ?, "

				+ " hp_effort_value = ?, attack_effort_value = ?, defense_effort_value = ?, special_attack_effort_value = ?, "

				+ " special_defense_effort_value = ?, speed_effort_value = ?, "

				+ " hp_individual_value = ?, attack_individual_value = ?, defense_individual_value = ?, special_attack_individual_value = ?, "

				+ " special_defense_individual_value = ?, speed_individual_value = ?, "

				+ " remarks = ? "

				+ "WHERE user_id = ? AND create_id = ?");


		st.setString(1, nickname);
		st.setString(2, characteristic);
		st.setString(3, correction);
		st.setString(4, item);
		st.setString(5, skill1);
		st.setString(6, skill2);
		st.setString(7, skill3);
		st.setString(8, skill4);

		//このままだと何も入力されてない欄に数字以外のものが入るため以下の処理が必要
		st.setString(9, pokemonTrainingDao.hpEffortValue(h_ev));
		st.setString(10, pokemonTrainingDao.attackEffortValue(a_ev));
		st.setString(11, pokemonTrainingDao.defenseEffortValue(b_ev));
		st.setString(12, pokemonTrainingDao.specialAttackEffortValue(c_ev));
		st.setString(13, pokemonTrainingDao.specialDefenseEffortValue(d_ev));
		st.setString(14, pokemonTrainingDao.speedEffortValue(s_ev));

		st.setString(15, h_iv);
		st.setString(16, a_iv);
		st.setString(17, b_iv);
		st.setString(18, c_iv);
		st.setString(19, d_iv);
		st.setString(20, s_iv);

		st.setString(21, remarks);
		st.setString(22, loginId);//user_id
		st.setInt(23, createId);

		st.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//育成ポケモン削除機能
	public void delete(String loginId, int createId) {

		String cId = String.valueOf(createId);
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			String sql = "DELETE FROM training_pokemon WHERE user_id=? AND create_id=?";

			st = con.prepareStatement(sql);
			st.setString(1, loginId);
			st.setString(2, cId);
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	//createIdをString型からデータ型にするためのメソッド
	public pokemonTrainingData cId(String createId) {

		Connection con = null;
		PreparedStatement st = null;
		try {

			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM training_pokemon WHERE create_id = ?");
			st.setString(1, createId);
			ResultSet rs = st.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int cId = rs.getInt("create_id");
			return new pokemonTrainingData(cId);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


	//実数値を計算するメソッド、switch文で21の分岐がある
	public int[] culculation(int correction, int h_rv, int a_rv, int b_rv, int c_rv, int d_rv, int s_rv,
			int h_ev, int a_ev, int b_ev, int c_ev, int d_ev, int s_ev) {
		int[] culList = new int[6];

		//0はHP、1は攻撃、2は防御、3は特攻、4は特防、5は素早さ
		switch (correction) {
	    case 1:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;//h
	    	culList[1] = (int) (((a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5) * 1.1);//a
	    	culList[2] = (int) (((b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5) * 0.9);//b
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;//c
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;//d
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;//s
	    	break;

	    case 2:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (int) (((a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (int) (((c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;
	    	break;

	    case 3:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (int) (((a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;
	    	culList[4] = (int) (((d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;
	    	break;

	    case 4:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (int) (((a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;
	    	culList[5] = (int) (((s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5) * 0.9);
	    	break;

	    case 5:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (int) (((a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[2] = (int) (((b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;
	    	break;

	    case 6:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (int) (((b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[3] = (int) (((c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;
	    	break;

	    case 7:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (int) (((b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;
	    	culList[4] = (int) (((d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;
	    	break;

	    case 8:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (int) (((b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;
	    	culList[5] = (int) (((s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5) * 0.9);
	    	break;

	    case 9:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (int) (((a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (int) (((c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;
	    	break;

	    case 10:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (int) (((b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[3] = (int) (((c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;
	    	break;

	    case 11:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (int) (((c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[4] = (int) (((d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;
	    	break;

	    case 12:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (int) (((c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;
	    	culList[5] = (int) (((s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5) * 0.9);
	    	break;

	    case 13:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (int) (((a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;
	    	culList[4] = (int) (((d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;
	    	break;

	    case 14:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (int) (((b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;
	    	culList[4] = (int) (((d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;
	    	break;

	    case 15:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (int) (((c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[4] = (int) (((d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;
	    	break;

	    case 16:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;
	    	culList[4] = (int) (((d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5) * 1.1);
	    	culList[5] = (int) (((s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5) * 0.9);
	    	break;

	    case 17:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (int) (((a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;
	    	culList[5] = (int) (((s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5) * 1.1);
	    	break;

	    case 18:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (int) (((b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;
	    	culList[5] = (int) (((s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5) * 1.1);
	    	break;

	    case 19:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (int) (((c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;
	    	culList[5] = (int) (((s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5) * 1.1);
	    	break;

	    case 20:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;
	    	culList[4] = (int) (((d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5) * 0.9);
	    	culList[5] = (int) (((s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5) * 1.1);
	    	break;

	    case 21:
	    	culList[0] = (h_rv * 2 + 31 + h_ev / 4) * 50 / 100 + 50 + 10;
	    	culList[1] = (a_rv * 2 + 31 + a_ev / 4) * 50 / 100 + 5;
	    	culList[2] = (b_rv * 2 + 31 + b_ev / 4) * 50 / 100 + 5;
	    	culList[3] = (c_rv * 2 + 31 + c_ev / 4) * 50 / 100 + 5;
	    	culList[4] = (d_rv * 2 + 31 + d_ev / 4) * 50 / 100 + 5;
	    	culList[5] = (s_rv * 2 + 31 + s_ev / 4) * 50 / 100 + 5;
	    	break;

		}

		return culList;
	}

	//ニックネームがない場合の処理を行う


	//ニックネームがない場合代わりにそのポケモンのもともとの名前に置き換える
	public String nickname1(String nickname, String pokemonName) {
		if(nickname == "") {
			return pokemonName;
		}
		return nickname;
	}

	//ニックネームがない場合の処理を行う
	public String nickname2(String nickname) {
		if(nickname == "") {
			return "\"ニックネームの設定がされていません\"";
		}
		return nickname;
	}

	public String skill1(String skill1) {
		if(skill1 == "") {
			return "　　　　　　　　";
		}
		return skill1;
	}

	public String skill2(String skill2) {
		if(skill2 == "") {
			return "　　　　　　　　";
		}
		return skill2;
	}

	public String skill3(String skill3) {
		if(skill3 == "") {
			return "　　　　　　　　";
		}
		return skill3;
	}

	public String skill4(String skill4) {
		if(skill4 == "") {
			return "　　　　　　　　";
		}
		return skill4;
	}


	//性格のIDが未入力の場合、自動的に無補正である21を入れるメソッド
	public String correction(String correction) {
		if(correction == null) {
			return "21";
		}
		return correction;
	}

	//努力値の値が何も入力されなかった場合自動的に0を入力するメソッド
	public String hpEffortValue(String hp_EffortValue) {
		if(hp_EffortValue == "") {
			return "0";
		}
		return hp_EffortValue;
	}

	public String attackEffortValue(String attack_EffortValue) {
		if(attack_EffortValue == "") {
			return "0";
		}
		return attack_EffortValue;
	}

	public String defenseEffortValue(String defense_EffortValue) {
		if(defense_EffortValue == "") {
			return "0";
		}
		return defense_EffortValue;
	}

	public String specialAttackEffortValue(String specialAttack_EffortValue) {
		if(specialAttack_EffortValue == "") {
			return "0";
		}
		return specialAttack_EffortValue;
	}

	public String specialDefenseEffortValue(String SpecialDefense_EffortValue) {
		if(SpecialDefense_EffortValue == "") {
			return "0";
		}
		return SpecialDefense_EffortValue;
	}

	public String speedEffortValue(String SpeedEffortValue) {
		if(SpeedEffortValue == "") {
			return "0";
		}
		return SpeedEffortValue;
	}


	//個体値の値がonで格納されているので最高値31かそうでなければ最低値0で返す
//	public int hReceValueConversion(String h_iv) {
//		if(h_iv == "on") {
//			return 31;
//		}
//		return 0;
//	}
//
//	public int aReceValueConversion(String a_iv) {
//		if(a_iv == "on") {
//			return 31;
//		}
//		return 0;
//	}
//
//	public String cEffortValue(String c_EffortValue) {
//		if(c_EffortValue == "") {
//			return "0";
//		}
//		return c_EffortValue;
//	}
//
//	public String dEffortValue(String d_EffortValue) {
//		if(d_EffortValue == "") {
//			return "0";
//		}
//		return d_EffortValue;
//	}
//
//	public String sEffortValue(String s_EffortValue) {
//		if(s_EffortValue == "") {
//			return "0";
//		}
//		return s_EffortValue;
//	}


	//個体値のチェックが入っていてonの値が入っている場合
	public String hRaceValueConversion(String h_iv) {
		if(h_iv != null) {
			return "31";
		}
		return "0～30";
	}

	public String aRaceValueConversion(String a_iv) {
		if(a_iv != null) {
			return "31";
		}
		return "0～30";
	}

	public String bRaceValueConversion(String b_iv) {
		if(b_iv != null) {
			return "31";
		}
		return "0～30";
	}

	public String cRaceValueConversion(String c_iv) {
		if(c_iv != null) {
			return "31";
		}
		return "0～30";
	}

	public String dRaceValueConversion(String d_iv) {
		if(d_iv != null) {
			return "31";
		}
		return "0～30";
	}

	public String sRaceValueConversion(String s_iv) {
		if(s_iv != null) {
			return "31";
		}
		return "0～30";
	}


}
