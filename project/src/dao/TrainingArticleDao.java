package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.trainingArticleData;

public class TrainingArticleDao {

	//育成論新規登録
	public void insert(String userId, String title, String name, String characteristic, String correction, String item,
			String skill1, String skill2, String skill3, String skill4,
			String hp_EffortValue, String attack_EffortValue, String defense_EffortValue, String specialAttack_EffortValue,
			String specialDefense_EffortValue, String speed_EffortValue, String hp_IndividualValue, String attack_IndividualValue,
			String defense_IndividualValue, String specialAttack_IndividualValue, String specialDefense_IndividualValue,
			String speed_IndividualValue, String consideration, String userName) {

		PokemonBasicDao pokemonBasicDao = new PokemonBasicDao();
		TrainingArticleDao trainingArticleDao = new TrainingArticleDao();
		CorrectionDao correctionDao = new CorrectionDao() ;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO training_article(" +
					"	user_id, " +
					"	pokemon_id, " +

					"	title, " +
					"	update_data, " +
					"	characteristic, " +
					"	correction, " +
					"	item, " +

					"	skill1, " +
					"	skill2, " +
					"	skill3, " +
					"	skill4, " +

					"	hp_effort_value, " +
					"	attack_effort_value, " +
					"	defense_effort_value," +
					"	special_attack_effort_value, " +
					"	special_defense_effort_value, " +
					"	speed_effort_value, " +

					"	hp_individual_value," +
					"	attack_individual_value, " +
					"	defense_individual_value, " +
					"	special_attack_individual_value, " +
					"	special_defense_individual_value," +
					"	speed_individual_value, " +

					"	consideration, " +
					"	name, " +
					"	create_id," +
					"	user_name" +
					"	) " +

					"	VALUES(?,?,?,now(),?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?)");

			st.setString(1, userId);
			st.setString(2, pokemonBasicDao.nameId(name));//ポケモン名前をidに変換

			st.setString(3, title);
			st.setString(4, characteristic);//特性
			st.setString(5, correctionDao.correctionId(correction));//性格補正
			st.setString(6, item);//所持アイテム


			st.setString(7, skill1);//技4つ
			st.setString(8, skill2);
			st.setString(9, skill3);
			st.setString(10, skill4);

			//このままだと何も入力されてない欄に数字以外のものが入るため、その場合に0が入るように処理するメソッド
			st.setString(11, hp_EffortValue);
			st.setString(12, attack_EffortValue);
			st.setString(13, defense_EffortValue);
			st.setString(14, specialAttack_EffortValue);
			st.setString(15, specialDefense_EffortValue);
			st.setString(16, speed_EffortValue);

			st.setString(17, hp_IndividualValue);//個体値String型
			st.setString(18, attack_IndividualValue);
			st.setString(19, defense_IndividualValue);
			st.setString(20, specialAttack_IndividualValue);
			st.setString(21, specialDefense_IndividualValue);
			st.setString(22, speed_IndividualValue);

			st.setString(23, consideration);//考察
			st.setString(24, name);//ネーム、データベースで確認するために一応
			st.setInt(25, trainingArticleDao.getCreateId(userId));
			st.setString(26, userName);
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}


	//削除用メソッド
	public void delete(String loginId, int createId){

		String cId = String.valueOf(createId);
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			String sql = "DELETE FROM training_article WHERE user_id=? AND create_id=?";

			st = con.prepareStatement(sql);
			st.setString(1, loginId);
			st.setString(2, cId);
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	//更新用メソッド
	public void update(String title, String name, String characteristic, String correction, String item, String skill1, String skill2, String skill3, String skill4,
			String hp_EffortValue, String attack_EffortValue, String defense_EffortValue, String specialAttack_EffortValue,
			String specialDefense_EffortValue, String speed_EffortValue, String hp_IndividualValue, String attack_IndividualValue,
			String defense_IndividualValue, String specialAttack_IndividualValue, String specialDefense_IndividualValue,
			String speed_IndividualValue, String consideration, String loginId, int createId){

		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		CorrectionDao correctionDao = new CorrectionDao() ;
		//String cId = String.valueOf(createId);
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			String sql = "UPDATE training_article SET"

					+ " title = ?, name = ?, characteristic = ?, correction = ?, item = ?, skill1 = ?, skill2 = ?,"

					+ " skill3 = ?, skill4 = ?, hp_effort_value = ?, attack_effort_value = ?, defense_effort_value = ?, "

					+ " special_attack_effort_value = ?, special_defense_effort_value = ?, speed_effort_value = ?, "

					+ " hp_individual_value = ?, attack_individual_value = ?, defense_individual_value = ?, special_attack_individual_value = ?, "

					+ " special_defense_individual_value = ?, speed_individual_value = ?, consideration = ?"

					+ " WHERE user_id = ? AND create_id = ?";

			st = con.prepareStatement(sql);
			st.setString(1, title);
			st.setString(2, name);
			st.setString(3, characteristic);
			st.setString(4, correctionDao.correctionId(correction));
			st.setString(5, item);

			st.setString(6, skill1);
			st.setString(7, skill2);
			st.setString(8, skill3);
			st.setString(9, skill4);

			st.setString(10, pokemonTrainingDao.hpEffortValue(hp_EffortValue));
			st.setString(11, pokemonTrainingDao.attackEffortValue(attack_EffortValue));
			st.setString(12, pokemonTrainingDao.defenseEffortValue(defense_EffortValue));
			st.setString(13, pokemonTrainingDao.specialAttackEffortValue(specialAttack_EffortValue));
			st.setString(14, pokemonTrainingDao.specialDefenseEffortValue(specialDefense_EffortValue));
			st.setString(15, pokemonTrainingDao.speedEffortValue(speed_EffortValue));

			st.setString(16, hp_IndividualValue);
			st.setString(17, attack_IndividualValue);
			st.setString(18, defense_IndividualValue);
			st.setString(19, specialAttack_IndividualValue);
			st.setString(20, specialDefense_IndividualValue);
			st.setString(21, speed_IndividualValue);

			st.setString(22, consideration);
			st.setString(23, loginId);
			st.setInt(24, createId);

			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//ユーザごとの育成リストの作成順をカウントする
	public int getCreateId(String userId) {

		Connection con = null;
		try{

			con = DBManager.getConnection();


			//user_idで検索をかけて一番後に作成されたレコードを1件だけ呼び出す
			String sql = "SELECT * FROM training_article " +
					"where user_id = ? " +
					"order by create_id desc limit 1";

			PreparedStatement st = con.prepareStatement(sql);

			st.setString(1, userId);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {

				int userCreateId = Integer.parseInt(rs.getString("create_id"));

				//呼び出したレコードに1を足していく
				userCreateId += 1;

				return userCreateId;



			}
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
		//データベースにデータがない、新しく登録するユーザの場合は1が返る仕組み
		return 1;
	}

	//ユーザのIDで検索をかけリストで取得
	public List<trainingArticleData> findTrainingArticleList(String loginId){
		Connection con = null;
		List<trainingArticleData> taList = new ArrayList<trainingArticleData>();

		try{

			con = DBManager.getConnection();


			String sql = "SELECT * FROM training_article WHERE user_id = ?";

			PreparedStatement st = con.prepareStatement(sql);

			st.setString(1, loginId);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				String id = rs.getString("user_id");
				String pokemonId = rs.getString("pokemon_id");
				String name = rs.getString("name");
				String title = rs.getString("title");
				String characteristic = rs.getString("characteristic");
				String item = rs.getString("item");

				String hp_EffortValue = rs.getString("hp_effort_value");
				String attack_EffortValue = rs.getString("attack_effort_value");
				String defense_EffortValue = rs.getString("defense_effort_value");
				String specialAttack_EffortValue = rs.getString("special_attack_effort_value");
				String special_Defense_EffortValue = rs.getString("special_defense_effort_value");
				String speed_EffortValue = rs.getString("speed_effort_value");

				int createId =  Integer.parseInt(rs.getString("create_id"));



				trainingArticleData article = new trainingArticleData(
						id, pokemonId, name, title, characteristic, item,
						hp_EffortValue, attack_EffortValue, defense_EffortValue,
						specialAttack_EffortValue, special_Defense_EffortValue, speed_EffortValue, createId
						);

				taList.add(article);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return taList;
	}

	public trainingArticleData findTrainingArticleList2(String loginId, int createId){
		CorrectionDao correctionDao = new CorrectionDao();
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		Connection con = null;
		try{

			con = DBManager.getConnection();


			String sql = "SELECT"+
					"	training_article.user_id, "+
					"	training_article.name, "+
					"	training_article.update_data, "+
					"	training_article.title, "+
					"	training_article.characteristic, "+
					"	training_article.correction, "+
					"	training_article.item, "+


					"	training_article.skill1, "+
					"	training_article.skill2, "+
					"	training_article.skill3, "+
					"	training_article.skill4, "+

					"	training_article.hp_effort_value, "+
					"	training_article.attack_effort_value, "+
					"	training_article.defense_effort_value, "+
					"	training_article.special_attack_effort_value, "+
					"	training_article.special_defense_effort_value, "+
					"	training_article.speed_effort_value, "+

					"	training_article.hp_individual_value, "+
					"	training_article.attack_individual_value, "+
					"	training_article.defense_individual_value, "+
					"	training_article.special_attack_individual_value, "+
					"	training_article.special_defense_individual_value, "+
					"	training_article.speed_individual_value, "+

					"	training_article.consideration, "+
					"	training_article.create_id, "+
					"	training_article.user_name, "+

					"	pokemon_basic.all_pokemon_id, "+

					"	pokemon_basic.hp_race_value, "+
					"	pokemon_basic.attack_race_value,"+
					"	pokemon_basic.defense_race_value, "+
					"	pokemon_basic.special_defense_race_value, "+
					"	pokemon_basic.special_attack_race_value, "+
					"	pokemon_basic.speed_race_value "+

					"FROM "+
					"	training_article INNER JOIN pokemon_basic "+
					"ON "+
					"	training_article.pokemon_id = pokemon_basic.all_pokemon_id "+
					"WHERE "+
					"	user_id = ? "+
					"AND "+
					"	training_article.create_id = ?";

			PreparedStatement st = con.prepareStatement(sql);

			st.setString(1, loginId);//user_id
			st.setInt(2, createId);//create_id
			ResultSet rs = st.executeQuery();

			if (!rs.next()) {
				return null;
			}
				String id = rs.getString("user_id");
				String name = rs.getString("name");
				String updateTime = rs.getString("update_data");
				String title = rs.getString("title");
				String characteristic = rs.getString("characteristic");
				int correction = rs.getInt("correction");
				String item = rs.getString("item");
				String pokemonId = rs.getString("all_pokemon_id");

				String skill1 = rs.getString("skill1");
				String skill2 = rs.getString("skill2");
				String skill3 = rs.getString("skill3");
				String skill4 = rs.getString("skill4");

				String hp_EffortValue = rs.getString("hp_effort_value");
				String attack_EffortValue = rs.getString("attack_effort_value");
				String defense_EffortValue = rs.getString("defense_effort_value");
				String specialAttack_EffortValue = rs.getString("special_attack_effort_value");
				String specialDefense_EffortValue = rs.getString("special_defense_effort_value");
				String speed_EffortValue = rs.getString("speed_effort_value");

				//やむを得ず作成、上のやつと型が違うだけで同じ
				int h_ev = rs.getInt("hp_effort_value");
				int a_ev = rs.getInt("attack_effort_value");
				int b_ev = rs.getInt("defense_effort_value");
				int c_ev = rs.getInt("special_attack_effort_value");
				int d_ev = rs.getInt("special_defense_effort_value");
				int s_ev = rs.getInt("speed_effort_value");

				String hp_IndividualValue = pokemonTrainingDao.hRaceValueConversion(rs.getString("hp_individual_value"));
				String attack_IndividualValue = pokemonTrainingDao.aRaceValueConversion(rs.getString("attack_individual_value"));
				String defense_IndividualValue = pokemonTrainingDao.bRaceValueConversion(rs.getString("defense_individual_value"));
				String specialAttack_IndividualValue = pokemonTrainingDao.cRaceValueConversion(rs.getString("special_attack_individual_value"));
				String specialDefense_IndividualValue = pokemonTrainingDao.dRaceValueConversion(rs.getString("special_defense_individual_value"));
				String speed_IndividualValue = pokemonTrainingDao.sRaceValueConversion(rs.getString("speed_individual_value"));

				int h_rv = rs.getInt("hp_race_value");
				int a_rv = rs.getInt("attack_race_value");
				int b_rv = rs.getInt("defense_race_value");
				int c_rv = rs.getInt("special_attack_race_value");
				int d_rv = rs.getInt("special_defense_race_value");
				int s_rv = rs.getInt("speed_race_value");

				String consideration = rs.getString("consideration");
				int getId = rs.getInt("create_id");
				String userName = rs.getString("user_name");

				int culList[] = pokemonTrainingDao.culculation(correction, h_rv, a_rv, b_rv, c_rv, d_rv, s_rv,
						h_ev, a_ev, b_ev, c_ev, d_ev, s_ev);

				int hp = culList[0];
				int attack = culList[1];
				int defense = culList[2];
				int specialAttack = culList[3];
				int specialDefense = culList[4];
				int speed = culList[5];

				return new trainingArticleData(id, name, updateTime, title, pokemonId, characteristic,
						correctionDao.correctionName(correction), item, skill1, skill2, skill3, skill4, hp_EffortValue, attack_EffortValue,
						defense_EffortValue, specialAttack_EffortValue, specialDefense_EffortValue, speed_EffortValue,
						hp_IndividualValue, attack_IndividualValue, defense_IndividualValue, specialAttack_IndividualValue,
						specialDefense_IndividualValue, speed_IndividualValue, consideration, hp, attack, defense, specialAttack,
						specialDefense, speed, getId, userName
						);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public trainingArticleData findTrainingArticleList3(int id){
		CorrectionDao correctionDao = new CorrectionDao();
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		Connection con = null;
		try{

			con = DBManager.getConnection();


			String sql = "SELECT " +
					"	training_article.id, " +
					"	training_article.user_id, " +
					"	training_article.pokemon_id, " +
					"	training_article.update_data, " +
					"	training_article.title," +
					"	training_article.characteristic, " +
					"	training_article.correction, " +
					"	training_article.item, " +

					"	training_article.skill1, " +
					"	training_article.skill2, " +
					"	training_article.skill3, " +
					"	training_article.skill4, " +

					"	training_article.hp_effort_value, " +
					"	training_article.attack_effort_value, " +
					"	training_article.defense_effort_value, " +
					"	training_article.special_attack_effort_value, " +
					"	training_article.special_defense_effort_value, " +
					"	training_article.speed_effort_value, " +

					"	training_article.hp_individual_value, " +
					"	training_article.attack_individual_value, " +
					"	training_article.defense_individual_value, " +
					"	training_article.special_attack_individual_value, " +
					"	training_article.special_defense_individual_value, " +
					"	training_article.speed_individual_value, " +

					"	training_article.consideration, " +
					"	training_article.create_id, " +
					"	training_article.user_name, " +

					"	pokemon_basic.name, " +
					"	pokemon_basic.hp_race_value, " +
					"	pokemon_basic.attack_race_value, " +
					"	pokemon_basic.defense_race_value, " +
					"	pokemon_basic.special_attack_race_value, " +
					"	pokemon_basic.special_defense_race_value, " +
					"	pokemon_basic.speed_race_value " +

					"FROM " +
					"	training_article " +
					"INNER JOIN " +
					"	pokemon_basic " +
					"ON " +
					"	training_article.pokemon_id = pokemon_basic.all_pokemon_id " +
					"WHERE " +
					"	training_article.id = ?";

			PreparedStatement st = con.prepareStatement(sql);

			st.setInt(1, id);
			ResultSet rs = st.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int getId = rs.getInt("id");
			String userId = rs.getString("user_id");
			String pokemonId = rs.getString("pokemon_id");
			String name = rs.getString("name");
			String update = rs.getString("update_data");
			String title = rs.getString("title");
			String characteristic = rs.getString("characteristic");
			int correction = rs.getInt("correction");

			String item = rs.getString("item");

			String skill1 = rs.getString("skill1");
			String skill2 = rs.getString("skill2");
			String skill3 = rs.getString("skill3");
			String skill4 = rs.getString("skill4");

			String h_ev = rs.getString("hp_effort_value");
			String a_ev = rs.getString("attack_effort_value");
			String b_ev = rs.getString("defense_effort_value");
			String c_ev = rs.getString("special_attack_effort_value");
			String d_ev = rs.getString("special_defense_effort_value");
			String s_ev = rs.getString("speed_effort_value");

			int int_h_ev = rs.getInt("hp_effort_value");
			int int_a_ev = rs.getInt("attack_effort_value");
			int int_b_ev = rs.getInt("defense_effort_value");
			int int_c_ev = rs.getInt("special_attack_effort_value");
			int int_d_ev = rs.getInt("special_defense_effort_value");
			int int_s_ev = rs.getInt("speed_effort_value");

			String h_iv = pokemonTrainingDao.hRaceValueConversion(rs.getString("hp_individual_value"));
			String a_iv = pokemonTrainingDao.aRaceValueConversion(rs.getString("attack_individual_value"));
			String b_iv = pokemonTrainingDao.bRaceValueConversion(rs.getString("defense_individual_value"));
			String c_iv = pokemonTrainingDao.cRaceValueConversion(rs.getString("special_attack_individual_value"));
			String d_iv = pokemonTrainingDao.dRaceValueConversion(rs.getString("special_defense_individual_value"));
			String s_iv = pokemonTrainingDao.sRaceValueConversion(rs.getString("speed_individual_value"));

			String consideration = rs.getString("consideration");
			int cId = rs.getInt("create_id");
			String userName = rs.getString("user_name");

			int h_rv = rs.getInt("hp_race_value");
			int a_rv = rs.getInt("attack_race_value");
			int b_rv = rs.getInt("defense_race_value");
			int c_rv = rs.getInt("special_attack_race_value");
			int d_rv = rs.getInt("special_defense_race_value");
			int s_rv = rs.getInt("speed_race_value");

			int culList[] = pokemonTrainingDao.culculation(correction, h_rv, a_rv, b_rv, c_rv, d_rv, s_rv,
					int_h_ev, int_a_ev, int_b_ev, int_c_ev, int_d_ev, int_s_ev);

			int hp = culList[0];
			int attack = culList[1];
			int defense = culList[2];
			int specialAttack = culList[3];
			int specialDefense = culList[4];
			int speed = culList[5];

			return new trainingArticleData(getId, userId, pokemonId, name, update, title, characteristic,
					correctionDao.correctionName(correction), item, skill1, skill2, skill3, skill4,
					h_ev, a_ev, b_ev, c_ev, d_ev, s_ev, h_iv, a_iv, b_iv, c_iv, d_iv, s_iv, consideration,
					cId, userName, hp, attack, defense, specialAttack,
					specialDefense, speed);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
//	public trainingArticleData findTrainingArticleList3(String title){
//		CorrectionDao correctionDao = new CorrectionDao();
//		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
//		Connection con = null;
//		try{
//
//			con = DBManager.getConnection();
//
//
//			String sql = "SELECT * FROM training_article create_id WHERE title = ?";
//			PreparedStatement st = con.prepareStatement(sql);
//
//			st.setString(1, title);
//			ResultSet rs = st.executeQuery();
//
//			while (rs.next()) {
//	}
//

	//String型をデータ型にするためのメソッド
	public trainingArticleData cId(String cid) {

		Connection con = null;
		PreparedStatement st = null;
		try {

			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM training_article WHERE create_id = ?");
			st.setString(1, cid);
			ResultSet rs = st.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int cId = rs.getInt("create_id");
			return new trainingArticleData(cId);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//全部のユーザで検索をかけリストで取得
	public List<trainingArticleData> allUserArticleTrainingList(){
		CorrectionDao correctionDao = new CorrectionDao();
		PokemonTrainingDao pokemonTrainingDao = new PokemonTrainingDao();
		Connection con = null;
		List<trainingArticleData> taList = new ArrayList<trainingArticleData>();
		try{

			con = DBManager.getConnection();


			String sql = "SELECT " +
					"	training_article.id, " +
					"	training_article.user_id, " +
					"	training_article.pokemon_id, " +
					"	training_article.update_data, " +
					"	training_article.title," +
					"	training_article.characteristic, " +
					"	training_article.correction, " +
					"	training_article.item, " +

					"	training_article.skill1, " +
					"	training_article.skill2, " +
					"	training_article.skill3, " +
					"	training_article.skill4, " +

					"	training_article.hp_effort_value, " +
					"	training_article.attack_effort_value, " +
					"	training_article.defense_effort_value, " +
					"	training_article.special_attack_effort_value, " +
					"	training_article.special_defense_effort_value, " +
					"	training_article.speed_effort_value, " +

					"	training_article.hp_individual_value, " +
					"	training_article.attack_individual_value, " +
					"	training_article.defense_individual_value, " +
					"	training_article.special_attack_individual_value, " +
					"	training_article.special_defense_individual_value, " +
					"	training_article.speed_individual_value, " +

					"	training_article.consideration, " +
					"	training_article.create_id, " +
					"	training_article.user_name, " +

					"	pokemon_basic.name, " +
					"	pokemon_basic.hp_race_value, " +
					"	pokemon_basic.attack_race_value, " +
					"	pokemon_basic.defense_race_value, " +
					"	pokemon_basic.special_attack_race_value, " +
					"	pokemon_basic.special_defense_race_value, " +
					"	pokemon_basic.speed_race_value " +

					"FROM " +
					"	training_article " +
					"INNER JOIN " +
					"	pokemon_basic " +
					"ON " +
					"	training_article.pokemon_id = pokemon_basic.all_pokemon_id";

			PreparedStatement st = con.prepareStatement(sql);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String userId = rs.getString("user_id");
				String pokemonId = rs.getString("pokemon_id");
				String name = rs.getString("name");
				String update = rs.getString("update_data");
				String title = rs.getString("title");
				String characteristic = rs.getString("characteristic");
				int correction = rs.getInt("correction");

				String item = rs.getString("item");

				String skill1 = rs.getString("skill1");
				String skill2 = rs.getString("skill2");
				String skill3 = rs.getString("skill3");
				String skill4 = rs.getString("skill4");

				String h_ev = rs.getString("hp_effort_value");
				String a_ev = rs.getString("attack_effort_value");
				String b_ev = rs.getString("defense_effort_value");
				String c_ev = rs.getString("special_attack_effort_value");
				String d_ev = rs.getString("special_defense_effort_value");
				String s_ev = rs.getString("speed_effort_value");

				int int_h_ev = rs.getInt("hp_effort_value");
				int int_a_ev = rs.getInt("attack_effort_value");
				int int_b_ev = rs.getInt("defense_effort_value");
				int int_c_ev = rs.getInt("special_attack_effort_value");
				int int_d_ev = rs.getInt("special_defense_effort_value");
				int int_s_ev = rs.getInt("speed_effort_value");

				String h_iv = pokemonTrainingDao.hRaceValueConversion(rs.getString("hp_individual_value"));
				String a_iv = pokemonTrainingDao.aRaceValueConversion(rs.getString("attack_individual_value"));
				String b_iv = pokemonTrainingDao.bRaceValueConversion(rs.getString("defense_individual_value"));
				String c_iv = pokemonTrainingDao.cRaceValueConversion(rs.getString("special_attack_individual_value"));
				String d_iv = pokemonTrainingDao.dRaceValueConversion(rs.getString("special_defense_individual_value"));
				String s_iv = pokemonTrainingDao.sRaceValueConversion(rs.getString("speed_individual_value"));

				String consideration = rs.getString("consideration");
				int getId = rs.getInt("create_id");
				String userName = rs.getString("user_name");

				int h_rv = rs.getInt("hp_race_value");
				int a_rv = rs.getInt("attack_race_value");
				int b_rv = rs.getInt("defense_race_value");
				int c_rv = rs.getInt("special_attack_race_value");
				int d_rv = rs.getInt("special_defense_race_value");
				int s_rv = rs.getInt("speed_race_value");

				int culList[] = pokemonTrainingDao.culculation(correction, h_rv, a_rv, b_rv, c_rv, d_rv, s_rv,
						int_h_ev, int_a_ev, int_b_ev, int_c_ev, int_d_ev, int_s_ev);

				int hp = culList[0];
				int attack = culList[1];
				int defense = culList[2];
				int specialAttack = culList[3];
				int specialDefense = culList[4];
				int speed = culList[5];

				trainingArticleData tad = new trainingArticleData(id, userId, pokemonId, name, update, title, characteristic,
						correctionDao.correctionName(correction), item, skill1, skill2, skill3, skill4,
						h_ev, a_ev, b_ev, c_ev, d_ev, s_ev, h_iv, a_iv, b_iv, c_iv, d_iv, s_iv, consideration,
						getId, userName, hp, attack, defense, specialAttack,
						specialDefense, speed);

				taList.add(tad);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	return taList;
	}
}

