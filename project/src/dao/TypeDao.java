package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.typeData;




public class TypeDao {

	//typeテーブルの情報を取得する
	public ArrayList<typeData> getTypeData(){
		ArrayList<typeData> typeDataList = new ArrayList<typeData>();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM type");

			ResultSet rs = st.executeQuery();


			while (rs.next()) {
				int id = rs.getInt("id");
				String type = rs.getString("type");
				typeData t = new typeData(id, type);
				typeDataList.add(t);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return typeDataList;
	}

	//送られてきたtypeをもとにidを返す
	public String typeId(String type) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM type WHERE type = ?");
			st.setString(1, type);

			ResultSet rs = st.executeQuery();
			if (!rs.next()) {
				return null;
			}

			String id = rs.getString("id");
			return id;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public String typeName(int typeId) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM type WHERE id = ?");
			st.setInt(1, typeId);

			ResultSet rs = st.executeQuery();
			if (!rs.next()) {
				return null;
			}

			String type = rs.getString("type");
			return type;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
