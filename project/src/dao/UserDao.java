package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.userData;


public class UserDao {

	//ユーザのログインIDとパスワードの組み合わせが存在するか
	public userData loginInfo(String loginId, String password) {
		Connection con = null;
		PreparedStatement st = null;
		try {

			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM user WHERE login_id = ? and password = ?");
			st.setString(1, loginId);
			st.setString(2, password);
			ResultSet rs = st.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int us = rs.getInt("user_id");
			String lo = rs.getString("login_id");
			String na = rs.getString("name");
			return new userData(us, lo, na);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public int getUserId(String loginId) {
		Connection con = null;
		PreparedStatement st = null;
		try {

			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM user WHERE login_id = ?");
			st.setString(1, loginId);
			ResultSet rs = st.executeQuery();

			if (!rs.next()) {
				return 0;
			}
			int uId = rs.getInt("user_id");
			return uId;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	//ユーザ情報入力
	public void userInsert(String loginId,String name,String password) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("INSERT INTO user(login_id, name, password, create_data) VALUES(?,?,?,now())");


			st.setString(1, loginId);
			st.setString(2, name);
			st.setString(3, password);
			st.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//登録されているユーザをすべて取得
	public List<userData> allUser(){

		Connection con = null;
		List<userData> userList = new ArrayList<userData>();

		try {
			con= DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE user_id >=2";
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while(rs.next()) {
				int id = rs.getInt("user_id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				Date createData = rs.getDate("create_data");

				userData userInfo = new userData(id, loginId, name, password, createData);
				userList.add(userInfo);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//指定したユーザIDのものだけを取得
	public userData userDetail(String id){

		Connection con = null;

		try {
			con= DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, id);
			ResultSet rs = st.executeQuery();

			if (!rs.next()) {
				return null;
			}

				int userId = rs.getInt("user_id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				Date createData = rs.getDate("create_data");

				return new userData(userId, loginId, name, password, createData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザ更新
	public void userUpdate(String loginId, String name, String pass){

		Connection con = null;
		PreparedStatement st = null;

		try {
			con= DBManager.getConnection();

			st = con.prepareStatement("UPDATE user SET name=?, password=? WHERE login_id=?");


			st.setString(1, name);
			st.setString(2, pass);
			st.setString(3, loginId);
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//ユーザ削除
	public void userDelete(String id){
		Connection con = null;
		PreparedStatement st = null;

		try {
			con= DBManager.getConnection();
			String sql = "DELETE FROM user WHERE login_id = ?";

			st = con.prepareStatement(sql);
			st.setString(1, id);
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	//ユーザ検索
	public List<userData> userSearch(String searchLoginId,String searchName){

		Connection con = null;
		List<userData> userList = new ArrayList<userData>();


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE user_id >=2";

			if(!(searchLoginId.equals(""))) {
				sql += " AND login_id = '" + searchLoginId + "'";
			}
			//部分一致で処理
			if(!(searchName.equals(""))) {
				sql += " AND name LIKE '%" + searchName + "%'";
			}

			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("user_id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date createData = rs.getDate("create_data");

				userData user = new userData(id, loginId, name, createData);
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;//[]が入っていた
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;//nullのまま
				}
			}
		}
			return userList;
	}

	//MD5メソッド
	public String md5(String password) throws NoSuchAlgorithmException {
	//ハッシュを生成したい元の文字列
	String source = password;
	//ハッシュ生成前にバイト配列に置き換える際のCharset
	Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
	String algorithm = "MD5";

	//ハッシュ生成処理
	byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	String result = DatatypeConverter.printHexBinary(bytes);
	//標準出力
	return result;
	}


	//ログインIDの入力項目の重複チェック
	public  String loginIdRegistrationError(String loginId) {
		Connection con = null;

		try {
			con = DBManager.getConnection();

			//String型を表示しないのでどっか修正
			String sql = "SELECT login_id FROM user WHERE login_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			//UserCreateで入力されたloginIdが第2引数に入る
			pStmt.setString(1, loginId);
			//loginIdの結果表の準備
			ResultSet rs = pStmt.executeQuery();

			while (!rs.next()) {
				return null;
			}


			//結果表のlogin_idを呼び出し、行数分それを実行
			String loginData = rs.getString("login_id");
			return loginData;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
	}


}
