
CREATE TABLE classification (
	id int PRIMARY KEY AUTO_INCREMENT,
	classification varchar(4) UNIQUE NOT NULL
)
