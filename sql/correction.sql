
CREATE TABLE correction(
	id int PRIMARY KEY AUTO_INCREMENT,
	correction_type varchar(10) NOT NULL,
	ascent_correction varchar(10),
	descent_correction varchar(10)
)