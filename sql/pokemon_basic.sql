
CREATE TABLE pokemon_basic (
	id int PRIMARY KEY AUTO_INCREMENT,
	all_pokemon_id varchar(10) UNIQUE NOT NULL,
	galar_pokemon_id varchar(10) UNIQUE NOT NULL,
	name varchar(10) UNIQUE NOT NULL,
	weight double NOT NULL,
	type_id1 int NOT NULL,
	type_id2 int,
	characteristic1 varchar(30) NOT NULL,
	characteristic2 varchar(30),
	characteristic_secret varchar(30),
	egg_group1_id int NOT NULL,
	egg_group2_id int,
	hp_race_value int NOT NULL,
	attack_race_value int NOT NULL,
	defense_race_value int NOT NULL,
	special_attack_race_value int NOT NULL,
	special_defense_race_value int NOT NULL,
	speed_race_value int NOT NULL
)
