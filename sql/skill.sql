
CREATE TABLE skill (
	skill_id int PRIMARY KEY AUTO_INCREMENT
	name int UNIQUE NOT NULL,
	type_id int NOT NULL,
	classification int NOT NULL,
	power int NOT NULL,
	accuracy int NOT NULL,
	pp int NOT NULL,
	description text NOT NULL
)
