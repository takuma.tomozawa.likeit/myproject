

CREATE TABLE training_article (
	id int PRIMARY KEY AUTO_INCREMENT,
	user_id int NOT NULL,
	pokemon_id int NOT NULL,
	update_data date NOT NULL,
	title varchar(50) UNIQUE NOT NULL,
	characteristic varchar(10) NOT NULL,
	correction int NOT NULL,
	item varchar(10) NOT NULL,
	
	skill1 varchar(10) NOT NULL,
	skill2 varchar(10) NOT NULL,
	skill3 varchar(10) NOT NULL,
	skill4 varchar(10) NOT NULL,
	
	hp_effort_value int NOT NULL,
	attack_effort_value int NOT NULL,
	defense_effort_value int NOT NULL,
	special_attack_effort_value int NOT NULL,
	special_defense_effort_value int NOT NULL,
	speed_effort_value int NOT NULL,
	
	hp_individual_value varchar(5),
	attack_individual_value varchar(5),
	defense_individual_value varchar(5),
	special_attack_individual_value varchar(5),
	special_defense_individual_value varchar(5),
	speed_individual_value varchar(5),
	
	consideration text NOT NULL,
	name varchar(10)
)
