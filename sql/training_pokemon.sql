
CREATE TABLE training_pokemon (
	id int PRIMARY KEY AUTO_INCREMENT,
	pokemon_id int NOT NULL,
	user_id int NOT NULL,
	nickname varchar(10),
	characteristic varchar(10),
	correction int,
	item varchar(10),
	
	skill1 varchar(10),
	skill2 varchar(10),
	skill3 varchar(10),
	skill4 varchar(10),
	
	hp_effort_value int,
	attack_effort_value int,
	defense_effort_value int,
	special_attack_effort_value int,
	special_defense_effort_value int,
	speed_effort_value int,
	
	hp_individual_value varchar(5),
	attack_individual_value varchar(5),
	defense_individual_value varchar(5),
	special_attack_individual_value varchar(5),
	special_defense_individual_value varchar(5),
	speed_individual_value varchar(5),
	
	remarks text
)

