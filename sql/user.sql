
CREATE TABLE user (
	user_id int PRIMARY KEY AUTO_INCREMENT,
	login_id int UNIQUE NOT NULL,
	name varchar(20) UNIQUE NOT NULL,
	password varchar(50) UNIQUE NOT NULL,
	create_data datetime NOT NULL
)
